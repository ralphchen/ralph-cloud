package com.ralph.common.codegen;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.ralph.common.constant.CodeGenInfo;
import com.ralph.common.core.entity.BaseEntity;

import java.util.Collections;

/**
 * MybatisPlus简单代码生成
 */
public class CodeGenerator {


    /**
     * 通过main方法直接生成
     * @param args
     */
    public static void main(String[] args) {
        // 构建自动生成代码对象
        FastAutoGenerator.create(CodeGenInfo.DATABASE_URL, CodeGenInfo.USERNAME, CodeGenInfo.PASSWORD)
                // 全局配置
                .globalConfig(builder -> {
                    builder.author(CodeGenInfo.AUTHOR) // 设置作者
                            .disableOpenDir() // 禁止打开目录
                            .enableSwagger() // 开启 swagger 模式
                            .outputDir(CodeGenInfo.FILEPATH); // 指定输出目录
                })
                // 包配置
                .packageConfig(builder -> {
                    builder.parent(CodeGenInfo.PARENT_PACKAGE) // 设置父包名
                            .moduleName(CodeGenInfo.PARENT_MODLE) // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, CodeGenInfo.XML_PATH)); // 设置mapperXml生成路径
                })
                // 策略配置
                .strategyConfig(builder -> {
                    builder
                            .addInclude(CodeGenInfo.GEN_TABLE_LIST) // 设置需要生成的表名
                            // .addFieldPrefix("sjcq_") // 通过表前缀增加表
                            // .addTableSuffix() // 通过表后缀增加表
                            // .addExclude() // 需要排除的表
                            .entityBuilder()
                            .superClass(BaseEntity.class) // 表继承的父类
                            // 生成表字段时忽略的字段
                           .addIgnoreColumns(CodeGenInfo.IGNORE_COLUMN_LIST)
                    ;
                    // mapper配置
                    builder.mapperBuilder()
                            .enableBaseColumnList() // 生成<sql id="Base_Column_List">
                            .enableBaseResultMap()
                            .serviceBuilder();

                    // 使用Lombok注解
                    builder.entityBuilder().enableLombok();

                    // 覆盖原有文件
                    builder.entityBuilder().enableFileOverride();
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
