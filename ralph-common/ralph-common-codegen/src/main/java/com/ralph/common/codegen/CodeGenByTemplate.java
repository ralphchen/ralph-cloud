package com.ralph.common.codegen;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.po.TableField;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import com.ralph.common.constant.CodeGenInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Collections;

/**
 * MybatisPlus基于模板生成代码
 */
public class CodeGenByTemplate {

   public static void main(String[] args) {
        // 构建自动生成代码对象
        FastAutoGenerator.create(CodeGenInfo.DATABASE_URL, CodeGenInfo.USERNAME, CodeGenInfo.PASSWORD)
                .globalConfig(builder -> {
                    builder
                            .outputDir(CodeGenInfo.FILEPATH)
                            .enableSwagger()
                            .disableOpenDir()
                            .author(CodeGenInfo.AUTHOR)
                            .dateType(DateType.ONLY_DATE);
                })
                .packageConfig(builder -> {
                    builder.parent(CodeGenInfo.PARENT_PACKAGE)
                            .moduleName(CodeGenInfo.PARENT_MODLE)
                            .pathInfo(Collections.singletonMap(OutputFile.xml, CodeGenInfo.XML_PATH)); // 设置mapperXml生成路径
                })
                // 策略配置
                .strategyConfig(builder -> {
                    // 通用策略配置
                    builder
                            .addInclude(CodeGenInfo.GEN_TABLE_LIST) // 设置需要生成的表名
                    // .addFieldPrefix("sjcq_") // 通过表前缀增加表
                    // .addTableSuffix() // 通过表后缀增加表
                    // .addExclude() // 需要排除的表
                    ;
                    // controller策略配置
                    builder.controllerBuilder().enableFileOverride().enableRestStyle();
                    // 覆盖原文件
                    // builder.serviceBuilder().enableFileOverride();
                    // mapper配置
                    builder.mapperBuilder()
                            .mapperAnnotation(Mapper.class)
                            .enableFileOverride()
                            // .enableBaseColumnList() // 生成<sql id="Base_Column_List">
                            .enableBaseResultMap();
                    // 实体类策略配置
                    builder.entityBuilder().enableLombok().enableFileOverride();
                            // .superClass(BaseEntity.class)
                            // 生成表字段时忽略的字段
                            // .addIgnoreColumns(CodeGenInfo.IGNORE_COLUMN_LIST)
                            // 父类公共字段
                            // .addSuperEntityColumns(CodeGenInfo.IGNORE_COLUMN_LIST);
                    builder.serviceBuilder().formatServiceFileName("%sService");
                    // 重写生成的文件名
                    // builder.entityBuilder().convertFileName(converterFileName)
                    //         .serviceBuilder().convertServiceFileName(converterFileName).convertServiceImplFileName(converterFileName)
                    //         .mapperBuilder().convertMapperFileName(converterFileName).convertXmlFileName(converterFileName);
                })
                .templateConfig(builder -> builder.disable())// 禁用所有内置模板
                .injectionConfig(builder -> {
                    builder.beforeOutputFile((tableInfo,objectMap)->{
                                String pathName = tableInfo.getName();
                                pathName = pathName.substring(!pathName.contains("_") ? 0: pathName.indexOf("_") + 1);
                                // 自定义模板参数
                                objectMap.put("pathName",pathName);
                                // 是否增加actionLog注解
                                objectMap.put("actionLog",CodeGenInfo.ACTION_LOG);
                                // 是否增加权限注解
                                objectMap.put("permission",CodeGenInfo.PERMISSION);
                                // 是否手动封装返回结果
                                objectMap.put("responseHandle",CodeGenInfo.RESPONSE_HANDLE);
                                // 是否使用AR式
                                objectMap.put("activeRecord",CodeGenInfo.ACTIVE_RECORD);
                                // 是否有主键
                                objectMap.put("hasKeyIdentity",false);
                                // 获取主键与主键类型
                                for (TableField commonField : tableInfo.getCommonFields()) {
                                    if (commonField.isKeyIdentityFlag()){
                                        objectMap.put("hasKeyIdentity",true);
                                        objectMap.put("columnId",commonField.getName());
                                        objectMap.put("columnType",commonField.getColumnType().getType());
                                    }
                                }
                            })
                            // 自定义模板参数
                            // .customMap(Collections.singletonMap("test", "hello"))
                            .customFile(CodeGenInfo.TEMPLATE_LIST).build();
                })
                .templateEngine(new VelocityTemplateEngine()) // 使用Velocity引擎模板
                .execute();
    }
}
