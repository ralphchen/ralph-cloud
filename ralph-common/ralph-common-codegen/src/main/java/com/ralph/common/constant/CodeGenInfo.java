package com.ralph.common.constant;

import com.baomidou.mybatisplus.generator.config.builder.CustomFile;
import com.google.common.collect.Lists;

import java.util.List;

public class CodeGenInfo {

    private static final String BASE_PATH = System.getProperty("user.dir") + "\\ralph-common\\ralph-common-codegen\\src\\main";

    /**
     * 代码文件生成路径：codegen模块resource下codegen文件夹
     */
    public static final String FILEPATH = BASE_PATH + "\\java";

    /**
     * XML文件路径
     */
    public static final String XML_PATH = BASE_PATH + "\\resources\\mappers";

    /**
     * 数据库连接
     */
    public static final String DATABASE_URL = "jdbc:mysql://localhost:3306/ralph_cloud?userSSl=true&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT";

    /**
     * 数据库用户名
     */
    public static final String USERNAME = "root";

    /**
     * 数据库密码
     */
    public static final String PASSWORD = "123456";

    /**
     * 作者
     */
    public static final String AUTHOR = "ralph";

    /**
     * 是否自动为增删改接口增加日志注解
     */
    public static final boolean ACTION_LOG = true;

    /**
     * 是否增加权限注解
     */
    public static final boolean PERMISSION = false;

    /**
     * 是否手动封装返回结果
     */
    public static final boolean RESPONSE_HANDLE = false;

    /**
     * 是否使用AR模式
     */
    public static final boolean ACTIVE_RECORD = true;

    /**
     * 忽略字段列表
     */
    public static final List<String> IGNORE_COLUMN_LIST = Lists.newArrayList("id","del_flag","version","create_date","create_by","update_date","update_by");
    // public static final List<String> IGNORE_COLUMN_LIST = new ArrayList<>();

    /**
     * 生成表列表
     */
    public static final List<String> GEN_TABLE_LIST = Lists.newArrayList("rule_item");

    /**
     * 父包名
     */
    public static final String PARENT_PACKAGE = "com.ralph.common.rule";

    /**
     * 父模块
     */
    // public static final String PARENT_MODLE = "article";
    public static final String PARENT_MODLE = "";

    /**
     * 模板文件列表
     */
    public static List<CustomFile> TEMPLATE_LIST;

    static {
        TEMPLATE_LIST = Lists.newArrayList(
                new CustomFile.Builder().fileName("Controller.java").templatePath("\\template\\Controller.java.vm").packageName("controller").enableFileOverride().build(),
                new CustomFile.Builder().fileName(".java").templatePath("\\template\\Entity.java.vm").packageName("entity").enableFileOverride().build(),
                new CustomFile.Builder().fileName("Mapper.java").templatePath("\\template\\Mapper.java.vm").packageName("mapper").enableFileOverride().build(),
                // xml文件生成路径为XML_PATH变量指定，可自定义修改
                new CustomFile.Builder().fileName("Mapper.xml").templatePath("\\template\\Mapper.xml.vm").filePath(XML_PATH).enableFileOverride().build(),
                new CustomFile.Builder().fileName("Service.java").templatePath("\\template\\Service.java.vm").packageName("service").enableFileOverride().build(),
                new CustomFile.Builder().fileName("ServiceImpl.java").templatePath("\\template\\ServiceImpl.java.vm").packageName("service\\impl").enableFileOverride().build()

        );
        // 如果需要增加permission注解才生成sql文件
        if (PERMISSION){
            TEMPLATE_LIST.add(new CustomFile.Builder().fileName("menu.sql").templatePath("\\template\\menu.sql.vm").packageName("sql").enableFileOverride().build());
        }
    }
}
