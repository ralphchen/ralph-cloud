package com.ralph.common.codegen;

import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
import com.google.common.collect.Lists;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.util.List;

/**
 * 数据库文档自动生成
 */
public class DataBaseDocs {

    public static void main(String[] args) {
        Configuration screwConfig = getScrewConfig(getDataSource(), getEngineConfig(), getProcessConfig());
        new DocumentationExecute(screwConfig).execute();
    }

    private static Configuration getScrewConfig(DataSource dataSource, EngineConfig engineConfig, ProcessConfig processConfig) {
        return Configuration.builder()
                //版本
                .version("1.0.0")
                //描述
                .description("数据库设计文档生成")
                //数据源
                .dataSource(dataSource)
                //生成配置
                .engineConfig(engineConfig)
                //生成配置
                .produceConfig(processConfig)
                .build();
    }

    /**
     * 获取数据库源
     */
    private static DataSource getDataSource() {
        //数据源
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName("com.mysql.cj.jdbc.Driver");
        hikariConfig.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/ralph_cloud?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8");
        hikariConfig.setUsername("root");
        hikariConfig.setPassword("123456");
        //设置可以获取tables remarks信息
        hikariConfig.addDataSourceProperty("useInformationSchema", "true");
        hikariConfig.setMinimumIdle(2);
        hikariConfig.setMaximumPoolSize(5);
        return new HikariDataSource(hikariConfig);
    }

    /**
     * 获取文件生成配置
     */
    private static EngineConfig getEngineConfig() {
        String path = System.getProperty("user.dir") + "\\ralph-common\\ralph-common-codegen\\src\\main\\java\\com\\ralph\\common\\database";
        //生成配置
        return EngineConfig.builder()
                //生成文件路径
                .fileOutputDir(path)
                //打开目录
                .openOutputDir(false)
                //文件类型
                .fileType(EngineFileType.MD)
                //生成模板实现
                .produceType(EngineTemplateType.freemarker)
                //自定义文件名称
                .fileName("数据库结构文档").build();
    }

    /**
     * 获取数据库表的处理配置，可忽略
     */
    private static ProcessConfig getProcessConfig() {
        // 需要生成的表名
        List<String> designatedTableName = Lists.newArrayList("system");
        // 需要忽略的表名
        List<String> ignoreTableName = Lists.newArrayList("business");
        return ProcessConfig.builder()
                .designatedTableName(designatedTableName)
                .ignoreTableName(ignoreTableName)
                .designatedTablePrefix(designatedTableName)
                .ignoreTablePrefix(ignoreTableName)
                .build();
    }
}
