package com.ralph.common.feign.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableFeignClients(basePackages = {"com.ralph.common.feign.feigns"})
@ComponentScan(basePackages = {"com.ralph.common.feign.fallback"})
public class FeignConfig {


}
