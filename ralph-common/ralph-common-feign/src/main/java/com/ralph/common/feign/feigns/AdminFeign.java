package com.ralph.common.feign.feigns;

import com.ralph.common.core.constant.ServiceConstant;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.entity.UserDTO;
import com.ralph.common.feign.fallback.AdminFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = ServiceConstant.ADMIN, fallback = AdminFeignFallback.class)
public interface AdminFeign {

    @GetMapping("/user/userinfo/username")
    Result<UserDTO> getUserByUsername(@RequestParam("username")String username);

    @GetMapping("/user/third/key")
    Result<UserDTO> getUserInfoByThirdKey(@RequestParam("type")String thirdInfo);

    @GetMapping("/user/sms/phone")
    Result<UserDTO> getUserInfoByPhone(@RequestParam("phone")String phone);
}
