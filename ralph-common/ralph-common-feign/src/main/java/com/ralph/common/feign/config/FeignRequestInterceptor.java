package com.ralph.common.feign.config;


import feign.Request;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

@Component
public class FeignRequestInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        // 处理POST请求的时候转换成了GET的bug
        if (HttpMethod.POST.name().equals(template.method()) && template.requestBody().length() == 0 && !template.queries().isEmpty()) {
            // 获取请求头信息
            Boolean isPost = Optional.of(template.headers()).map(item -> item.get("ralph-cus-header"))
                    .map(item -> item.contains("post"))
                    .orElse(false);
            if (isPost){
                StringBuilder builder = new StringBuilder();
                Map<String, Collection<String>> queries = template.queries();
                Iterator<String> queriesIterator = queries.keySet().iterator();

                while (queriesIterator.hasNext()) {
                    String field = queriesIterator.next();
                    Collection<String> strings = queries.get(field);
                    // 由于参数已经做了url编码处理，这里直接拼接即可
                    builder.append(field + "=" + StringUtils.join(strings, ","));
                    builder.append("&");
                }
                template.body(Request.Body.encoded(builder.toString().getBytes(), template.requestCharset()));
                template.queries(null);
            }
        }
    }
}
