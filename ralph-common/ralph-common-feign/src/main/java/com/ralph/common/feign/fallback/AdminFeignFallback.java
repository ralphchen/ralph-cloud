package com.ralph.common.feign.fallback;

import com.ralph.common.core.entity.Result;
import com.ralph.common.core.entity.UserDTO;
import com.ralph.common.feign.feigns.AdminFeign;
import org.springframework.stereotype.Component;

@Component
public class AdminFeignFallback implements AdminFeign {
    @Override
    public Result<UserDTO> getUserByUsername(String username) {
        return Result.failed("服务调用失败");
    }

    @Override
    public Result<UserDTO> getUserInfoByThirdKey(String thirdInfo) {
        return Result.failed("服务调用失败");
    }

    @Override
    public Result<UserDTO> getUserInfoByPhone(String phone) {
        return Result.failed("服务调用失败");
    }
}
