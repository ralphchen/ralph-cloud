package com.ralph.common.rule.service;

import java.util.Map;

public interface RuleCheckService {

    void check(Map<String,Object> paramMap, String ruleCode);
}
