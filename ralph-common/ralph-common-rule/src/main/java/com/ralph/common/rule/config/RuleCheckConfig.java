package com.ralph.common.rule.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.ralph.common.rule.engin","com.ralph.common.rule.service","com.ralph.common.rule.util","com.ralph.common.rule.rule"})
//@ComponentScan({"com.ralph.common.rule"})
@MapperScan("com.ralph.common.rule.mapper")
public class RuleCheckConfig {
}
