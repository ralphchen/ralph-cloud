package com.ralph.common.rule.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 
 *
 * @author ralph
 * @since 2023-07-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("rule_item")
public class RuleItem extends Model<RuleItem> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 创建时间
     */
    private Timestamp createDate;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 更新时间
     */
    private Timestamp updateDate;

    /**
     * 更新时间
     */
    private String updateBy;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer delFlag;

    /**
     * 乐观锁
     */
    @Version
    private Integer version;

    /**
     * 规则编码
     */
    private String ruleCode;

    /**
     * 规则描述
     */
    private String ruleDesc;

    /**
     * 规则执行条件
     */
    private String executeCond;

    /**
     * 规则执行顺序
     */
    private Integer ruleSort;

    /**
     * 规则执行条件描述
     */
    private String executeCondDesc;


@Override
public Serializable pkVal() {
            return this.id;
        }

}

