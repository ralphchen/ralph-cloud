package com.ralph.common.rule.rule;

import java.util.List;

public interface RalphRule {

    void check(List<Object> paramList, List<Object> valueList);
}
