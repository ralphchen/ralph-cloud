package com.ralph.common.rule.contant;

public class RalphRuleConstant {

    /**
     * 所有参数不允许为空
     */
    public static final String ALL_NOT_BLANK = "ruleAllNotBlank";
}
