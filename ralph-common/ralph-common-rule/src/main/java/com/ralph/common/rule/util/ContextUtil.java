package com.ralph.common.rule.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ContextUtil implements ApplicationContextAware {
    private static ApplicationContext appCxt;

    @Override
    public void setApplicationContext(ApplicationContext appContext)// 交给spring容器管理 自动注入进来
            throws BeansException {
        if (appCxt == null) {
            appCxt = appContext;
        }
        log.info("ApplicationContext 对象初始化成功");
    }

    public static ApplicationContext getApplicationContext() {
        return appCxt;
    }

    // 根据名称从spring容器中获取bean实例
    public static Object getBean(String beanName) {
        return appCxt.getBean(beanName);
    }

    // 根据Java类型从Spring容器中获取bean实例
    public static <T> T getBean(Class<T> clazz) {
        return appCxt.getBean(clazz);
    }

    // 根据名称和Java类型从Spring容器中获取bean实例
    public static <T> T getBean(String beanName, Class<T> clazz) {
        return appCxt.getBean(beanName, clazz);
    }

}
