package com.ralph.common.rule.engin.impl;

import com.ralph.common.rule.engin.RuleCheckEngin;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 规则实现类，命名规则：Rule+RuleCode
 */
@Service
public class RuleTest implements RuleCheckEngin {

    @Override
    public String check(Map<String, Object> paramMap) {
        if (!paramMap.get("name").equals("张三")){
            return "姓名不等于张三";
        }
        return null;
    }
}
