package com.ralph.common.rule.rule.impl;

import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.rule.rule.RalphRule;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.ralph.common.core.constant.ErrorEnum.RULE_CHECK_ERROR;

@Component
public class RuleAllNotBlank implements RalphRule {

    @Override
    public void check(List<Object> paramList, List<Object> valueList) {
        List<String> resList = new ArrayList<>();
        for (int i = 0; i < valueList.size(); i++) {
            Object object = valueList.get(i);
            if (null == object || StringUtils.isBlank(object.toString())){
                resList.add(paramList.get(i).toString());
            }
        }
        if (!resList.isEmpty()){
            String message = "参数【" + String.join(",", resList) + "】不能为空";
            throw new BusinessException(RULE_CHECK_ERROR,message,true);
        }
    }
}
