package com.ralph.common.rule.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.rule.engin.RuleCheckEngin;
import com.ralph.common.rule.entity.RuleItem;
import com.ralph.common.rule.mapper.RuleItemMapper;
import com.ralph.common.rule.service.RuleCheckService;
import com.ralph.common.rule.util.ContextUtil;
import com.ralph.common.rule.util.RuleCheckUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.ralph.common.core.constant.ErrorEnum.RULE_CHECK_ERROR;

@Slf4j
@Service
@RequiredArgsConstructor
public class RuleCheckServiceImpl implements RuleCheckService {

    private final RuleItemMapper ruleItemMapper;

    private final static String RULE_PRE = "rule";

    @Override
    public void check(Map<String, Object> paramMap, String ruleCode) {
        log.info("规则引擎执行开始，规则编码："+ ruleCode + ",校验参数：" + paramMap);
        if (StringUtils.isBlank(ruleCode)){
            return;
        }
        // 根据规则编码查询数据库
        List<RuleItem> ruleItems = ruleItemMapper.selectList(Wrappers.<RuleItem>lambdaQuery()
                .eq(RuleItem::getRuleCode, ruleCode)
                .orderByAsc(RuleItem::getRuleSort));
        List<String> resList = new ArrayList<>();
        for (RuleItem ruleItem : ruleItems) {
            // 判断规则执行条件
            Boolean parserResult = RuleCheckUtils.expressionParser(paramMap, ruleItem.getExecuteCond());
            if (!parserResult.equals(Boolean.TRUE)) {
                log.info("规则 "+ ruleItem.getRuleCode() + " 条件校验不通过，不执行该规则");
                continue;
            }
            // 执行规则引擎
            String beanName = RULE_PRE + ruleItem.getRuleCode();
            RuleCheckEngin ruleCheckEngin = (RuleCheckEngin)ContextUtil.getBean(beanName);
            String check = ruleCheckEngin.check(paramMap);
            if (StringUtils.isNotBlank(check)){
                resList.add(check);
            }
        }

        if (!resList.isEmpty()){
            log.info("校验不通过，原因：" + resList);
            throw new BusinessException(RULE_CHECK_ERROR, resList.toString(), true);
        }
        log.info("规则引擎执行结束，校验通过");
    }
}
