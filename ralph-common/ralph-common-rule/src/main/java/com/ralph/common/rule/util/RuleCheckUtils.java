package com.ralph.common.rule.util;

import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.rule.rule.RalphRule;
import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.List;
import java.util.Map;

public class RuleCheckUtils {

    /**
     * 表达式解析
     * @param map 参数
     * @param expression 表达式
     * @return 解析结果
     */
    public static Boolean expressionParser(Map<String,Object> map, String expression) {
        // 示例：
        // Map<String,String> map = new HashMap<>();
        // map.put("name","Abc");
        // String str = "#map.get(\"name\").equals(\"Abc\") && #map.get(\"name\").length()>3";
        if (StringUtils.isBlank(expression)){
            return Boolean.TRUE;
        }
        SpelExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("map",map);
        return parser.parseExpression(expression).getValue(context,Boolean.class);
    }

    /**
     * 规则检查
     * @param checkCode 规则检查编码
     * @param paramList 检查参数列表
     * @param valueList 检查值列表
     */
    public static void check(String checkCode, List<Object> paramList, List<Object> valueList){
        if (null == paramList || null == valueList){
            throw new BusinessException(ErrorEnum.RULE_CHECK_ERROR, "规则检查请求参数为空",true);
        }
        if (paramList.size() != valueList.size()){
            throw new BusinessException(ErrorEnum.RULE_CHECK_ERROR, "参数长度不一致",true);
        }
        RalphRule ralphRule = (RalphRule)ContextUtil.getBean(checkCode);
        ralphRule.check(paramList,valueList);
    }

}
