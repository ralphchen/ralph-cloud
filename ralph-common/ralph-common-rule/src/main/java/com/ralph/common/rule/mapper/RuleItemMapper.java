package com.ralph.common.rule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.common.rule.entity.RuleItem;
import org.apache.ibatis.annotations.Mapper;

/**
 *  Mapper 接口
 *
 * @author ralph
 * @since 2023-07-12
 */
@Mapper
public interface RuleItemMapper extends BaseMapper<RuleItem> {

}