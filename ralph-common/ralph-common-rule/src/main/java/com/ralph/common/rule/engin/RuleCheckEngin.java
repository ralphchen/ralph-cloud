package com.ralph.common.rule.engin;

import java.util.Map;

public interface RuleCheckEngin {

    String check(Map<String,Object> paramMap);
}
