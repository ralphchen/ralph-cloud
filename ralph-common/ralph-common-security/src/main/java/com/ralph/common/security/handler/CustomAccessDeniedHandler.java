package com.ralph.common.security.handler;

import cn.hutool.json.JSONUtil;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.Result;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 403报错拦截
 */
@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
 
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        Result<String> failed = Result.failed(ErrorEnum.NO_PERMISSION,"");
        // 按照系统自定义结构返回授权失败
        response.getWriter().write(JSONUtil.toJsonStr(failed));
    }
}