package com.ralph.common.security.config;

import cn.hutool.crypto.digest.MD5;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 自定义密码解析器
 */
public class CusPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        // 使用md5进行加密
        return MD5.create().digestHex(charSequence.toString());
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        // 使用md5进行密码匹配，后续可进行扩展匹配其他算法
        return MD5.create().digestHex(charSequence.toString()).equals(s);
    }
}
