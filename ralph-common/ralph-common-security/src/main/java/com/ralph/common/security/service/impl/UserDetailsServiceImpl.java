package com.ralph.common.security.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Sets;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.constant.ServiceConstant;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.entity.UserDTO;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.feign.feigns.AdminFeign;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 登录认证授权
 **/
@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AdminFeign adminFeign;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if(!StringUtils.isNotBlank(username)){
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }

        // 获取用户信息
        Result<UserDTO> userRes = adminFeign.getUserByUsername(username);
        if(null == userRes || userRes.getCode() != ErrorEnum.SUCCESS.getCode() || null == userRes.getData()){
            throw new BusinessException(ErrorEnum.SERVICE_ERROR, ServiceConstant.ADMIN, true);
        }
        UserDTO resUser = userRes.getData();

        // 判断用户锁定状态
        if (resUser.getLockFlag()){
            throw new BusinessException(ErrorEnum.USER_LOCKED);
        }

        // 设置用户角色权限
        resUser.setAuthorities(buildAuthority(resUser));
        return resUser;
    }

    /**
     * 构建角色的authorities
     * @param resUser 用户的角色信息与permission信息
     * @return 构建结果
     */
    private List<GrantedAuthority> buildAuthority(UserDTO resUser){
        Set<String> permissionSet = new HashSet<>();
        if (CollUtil.isNotEmpty(resUser.getPermissions())){
            permissionSet.addAll(resUser.getPermissions());
        }

        // 添加工作流需要的权限，默认所有人都可以使用工作流，如果想要根据配置进行，可以将这行代码取消
        Set<String> roleList = Sets.newHashSet("ROLE_ACTIVITI_USER");
        if (CollUtil.isNotEmpty(resUser.getRoles())){
            roleList.addAll(resUser.getRoles().stream().map(item -> "ROLE_" + item).collect(Collectors.toSet()));
        }
        permissionSet.addAll(roleList);
        return AuthorityUtils.createAuthorityList(permissionSet.toArray(new String[0]));
    }
}
