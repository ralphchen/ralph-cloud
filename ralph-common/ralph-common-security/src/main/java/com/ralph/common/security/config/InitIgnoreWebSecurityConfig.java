package com.ralph.common.security.config;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.ralph.common.core.annotation.IgnoreUrl;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 找到所有加了@Ignore注解的接口，将之忽略
 */
@Configuration
public class InitIgnoreWebSecurityConfig implements ApplicationContextAware {

    private static final Pattern PATTERN = Pattern.compile("\\{(.*?)\\}");

    @Getter
    @Setter
    private List<String> ignoreUrls = new ArrayList<>();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
        for (RequestMappingInfo info : map.keySet()) {
            HandlerMethod handlerMethod = map.get(info);
            // 1. 首先获取类上边 @Inner 注解
            IgnoreUrl annotation = AnnotationUtils.findAnnotation(handlerMethod.getBeanType(), IgnoreUrl.class);

            // 2. 当类上不包含 @Inner 注解则获取该方法的注解
            if (annotation == null) {
                IgnoreUrl method = AnnotationUtils.findAnnotation(handlerMethod.getMethod(), IgnoreUrl.class);
                Optional.ofNullable(method).ifPresent(inner -> info.getPatternsCondition().getPatterns()
                        .forEach(url -> this.filterPath(url, info, map)));
                continue;
            }

            // 3. 当类上包含 @Inner 注解 判断handlerMethod 是否包含在 inner 类中
            Class<?> beanType = handlerMethod.getBeanType();
            Method[] methods = beanType.getDeclaredMethods();
            Method method = handlerMethod.getMethod();
            if (ArrayUtil.contains(methods, method)) {
                info.getPatternsCondition().getPatterns().forEach(url -> filterPath(url, info, map));
            }
        }
        System.out.println();
    }

    /**
     * @param url mapping路径
     * @param info 请求方法
     * @param map 路由映射信息
     */
    private void filterPath(String url, RequestMappingInfo info, Map<RequestMappingInfo, HandlerMethod> map) {
        List<String> methodList = info.getMethodsCondition().getMethods().stream().map(RequestMethod::name)
                .collect(Collectors.toList());
        String resultUrl = ReUtil.replaceAll(url, PATTERN, "*");
        if (CollUtil.isEmpty(methodList)) {
            ignoreUrls.add(resultUrl);
        }
        else {
            ignoreUrls.add(String.format("%s|%s", resultUrl, CollUtil.join(methodList, StrUtil.COMMA)));
        }
    }

    /**
     * 获取对外暴露的URL，注册到 spring security
     * @param registry spring security context
     */
    public void registry(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry) {
        for (String url : getIgnoreUrls()) {
            String[] strings = url.split("\\|");

            // 仅配置对外暴露的URL ，则注册到 spring security的为全部方法
            if (strings.length == 1) {
                registry.antMatchers(strings[0]).permitAll();
                continue;
            }

            // 当配置对外的URL|GET,POST 这种形式，则获取方法列表 并注册到 spring security
            if (strings.length == 2) {
                for (String method : StrUtil.split(strings[1], StrUtil.COMMA)) {
                    registry.antMatchers(HttpMethod.valueOf(method), strings[0]).permitAll();
                }
            }
        }
    }
}
