package com.ralph.common.security.config;

import cn.hutool.core.collection.CollUtil;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.utils.JwtTokenUtil;
import com.ralph.common.core.utils.RedisUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Component
public class JwtAuthenticationTokenFilter  extends OncePerRequestFilter {
    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        // 获取请求头中的token
        String authHeader = httpServletRequest.getHeader(JwtTokenUtil.TOKEN_HEAD_KEY);
        if (StringUtils.isNotEmpty(authHeader) && authHeader.startsWith(JwtTokenUtil.TOKEN_PRE)) {
            // 过滤掉token中的Bearer
            authHeader = authHeader.substring(JwtTokenUtil.TOKEN_PRE.length());
            // 校验token合法性
            JwtTokenUtil.validateTokenBySecret(authHeader);
            // 从token中获取username
            String username = JwtTokenUtil.getUsernameFromToken(authHeader);
            boolean verifyToken = verifyCurrToken(username, authHeader);
            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null && verifyToken) {
                // 根据用户名加载用户信息
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                if (JwtTokenUtil.validateToken(authHeader, userDetails)) {
                    UsernamePasswordAuthenticationToken authentication  =
                    new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    /**
     * 从redis中获取当前用户的token并进行匹配
     * @param username 用户名
     * @param token 当前token
     * @return 当前token是否与redis的token匹配
     */
    private boolean verifyCurrToken(String username, String token){
        if (StringUtils.isBlank(username)){
            return false;
        }
        // 从redis中获取当前用户的key
        List<String> tokenList = RedisUtils.lGetAllStr(RedisKeyConstant.TOKEN_KEY_PRE + username);
        return CollUtil.isNotEmpty(tokenList) && tokenList.contains(token);
    }
}