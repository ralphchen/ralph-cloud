package com.ralph.common.core.constant;

/**
 * 系统通用常量类
 */
public class CommonConstant {

    /**
     * 菜单类型，菜单
     */
    public static final String MENU_TYPE_MENU = "0";

    /**
     * 菜单类型，按钮
     */
    public static final String MENU_TYPE_BUTTON = "1";

    /**
     * 根菜单id
     */
    public static final Integer ROOT_MENU_ID = -1;
}
