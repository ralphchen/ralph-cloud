package com.ralph.common.core.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorEnum {

    /** 通用，0-1000 **/
    SUCCESS(0,"成功"),
    FAILED(1,"失败"),
    /** 全局异常，1001-2000 **/
    SERVICE_ERROR(1001,"服务调用失败"),
    NO_PERMISSION(1002,"无权限访问"),
    PERMISSION_ERROR(1003,"权限认证失败"),
    NO_HANDLE_EXCEPTION(1004,"未处理异常"),
    METHOD_PARAM_NOT_MATCH(1005,"方法参数不匹配"),
    REQUEST_TYPE_NOT_MATCH(1006,"请求类型不匹配"),
    NULL_EXCEPTION(1007,"空指针异常"),
    RULE_CHECK_ERROR(1008,"规则校验失败"),
    /** 数据相关，2001-3000 **/
    DATA_REPEAT(2001,"数据已存在"),
    DATA_NOT_EXISTS(2002,"数据不存在"),
    REQUIRE_PARAM_EMPTY(2003,"缺少必填参数"),
    DATA_TYPE_TRANS_ERROR(2004,"数据类型转换失败"),
    DATA_FORMAT_ERROR(2005,"数据格式错误"),
    /** 业务异常，3001-5001 **/
    TASK_STARTED(3001,"定时任务正进行中"),
    TASK_NO_STARTED(3002,"定时任务还未开始"),
    ANALYSIS_FAILED(3003,"结果解析失败"),
    USER_LOCKED(3004,"用户已锁定"),
    USER_ACCOUNT_EXPIRED(3005,"账号已过期"),
    USER_CREDENTIALS_ERROR(3006,"密码错误"),
    USER_CREDENTIALS_EXPIRED(3007,"密码过期"),
    USER_ACCOUNT_DISABLE(3008,"账号不可用"),
    USER_ACCOUNT_LOCKED(3009,"账号锁定"),
    USER_ACCOUNT_NOT_EXIST(3010,"用户不存在"),
    CAPTCHA_ERROR(3012,"验证码错误，请重新输入"),
    BUCKET_NOT_EXISTS(3013,"bucket不存在"),
    FILE_UPLOAD_ERROR(3014,"文件上传失败"),
    DICT_HAS_ITEM(3015,"该字典有字典项未删除"),
    THIRD_BIND_ERROR(3016,"第三方账号绑定失败"),
    CAPTCHA_COUNT_ERROR(3017,"验证码发送次数过多，请明天再试"),
    CAPTCHA_OFTEN_ERROR(3018,"验证码发送过于频繁，请稍候再试"),
    RESPONSE_PACK_ERROR(3018,"统一包装返回结果失败"),
    DEL_DEFIN_ERROR(3019,"删除流程定义失败，流程下有任务未删除"),
    FILE_DOWNLOAD_ERROR(3020,"文件获取失败"),
    ;

    private Integer code;

    private String msg;
}
