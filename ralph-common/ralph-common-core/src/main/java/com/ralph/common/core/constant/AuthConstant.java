package com.ralph.common.core.constant;

/**
 * 权限认证相关常量类
 */
public class AuthConstant {

    public static final String RESOURCE_ROLES_MAP_KEY = "resourceRolesMapKey";

    public static final String ROLE = "ROLE_";
}
