package com.ralph.common.core.utils;

import com.ralph.common.core.entity.UserDTO;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class SecurityUtils {

	/**
	 * 获取Authentication
	 */
	public static Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	/**
	 * 获取用户
	 * @param authentication
	 * @return PigxUser
	 * <p>
	 */
	public static UserDTO getUser(Authentication authentication) {
		if (null == authentication){
			return new UserDTO();
		}
		Object principal = authentication.getPrincipal();
		if (principal instanceof UserDTO) {
			return (UserDTO) principal;
		}
		return new UserDTO();
	}

	/**
	 * 获取用户
	 */
	public static UserDTO getUser() {
		Authentication authentication = getAuthentication();
		return getUser(authentication);
	}

	public static Integer getCurrentId(){
		return Optional.of(getUser()).map(UserDTO::getId).orElse(null);
	}

	/**
	 * 获取当前登录用户名
	 * @return 用户名
	 */
	public static String getUsername(){
		return Optional.of(getUser()).map(UserDTO::getUsername).orElse("");
	}
}