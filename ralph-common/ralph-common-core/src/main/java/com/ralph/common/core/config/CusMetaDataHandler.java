package com.ralph.common.core.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ralph.common.core.utils.SecurityUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 元数据处理
 * 自动增加创建时间与更新时间
 */
@Component
public class CusMetaDataHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        Date date = new Date();
        this.setFieldValByName("createDate",date,metaObject);
        this.setFieldValByName("updateDate",date,metaObject);
        this.setFieldValByName("createBy", SecurityUtils.getUsername(),metaObject);
        this.setFieldValByName("updateBy",SecurityUtils.getUsername(),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Date date = new Date();
        this.setFieldValByName("updateDate",date,metaObject);
        this.setFieldValByName("updateBy",SecurityUtils.getUsername(),metaObject);
    }
}
