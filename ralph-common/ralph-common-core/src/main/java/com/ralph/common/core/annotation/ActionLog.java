package com.ralph.common.core.annotation;

import java.lang.annotation.*;

/**
 * 接口调用日志记录
 * @author ralph
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ActionLog {

    String value() default "";
}
