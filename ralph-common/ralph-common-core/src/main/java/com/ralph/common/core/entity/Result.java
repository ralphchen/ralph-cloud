package com.ralph.common.core.entity;


import com.ralph.common.core.constant.ErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private int code;

    private String msg;

    private T data;

    public static <T> Result<T> success() {
        return new Result<T>(ErrorEnum.SUCCESS.getCode(),ErrorEnum.SUCCESS.getMsg(),null);
    }

    public static <T> Result<T> success(String msg) {
        return new Result<T>(ErrorEnum.SUCCESS.getCode(),msg,null);
    }

    public static <T> Result<T> success(String msg, T data) {
        return new Result<T>(ErrorEnum.SUCCESS.getCode(),msg,data);
    }

    public static <T> Result<T> success(T data) {
        return new Result<T>(ErrorEnum.SUCCESS.getCode(),ErrorEnum.SUCCESS.getMsg(), data);
    }

    public static <T> Result<T> failed() {
        return new Result<T>(ErrorEnum.FAILED.getCode(),ErrorEnum.FAILED.getMsg(),null);
    }

    public static <T> Result<T> failed(String msg) {
        return new Result<T>(ErrorEnum.FAILED.getCode(),msg,null);
    }

    public static <T> Result<T> failed(String msg, T data) {
        return new Result<T>(ErrorEnum.FAILED.getCode(),msg,data);
    }

    public static <T> Result<T> failed(ErrorEnum errorEnum, T data) {
        return new Result<T>(errorEnum.getCode(),errorEnum.getMsg(),data);
    }

    public static <T> Result<T> failed(ErrorEnum errorEnum, String message, T data) {
        message = StringUtils.isBlank(message)? errorEnum.getMsg(): errorEnum.getMsg() + message;
        return new Result<T>(errorEnum.getCode(), message, data);
    }

    public static <T> Result<T> failed(T data) {
        return new Result<T>(ErrorEnum.FAILED.getCode(),ErrorEnum.FAILED.getMsg(),data);
    }

    public static <T> Result<T> failed(Integer code, String msg) {
        return new Result<T>(code,msg,null);
    }

    public static <T> Result<T> failed(Integer code, String msg, T data) {
        return new Result<T>(code,msg,data);
    }
}
