package com.ralph.common.core.handle;

import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.exceptions.BusinessException;
import feign.RetryableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Result<String> exception(Exception e){
        log.error(e.getMessage());
        e.printStackTrace();
        return Result.failed(ErrorEnum.NO_HANDLE_EXCEPTION, e.getMessage());
    }

    /**
     * 空指针异常处理
     * @param e 空指针异常
     * @return 处理结果
     */
    @ExceptionHandler(NullPointerException.class)
    public Result<NullPointerException> exception(NullPointerException e){
        log.error(ErrorEnum.NULL_EXCEPTION.getMsg() + " " + e.getMessage());
        e.printStackTrace();
        return Result.failed(ErrorEnum.NULL_EXCEPTION, e);
    }

    /**
     * 自定义异常处理
     * @param e 自定义异常
     * @return 处理结果
     */
    @ExceptionHandler(BusinessException.class)
    public Result<String> businessException(BusinessException e){
        log.error(e.getMsg());
        return Result.failed(e.getCode(), e.getMsg(), null);
    }

    /**
     * Security异常处理
     * @param e Security相关异常
     * @return 处理结果
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Result<String> exception(MissingServletRequestParameterException e){
        log.error(ErrorEnum.REQUIRE_PARAM_EMPTY + e.getParameterType()+" "+e.getParameterName());
        return Result.failed(ErrorEnum.REQUIRE_PARAM_EMPTY, e.getParameterType()+" "+e.getParameterName(), e.getMessage());
    }

    /**
     * Security异常处理
     * @param e Security相关异常
     * @return 处理结果
     */
    @ExceptionHandler(RetryableException.class)
    public Result<String> exception(RetryableException e){
        String exceptionMessage = "";
        if (e.getMessage().startsWith("Read timed out")){
            exceptionMessage += "，请求超时";
        }
        log.error(ErrorEnum.SERVICE_ERROR.getMsg() + exceptionMessage + "," + e.getMessage());
        return Result.failed(ErrorEnum.SERVICE_ERROR, exceptionMessage, e.getMessage());
    }

    /**
     * Security异常处理
     * @param e Security相关异常
     * @return 处理结果
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public Result<String> exception(MethodArgumentTypeMismatchException e){
        String className = e.getParameter().getDeclaringClass().getName();
        String methodName = null == e.getParameter().getMethod()? "未找到方法名"
                : e.getParameter().getMethod().getName();
        log.error(ErrorEnum.METHOD_PARAM_NOT_MATCH.getMsg() + ", 类路径：" + className
                + "，方法名：" + methodName + "，异常信息：" + e.getMessage());
        return Result.failed(ErrorEnum.METHOD_PARAM_NOT_MATCH, e.getMessage());
    }

    /**
     * Security异常处理
     * @param e Security相关异常
     * @return 处理结果
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Result<String> exception(HttpRequestMethodNotSupportedException e){
        log.error(ErrorEnum.REQUEST_TYPE_NOT_MATCH.getMsg() + e.getMessage() + "supported: " + e.getSupportedHttpMethods());
        return Result.failed(ErrorEnum.REQUEST_TYPE_NOT_MATCH, e.getMessage());
    }

    /**
     * 参数校验异常处理
     * @param e 异常信息
     * @return 处理结果
     */
    @ExceptionHandler(value = {BindException.class, ValidationException.class, MethodArgumentNotValidException.class})
    public Result<String> handleValidatedException(Exception e) {
        Result<String> result = new Result<>();
        if (e instanceof MethodArgumentNotValidException) {
            // BeanValidation exception
            MethodArgumentNotValidException ex = (MethodArgumentNotValidException) e;
            result = Result.failed(HttpStatus.BAD_REQUEST.value(),
                    ex.getBindingResult().getAllErrors().stream()
                            .map(ObjectError::getDefaultMessage)
                            .collect(Collectors.joining("; "))
            );
        } else if (e instanceof ConstraintViolationException) {
            // BeanValidation GET simple param
            ConstraintViolationException ex = (ConstraintViolationException) e;
            result = Result.failed(HttpStatus.BAD_REQUEST.value(),
                    ex.getConstraintViolations().stream()
                            .map(ConstraintViolation::getMessage)
                            .collect(Collectors.joining("; "))
            );
        } else if (e instanceof BindException) {
            // BeanValidation GET object param
            BindException ex = (BindException) e;
            result = Result.failed(HttpStatus.BAD_REQUEST.value(),
                    ex.getAllErrors().stream()
                            .map(ObjectError::getDefaultMessage)
                            .collect(Collectors.joining("; "))
            );
        }
        return result;
    }
}
