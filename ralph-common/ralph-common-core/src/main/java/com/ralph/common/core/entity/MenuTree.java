package com.ralph.common.core.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MenuTree implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Integer id;

    /**
     * 父菜单id
     */
    private Integer parentId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单编码
     */
    private String menuCode;

    /**
     * 菜单地址
     */
    private String path;

    /**
     * 菜单icon
     */
    private String icon;

    /**
     * 菜单排序
     */
    private Integer sort;

    /**
     * 子菜单集合
     */
    List<MenuTree> children;
}
