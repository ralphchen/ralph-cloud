package com.ralph.common.core.annotation;

import java.lang.annotation.*;

/**
 * 使用该注解的类与接口将不会使用SpringSecurity进行卡权限
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreUrl {
}
