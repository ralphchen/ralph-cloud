package com.ralph.common.core.constant;

public class RedisKeyConstant {

    //============== auth模块redis key ===================//
    /**
     * auth模块redis key前缀
     */
    private static final String AUTH_PRE = "auth:";

    /**
     * token redis key
     */
    public static final String TOKEN_KEY_PRE = AUTH_PRE + "token:";

    /**
     * 手机号验证码发送次数前缀
     */
    public static final String SMS_CAPTCHA_COUNT = AUTH_PRE + "sms:captcha_count:";

    /**
     * 手机号验证码
     */
    public static final String SMS_CAPTCHA = AUTH_PRE + "sms:captcha:";

    //============== admin模块redis key ===================//
    /**
     * admin模块redis key前缀
     */
    private static final String ADMIN_PRE = "admin:";

    /**
     * 动态路由 redis key
     */
    public static final String SYSTEM_ROUTE = ADMIN_PRE + "route";

    /**
     * 字典项缓存key
     */
    public static final String ADMIN_DICT_ITEM = ADMIN_PRE + "dictItems";

    /**
     * 参数缓存key
     */
    public static final String ADMIN_PARAMS = ADMIN_PRE + "params";

    //============== 消息队列redis key ===================//
    /**
     * 系统日志topic
     */
    public static final String SYS_LOG_TYPE_TOPIC = "systemLogTopic";

    /**
     * 动态路由请求topic
     */
    public static final String SYS_ROUTE_REQ_TOPIC = "systemRouteRequestTopic";

    /**
     * 动态路由响应topic
     */
    public static final String SYS_ROUTE_RES_TOPIC = "systemRouteResponseTopic";

}
