package com.ralph.common.core.utils;

import com.ralph.common.core.entity.UserDTO;
import io.jsonwebtoken.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Data
public class JwtTokenUtil implements Serializable {

    /**
     * 生成token的security
     */
    private final static String TOKEN_SECRET = "ralph";

    /**
     * token失效时间
     */
    public final static Long TOKEN_EXPIRATION = 24 * 60 * 60 * 1000L;

    /**
     * token请求头key
     */
    public final static String TOKEN_HEAD_KEY = "Authorization";

    /**
     * TOKEN前缀
     */
    public final static String TOKEN_PRE = "Bearer ";

    private static String generateToken(Map<String, Object> claims) {
        Date expirationDate = new Date(System.currentTimeMillis() + TOKEN_EXPIRATION);
        return Jwts.builder().setClaims(claims).setExpiration(expirationDate).signWith(SignatureAlgorithm.HS512, TOKEN_SECRET).compact();
    }

    private static Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(TOKEN_SECRET).parseClaimsJws(token).getBody();

        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    public static String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>(2);
        claims.put("sub", userDetails.getUsername());
        claims.put("created", new Date());
        return generateToken(claims);
    }

    public static String getUsernameFromToken(String token) {
        String username;
        try {
            Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();

        } catch (Exception e) {
            username = null;

        }
        return username;
    }

    public static Boolean isTokenExpired(String token) {
        try {
            Claims claims = getClaimsFromToken(token);
            Date TOKEN_EXPIREATION = claims.getExpiration();
            return TOKEN_EXPIREATION.before(new Date());
        } catch (Exception e) {
            return false;
        }
    }

    public static String refreshToken(String token) {
        String refreshedToken;
        try {
            Claims claims = getClaimsFromToken(token);
            claims.put("created", new Date());
            refreshedToken = generateToken(claims);

        } catch (Exception e) {
            refreshedToken = null;

        }
        return refreshedToken;
    }

    public static Boolean validateToken(String token, UserDetails userDetails) {
        UserDTO user = (UserDTO) userDetails;
        String username = getUsernameFromToken(token);
        return (username.equals(user.getUsername()) && !isTokenExpired(token));

    }

    /**
     * 校验token是否合法
     * @param authToken token
     */
    public static void validateTokenBySecret(String authToken) {
        try {
            // 通过密钥验证Token
            Jwts.parser().setSigningKey(TOKEN_SECRET).parseClaimsJws(authToken);
        } catch (SignatureException e) {
            log.error("JWT签名异常");
        } catch (MalformedJwtException e) {
            log.error("JWT格式错误");
        } catch (ExpiredJwtException e) {
            log.error("JWT过期");
        } catch (UnsupportedJwtException e) {
            log.error("不支持该JWT");
        } catch (IllegalArgumentException e) {
            log.error("JWT参数错误异常");
        }
    }
}