package com.ralph.common.core.utils;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.DES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.jasypt.util.text.BasicTextEncryptor;

/**
 * 加密工具类
 */
public class EncryptUtils {

  /**
   * 加密key
   */
  private static final String KEY = "KqLWXVdrbgs=";

  /** 根据KEY生成DES */
  private static final DES des = SecureUtil.des(SecureUtil.generateKey(SymmetricAlgorithm.DES.getValue(), KEY.getBytes()).getEncoded());

  /**
   * 获取加密后信息
   *
   * @param plainText 明文
   * @return 加密后信息
   */
  public static String getEncryptData(String plainText) {
    return des.encryptHex(plainText);
  }

  /**
   * 获取解密后信息
   *
   * @param cipherText 密文
   * @return 解密后信息
   */
  public static String getDecryptData(String cipherText) {
    return des.decryptStr(cipherText);
  }

  public static JSONObject decryptDataJson(String str){
    JSONObject resJson;
    String decryptData = getDecryptData(str);
    try {
      resJson = JSONUtil.parseObj(decryptData);
    }catch (Exception e){
      resJson = new JSONObject();
    }
    return resJson;
  }

  public static String encryptConfig(String key, String source){
    BasicTextEncryptor encryptor = new BasicTextEncryptor();
    // 加密密钥
    encryptor.setPassword(key);
    String encrypt = encryptor.encrypt(source);
    System.out.println("encrypt: " + encrypt);
    String decrypt = encryptor.decrypt(encrypt);
    System.out.println("decrypt: " + decrypt);
    return encrypt;
  }

  public static void main(String[] args) {
    encryptConfig("ralph", "123456");
    encryptConfig("ralph", "root");
    encryptConfig("ralph", "vcj1WBFTqNRqmHq1");
    encryptConfig("ralph", "i0uU1uSwQolzrhAJiaAM5jIzaGamacx5");
  }
}

