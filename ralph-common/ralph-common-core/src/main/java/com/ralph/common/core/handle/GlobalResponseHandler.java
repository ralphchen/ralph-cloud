package com.ralph.common.core.handle;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ralph.common.core.annotation.NotResHandler;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.exceptions.BusinessException;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 全局返回结果封装类
 */
@RestControllerAdvice(basePackages = "com.ralph.*.controller")
public class GlobalResponseHandler implements ResponseBodyAdvice<Object> {

    /**
     * 满足条件的才进行封装
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        // response是ResultVo类型，或者注释了NotControllerResponseAdvice都不进行包装
        return !methodParameter.getParameterType().isAssignableFrom(Result.class)
                // && !"void".equals(methodParameter.getParameterType().getName())
                && !methodParameter.hasMethodAnnotation(NotResHandler.class);
    }

    /**
     * 封装返回结果
     */
    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        // String类型不能直接包装
        if (methodParameter.getGenericParameterType().equals(String.class)) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                // 将数据包装在ResultVo里后转换为json串进行返回
                return objectMapper.writeValueAsString(Result.success(o));
            } catch (JsonProcessingException e) {
                throw new BusinessException(ErrorEnum.RESPONSE_PACK_ERROR);
            }
        }
        // 否则直接包装成ResultVo返回
        return Result.success(o);
    }
}
