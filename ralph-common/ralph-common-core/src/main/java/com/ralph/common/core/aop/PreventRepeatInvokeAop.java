package com.ralph.common.core.aop;


import cn.hutool.extra.servlet.ServletUtil;
import com.ralph.common.core.annotation.PreventRepeatInvoke;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.core.utils.RedisUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 防止重复调用aop
 */
@Aspect
@Component
@Slf4j
public class PreventRepeatInvokeAop {

    /**
     * 切入点
     */
    @Pointcut("@annotation(com.ralph.common.core.annotation.PreventRepeatInvoke)")
    public void pointcut() {
    }

    /**
     * 处理前
     *
     * @return
     */
    @Before("pointcut()")
    public void joinPoint(JoinPoint joinPoint) throws Exception {
        Object[] params = joinPoint.getArgs();

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = joinPoint.getTarget().getClass().getMethod(methodSignature.getName(),
                methodSignature.getParameterTypes());

        PreventRepeatInvoke preventAnnotation = method.getAnnotation(PreventRepeatInvoke.class);
        String methodFullName = method.getDeclaringClass().getName() +"."+ method.getName();

        defaultHandle(params, preventAnnotation, methodFullName);
        return;
    }

    /**
     * 默认处理方式
     *
     * @param params
     * @param prevent
     */
    private void defaultHandle(Object[] params, PreventRepeatInvoke prevent,String methodFullName) {
        // 获取请求信息
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String ipAddress = ServletUtil.getClientIP(request);
        String key = ipAddress + ":" + methodFullName;
        long expire = Long.parseLong(prevent.value());

        boolean resp = RedisUtils.hasKey(key);
        if (!resp) {
            RedisUtils.set(key, params, expire);
        } else {
            String message = !StringUtils.isEmpty(prevent.message()) ? prevent.message() :
                    expire + "秒内不允许重复请求";
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY, message);
        }
    }
}
