package com.ralph.common.core.annotation;

import java.lang.annotation.*;

/**
 * 防止接口重复调用
 * @author ralph
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PreventRepeatInvoke {
    /**
     * 限制的时间值（秒）
     */
    String value() default "60";

    /**
     * 提示
     */
    String message() default "";
}
