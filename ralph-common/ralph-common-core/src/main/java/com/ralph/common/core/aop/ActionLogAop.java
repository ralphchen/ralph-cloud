package com.ralph.common.core.aop;

import cn.hutool.core.net.URLDecoder;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.entity.SystemLog;
import com.ralph.common.core.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Aspect
@Component
@RequiredArgsConstructor
@Slf4j
public class ActionLogAop {

    @Around("@annotation(actionLog)")
    public Object around(ProceedingJoinPoint point, ActionLog actionLog) throws Throwable {
        SystemLog systemLog = initLog(actionLog);
        // 发送异步日志事件
        long startTime = System.currentTimeMillis();
        Object obj = point.proceed();
        long endTime = System.currentTimeMillis();
        systemLog.setTime((int)(endTime-startTime));
        systemLog.setResult(null == obj? null: JSONUtil.toJsonStr(obj));
        //发送
        RedisUtils.convertAndSend(RedisKeyConstant.SYS_LOG_TYPE_TOPIC,systemLog);
        return obj;
    }

    @AfterThrowing(value = "@annotation(actionLog)",throwing = "e")
    public void afterThrow(JoinPoint joinPoint, Exception e, ActionLog actionLog){
        SystemLog systemLog = initLog(actionLog);
        // 发送异步日志事件
        systemLog.setResult(e.toString());
        // 发送异步日志事件
        RedisUtils.convertAndSend(RedisKeyConstant.SYS_LOG_TYPE_TOPIC,systemLog);
    }

    private SystemLog initLog(ActionLog actionLog){
        SystemLog systemLog = new SystemLog();
        systemLog.setAction(actionLog.value());
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        // 如果使用localhost访问，ip会展示0:0:0:0:0:0:0:1
        // 如果是本地访问，可以使用127.0.0.1进行访问
        systemLog.setIpAddress(ServletUtil.getClientIP(request));
        systemLog.setUri(URLUtil.getPath(request.getRequestURI()));
        systemLog.setRequestType(request.getMethod());
        String params = URLDecoder.decode(HttpUtil.toParams(request.getParameterMap()), CharsetUtil.CHARSET_UTF_8);
        systemLog.setParams(params);
        return systemLog;
    }
}
