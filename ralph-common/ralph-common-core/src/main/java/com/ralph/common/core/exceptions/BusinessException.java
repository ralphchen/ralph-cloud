package com.ralph.common.core.exceptions;


import com.ralph.common.core.constant.ErrorEnum;
import lombok.Data;

@Data
public class BusinessException extends RuntimeException{

    private Integer code;

    private String msg;

    /**
     * 抛出异常
     * @param errorEnum 错误枚举
     */
    public BusinessException(ErrorEnum errorEnum){
        super(errorEnum.getMsg());
        this.code = errorEnum.getCode();
        this.msg = errorEnum.getMsg();
    }

    /**
     * 使用自定义报错信息替换错误枚举错误信息
     * @param errorEnum 错误枚举
     * @param msg 报错信息
     */
    public BusinessException(ErrorEnum errorEnum, String msg){
        super(msg);
        this.code = errorEnum.getCode();
        this.msg = msg;
    }

    /**
     *
     * @param errorEnum 错误枚举
     * @param msg 错误信息
     * @param suffix true为加在错误信息后缀，false为前缀
     */
    public BusinessException(ErrorEnum errorEnum, String msg, boolean suffix){
        super(errorEnum.getMsg());
        this.code = errorEnum.getCode();
        if (suffix){
            this.msg = errorEnum.getMsg() + "-" + msg;
        }else {
            this.msg = msg + "-" + errorEnum.getMsg();
        }
    }

    /**
     * 使用占位符
     * @param errorEnum 错误枚举
     * @param msg 替换占位符字符串数组
     */
    public BusinessException(ErrorEnum errorEnum, String... msg){
        super(errorEnum.getMsg());
        this.code = errorEnum.getCode();
        String message = errorEnum.getMsg();
        for (int i = 0; i < msg.length; i++) {
            message = message.replaceAll("\\{"+i+"\\}",msg[i]);
        }
        this.msg = message;
    }
}
