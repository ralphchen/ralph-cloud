package com.ralph.common.core.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

@Data
public class UserDTO implements UserDetails {

    private static final long serialVersionUID = 1L;

    private String token;

    private Integer id;

    private String username;

    private String password;

    private String phone;

    private String email;

    private String pwdUpdateTime;

    private Boolean lockFlag;

    /**
     * 角色编码列表
     */
    private Set<String> roles;

    /**
     * 菜单权限列表
     */
    private Set<String> permissions;

    /**
     * 菜单树
     */
    private MenuTree menuTree;

    /**
     * 第三方登录信息
     */
    private String thirdUserInfo;

    private Collection<? extends GrantedAuthority> authorities;

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
