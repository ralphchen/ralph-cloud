package com.ralph.common.core.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 操作日志
 * </p>
 *
 * @author ralph
 * @since 2022-12-16
 */
@Data
@TableName("system_log")
@ApiModel(value = "操作日志")
public class SystemLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id",type = IdType.AUTO)
    @ApiModelProperty(value="主键")
    private Integer id;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建人")
    private String createBy;

    /**
     * 操作
     */
    @ApiModelProperty(value="操作")
    private String action;

    /**
     * 请求结果
     */
    @ApiModelProperty(value="请求结果")
    private String result;

    /**
     * ip地址
     */
    @ApiModelProperty(value="ip地址")
    private String ipAddress;

    /**
     * 请求路径
     */
    @ApiModelProperty(value="请求路径")
    private String uri;

    /**
     * 请求方式
     */
    @ApiModelProperty(value="请求方式")
    private String requestType;

    /**
     * 请求参数
     */
    @ApiModelProperty(value="请求参数")
    private String params;

    /**
     * 耗费时间
     */
    @ApiModelProperty(value="耗费时间")
    private Integer time;
}

