package com.ralph.common.docs.support;

import cn.hutool.core.collection.CollUtil;
import com.fasterxml.classmate.ResolvedType;
import com.google.common.collect.Lists;
import com.ralph.common.docs.annotation.ApiIgnoreField;
import com.ralph.common.docs.annotation.ApiNeedField;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import springfox.documentation.builders.BuilderDefaults;
import springfox.documentation.builders.OperationBuilder;
import springfox.documentation.common.Compatibility;
import springfox.documentation.schema.Collections;
import springfox.documentation.schema.Maps;
import springfox.documentation.schema.ScalarTypes;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.service.ResolvedMethodParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.EnumTypeDeterminer;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spi.service.contexts.ParameterContext;
import springfox.documentation.spring.web.plugins.DocumentationPluginsManager;
import springfox.documentation.spring.web.readers.operation.OperationParameterReader;
import springfox.documentation.spring.web.readers.operation.ParameterAggregator;
import springfox.documentation.spring.web.readers.parameter.ExpansionContext;
import springfox.documentation.spring.web.readers.parameter.ModelAttributeParameterExpander;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Order(Integer.MIN_VALUE)
@Slf4j
public class RalphCusParamHandler implements OperationBuilderPlugin {

    @Autowired
    public RalphCusParamHandler(ModelAttributeParameterExpander expander, EnumTypeDeterminer enumTypeDeterminer, ParameterAggregator aggregator) {
        this.expander = expander;
        this.enumTypeDeterminer = enumTypeDeterminer;
        this.aggregator = aggregator;
    }
    private static final Logger LOGGER = LoggerFactory.getLogger(OperationParameterReader.class);
    private final ModelAttributeParameterExpander expander;
    private final EnumTypeDeterminer enumTypeDeterminer;
    private final ParameterAggregator aggregator;

    private boolean change = false;
    @Autowired
    private DocumentationPluginsManager pluginsManager;

    public void apply(OperationContext context) {
        change = false;
        context.operationBuilder().parameters(context.getGlobalOperationParameters());
        List<Compatibility<Parameter, RequestParameter>> compatibilities = this.readParameters(context);

        if (change) {
            // 反射给parameters赋值
            try {
                Field parametersField = OperationBuilder.class.getDeclaredField("parameters");
                parametersField.setAccessible(true);
                List<Parameter> parameters = compatibilities.stream().map(Compatibility::getLegacy).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
                List<Parameter> source = BuilderDefaults.nullToEmptyList(parameters);
                parametersField.set(context.operationBuilder(), source);

                Field requestParameters = OperationBuilder.class.getDeclaredField("requestParameters");
                requestParameters.setAccessible(true);
                Set<RequestParameter> requestParametersList = compatibilities.stream().map(Compatibility::getModern).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toSet());
                Set<RequestParameter> requestSource = BuilderDefaults.nullToEmptySet(requestParametersList);
                requestParameters.set(context.operationBuilder(), requestSource);

            } catch (Exception e) {
                log.error("动态更改swagger参数错误", e);
            }
        }else {
            context.operationBuilder().parameters((List)compatibilities.stream().map(Compatibility::getLegacy).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
            context.operationBuilder().requestParameters(new HashSet(context.getGlobalRequestParameters()));
            Collection<RequestParameter> requestParameters = (Collection)compatibilities.stream().map(Compatibility::getModern).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toSet());
            context.operationBuilder().requestParameters(this.aggregator.aggregate(requestParameters));
        }
    }

    public boolean supports(DocumentationType delimiter) {
        return true;
    }

    private List<Compatibility<Parameter, RequestParameter>> readParameters(OperationContext context) {
        List<ResolvedMethodParameter> methodParameters = context.getParameters();
        List<Compatibility<Parameter, RequestParameter>> parameters = new ArrayList();
        LOGGER.debug("Reading parameters for method {} at path {}", context.getName(), context.requestMappingPattern());
        int index = 0;
        Iterator var5 = methodParameters.iterator();

        while(var5.hasNext()) {
            ResolvedMethodParameter methodParameter = (ResolvedMethodParameter)var5.next();

            Optional<ApiIgnoreField> ignoreFieldAnnotation = methodParameter.findAnnotation(ApiIgnoreField.class);
            Optional<ApiNeedField> needFieldAnnotation = methodParameter.findAnnotation(ApiNeedField.class);
            List<String> needList = new ArrayList<>();
            List<String> ignoreList = new ArrayList<>();
            if (needFieldAnnotation.isPresent()) {
                String[] fields = needFieldAnnotation.get().value();
                needList.addAll(Lists.newArrayList(fields));
            }else if (ignoreFieldAnnotation.isPresent()){
                String[] fields = ignoreFieldAnnotation.get().value();
                ignoreList.addAll(Lists.newArrayList(fields));
            }

            LOGGER.debug("Processing parameter {}", methodParameter.defaultName().orElse("<unknown>"));
            ResolvedType alternate = context.alternateFor(methodParameter.getParameterType());
            if (!this.shouldIgnore(methodParameter, alternate, context.getIgnorableParameterTypes())) {
                ParameterContext parameterContext = new ParameterContext(methodParameter, context.getDocumentationContext(), context.getGenericsNamingStrategy(), context, index++);
                if (this.shouldExpand(methodParameter, alternate)) {
                    List<Compatibility<Parameter, RequestParameter>> expand = this.expander.expand(new ExpansionContext("", alternate, context));
                    // 处理需要和不需要字段
                    if (CollUtil.isNotEmpty(needList)){
                        expand = expand.stream().filter(item->{
                            String modelName = item.getModern().map(RequestParameter::getName).orElse("");
                            String legacyName = item.getLegacy().map(Parameter::getName).orElse("");
                            return needList.stream().anyMatch(need-> need.matches(modelName) && need.matches(legacyName));
                        }).collect(Collectors.toList());
                        change = true;
                    }else if (CollUtil.isNotEmpty(ignoreList)){
                        expand = expand.stream().filter(item->{
                            String modelName = item.getModern().map(RequestParameter::getName).orElse("");
                            String legacyName = item.getLegacy().map(Parameter::getName).orElse("");
                            return ignoreList.stream().noneMatch(ignore-> ignore.matches(modelName) && ignore.matches(legacyName));
                        }).collect(Collectors.toList());
                        change = true;
                    }
                    // 对象类型的参数都为非必填
                    expand.forEach(item->{
                        if (item.getModern().isPresent() && item.getLegacy().isPresent() &&
                                (item.getModern().get().getRequired() || item.getLegacy().get().isRequired())){
                            try {
                                Field requiredParameter = Parameter.class.getDeclaredField("required");
                                requiredParameter.setAccessible(true);
                                requiredParameter.set(item.getLegacy().get(),Boolean.FALSE);

                                Field required = RequestParameter.class.getDeclaredField("required");
                                required.setAccessible(true);
                                required.set(item.getModern().get(),Boolean.FALSE);
                            } catch (Exception e){
                                log.debug("修改字段必填值错误");
                            }
                        }
                    });
                    parameters.addAll(expand);
                } else {
                    parameters.add(this.pluginsManager.parameter(parameterContext));
                }
            }

        }
        return (List)parameters.stream().filter(this.hiddenParameter().negate()).collect(Collectors.toList());
    }

    private List<Compatibility<Parameter, RequestParameter>> parameterRequire(List<Compatibility<Parameter, RequestParameter>> extend){
        for (Compatibility<Parameter, RequestParameter> item : extend) {
            Optional<Parameter> legacy = item.getLegacy();
            Optional<RequestParameter> modern = item.getModern();

        }
        return extend;
    }

    private Predicate<Compatibility<Parameter, RequestParameter>> hiddenParameter() {
        return (c) -> {
            return (Boolean)c.getLegacy().map(Parameter::isHidden).orElse(false);
        };
    }

    private boolean shouldIgnore(
            final ResolvedMethodParameter parameter,
            ResolvedType resolvedParameterType,
            final Set<Class> ignorableParamTypes) {
        if (ignorableParamTypes.contains(resolvedParameterType.getErasedType())) {
            return true;
        } else {
            Stream var10000 = ignorableParamTypes.stream();
            Objects.requireNonNull(Annotation.class);
            var10000 = var10000.filter(item-> Annotation.class.isAssignableFrom((Class<?>) item));
            Objects.requireNonNull(parameter);
            return var10000.anyMatch(item->parameter.hasParameterAnnotation((Class<? extends Annotation>) item));
        }
    }

    private boolean shouldExpand(final ResolvedMethodParameter parameter, ResolvedType resolvedParamType) {
        return !parameter.hasParameterAnnotation(RequestBody.class) && !parameter.hasParameterAnnotation(RequestPart.class) && !parameter.hasParameterAnnotation(RequestParam.class) && !parameter.hasParameterAnnotation(PathVariable.class) && !ScalarTypes.builtInScalarType(resolvedParamType.getErasedType()).isPresent() && !this.enumTypeDeterminer.isEnum(resolvedParamType.getErasedType()) && !Collections.isContainerType(resolvedParamType) && !Maps.isMapType(resolvedParamType);
    }
}
