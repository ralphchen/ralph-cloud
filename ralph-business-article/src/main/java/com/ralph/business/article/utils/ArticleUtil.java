package com.ralph.business.article.utils;

import com.ralph.business.article.constant.ArticleConstants;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.core.constant.ErrorEnum;

/**
 * 业务工具类
 */
public class ArticleUtil {

    /**
     * 获取当前阅读量
     * @param body 响应结果
     * @return 阅读数
     */
    public static int getReadCount(String body) {
        int res = 0;
        try {
            // 下标
            int index = body.indexOf(ArticleConstants.READCOUNTPREFIX) + ArticleConstants.READCOUNTPREFIX.length();
            StringBuilder resStr = new StringBuilder();
            while (true) {
                char ch = body.charAt(index);
                if (ch == '<') {
                    break;
                }
                resStr.append(ch);
                index++;
            }
            res = Integer.parseInt(resStr.toString());
        }catch (Exception e){
            return 0;
        }
        return res;
    }

    /**
     * 获取文章名称
     * @param body 响应结果
     * @return 查询结果
     */
    public static String getArticleName(String body) {
        try {
            int index = body.indexOf(ArticleConstants.TITLEPREFIX) + ArticleConstants.TITLEPREFIX.length();
            StringBuilder resStr = new StringBuilder();
            while (true) {
                char ch = body.charAt(index);
                if (ch == '"') {
                    break;
                }
                resStr.append(ch);
                index++;
            }
            return resStr.toString();
        }catch (Exception e){
            throw new BusinessException(ErrorEnum.ANALYSIS_FAILED, "文章名称解析失败", true);
        }
    }
}
