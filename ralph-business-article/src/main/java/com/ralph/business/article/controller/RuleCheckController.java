package com.ralph.business.article.controller;

import com.ralph.business.article.service.RuleChecTestkService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@Api(value = "rule/check", tags = "模板替换接口")
@RequestMapping("/rule/check")
public class RuleCheckController {

    private final RuleChecTestkService ruleChecTestkService;

    @GetMapping("/engin")
    public void engin(){

    }

    @GetMapping("util")
    public void util(){
        ruleChecTestkService.utilTest();
    }
}
