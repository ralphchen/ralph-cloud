package com.ralph.business.article.service.impl;

import com.ralph.business.article.utils.Html2PdfUtil;
import org.springframework.stereotype.Service;

@Service
public class Html2PdfServiceImpl {

    public static void main(String[] args) throws Exception {
        String htmlFilePath = "E:\\TemFile\\Html2pdf\\htmlStr.html";
        String pdfFilePath = "E:\\TemFile\\Html2pdf\\html2pdf.pdf";
        Html2PdfUtil.html2pdf(htmlFilePath, pdfFilePath);
    }
}
