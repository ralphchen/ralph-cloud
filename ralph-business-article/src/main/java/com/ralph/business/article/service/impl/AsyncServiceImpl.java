package com.ralph.business.article.service.impl;

import cn.hutool.http.HttpUtil;
import com.ralph.business.article.service.AsyncService;
import com.ralph.business.article.utils.ArticleUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.ralph.business.article.entity.BusinessArticle;
import com.ralph.business.article.mapper.BusinessArticleMapper;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
public class AsyncServiceImpl implements AsyncService {

    private final BusinessArticleMapper businessArticleMapper;

    @Override
    @Async
    public void invokeInterface(BusinessArticle article){
        String response = HttpUtil.get(article.getUrl());
        article.setReadCount(ArticleUtil.getReadCount(response));
        businessArticleMapper.updateById(article);
    }

    @Override
    @Async
    public void saveArticle(String url){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);
        String articleName = ArticleUtil.getArticleName(Objects.requireNonNull(forEntity.getBody()));
        BusinessArticle article = new BusinessArticle();
        article.setName(articleName);
        article.setUrl(url);
        article.setReadCount(ArticleUtil.getReadCount(forEntity.getBody()));
        businessArticleMapper.insert(article);
        log.info("url added successfully");
    }
}
