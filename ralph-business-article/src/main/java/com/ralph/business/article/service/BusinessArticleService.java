package com.ralph.business.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.business.article.entity.BusinessArticle;
import com.ralph.common.core.entity.Result;

import java.util.List;

/**
 * <p>
 * 业务表-文章主体表 服务类
 * </p>
 *
 * @author ralph
 * @since 2022-12-15
 */
public interface BusinessArticleService extends IService<BusinessArticle> {

    /**
     * 添加文章
     * @param urlList 文章url
     */
    Result<Boolean> addArticle(List<String> urlList);
}

