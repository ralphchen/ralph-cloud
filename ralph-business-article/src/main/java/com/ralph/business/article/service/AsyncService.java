package com.ralph.business.article.service;

import com.ralph.business.article.entity.BusinessArticle;

public interface AsyncService {

    void saveArticle(String url);

    void invokeInterface(BusinessArticle article);
}
