package com.ralph.business.article.controller;


import cn.hutool.http.server.HttpServerRequest;
import com.ralph.business.article.service.FreeMarkerSerivce;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequiredArgsConstructor
@RestController
@Api(value = "freeemarker", tags = "模板替换接口")
@RequestMapping("/freeemarker")
public class FreeMarkerController {

    private final FreeMarkerSerivce freeMarkerSerivce;

    @PostMapping("/freemarker")
    public void sigle(HttpServerRequest httpServerRequest, MultipartFile file, String template){
        freeMarkerSerivce.genFile(httpServerRequest, file, template);
    }

}
