package com.ralph.business.article.constant;

public final class ArticleConstants {

	private ArticleConstants() {
	}

	// 文章名称下标索引
	public static final String TITLEPREFIX = "articleTitle = \"";

	// 阅读量前缀标识
	public static final String READCOUNTPREFIX = "<span class=\"read-count\">";

	// 搜索url前缀
	public static final String SEARCHPREFIX = "https://so.csdn.net/api/v3/search";

	// 搜索url后缀
	public static final String SEARCHSUFFIX = "&t=all&p=1&s=0&tm=0&lv=-1&ft=0&l=&u=&ct=-1&pnt=-1&ry=-1&ss=-1&dct=-1&vco=-1&cc=-1&sc=-1&akt=-1&art=-1&ca=-1&prs=&pre=&ecc=-1&ebc=-1&urw=&ia=1&platform=pc";
}
