package com.ralph.business.article.service.impl;

import com.google.common.collect.Lists;
import com.ralph.business.article.service.RuleChecTestkService;
import com.ralph.common.rule.contant.RalphRuleConstant;
import com.ralph.common.rule.util.RuleCheckUtils;
import org.springframework.stereotype.Service;

@Service
public class RuleCheckTestServiceImpl implements RuleChecTestkService {

    @Override
    public void utilTest(){
        RuleCheckUtils.check(RalphRuleConstant.ALL_NOT_BLANK, Lists.newArrayList("姓名"), Lists.newArrayList("张三"));
    }

}
