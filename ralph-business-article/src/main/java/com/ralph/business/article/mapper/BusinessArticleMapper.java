package com.ralph.business.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.ralph.business.article.entity.BusinessArticle;

/**
 * <p>
 * 业务表-文章主体表 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2022-12-15
 */
@Mapper
public interface BusinessArticleMapper extends BaseMapper<BusinessArticle> {

}