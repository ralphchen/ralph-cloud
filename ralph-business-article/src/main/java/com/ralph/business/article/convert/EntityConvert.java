package com.ralph.business.article.convert;


import com.ralph.business.article.entity.BusinessArticle;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

/**
 * 实体类转换接口，即使字段名称，类型不一致，也可实现转换
 *
 * 可能存在问题一：
 * Idea2022.1编译报错：
 * java: Internal error in the mapping processor: java.lang.NullPointerException
 * 解决方法：
 * Setting -->Build,Execution,Deployment -->Compiler -->User-local build加上参数：
 * -Djps.track.ap.dependencies=false
 * 可能存在问题二：
 * java: No property named "name" exists in source parameter(s). Did you mean "id"?
 * 原因是lombok与mapstruct的加载顺序问题
 * 解决方法：
 * 1、将lombok的依赖放在mapstruct前面
 * 2、更新lombok与mapstruct的版本，使两者版本适配
 * 3、在pom文件中增加binding插件，具体内容自行百度
 */
// @Mapper(componentModel = "spring",uses = {CusMapper.class})
// 使用main方法测试时，不需要注入到spring中，否则会找不到自定义映射类
@Mapper()
public interface EntityConvert {

    /**
     * 获取该类自动生成的实现类的实例
     * 接口中的属性都是 public static final 的 方法都是public abstract的
     */
    EntityConvert INSTANCES = Mappers.getMapper(EntityConvert.class);

    @Mappings({
            @Mapping(target = "parentId", expression = "java(source.getParentId().toString())"),
            @Mapping(source = "name", target = "realName"),
            @Mapping(source = "createDate", target = "updateDate", dateFormat = "yyyy-MM-dd HH:mm:ss")
    })
    TargetEntity toTargetEntity(SourceEntity source);

    /**
     * 逆转换，加了注解不需要重新写@Mapping
     * @param source source
     * @return target
     */
    @InheritInverseConfiguration(name = "toTargetEntity")
    SourceEntity toTargetEntity(TargetEntity source);

    /**
     * 合并转换
     * @param source 实体类一
     * @param article 实体类二
     * @return 合并结果
     */
    @Mappings({
            @Mapping(target = "parentId", expression = "java(source.getParentId().toString())"),
            @Mapping(source = "article.name", target = "realName", defaultValue = "defaultName"),
            @Mapping(source = "article.id", target = "id"),
            @Mapping(source = "source.createDate", target = "updateDate", dateFormat = "yyyy-MM-dd HH:mm:ss")
    })
    TargetEntity mergeEntity(SourceEntity source, BusinessArticle article);

    /**
     * 默认值
     * @param source 实体类
     * @return 转换结果
     */
    @Mapping(source = "name", target = "realName", defaultValue = "defaultName")
    TargetEntity defaultValue(SourceEntity source);

    @Mapping(source = "sexInt", target = "sex", qualifiedByName = "sexFormat")
    TargetEntity sexMapper(SourceEntity source);

    @Named("sexFormat")
    static String toSex(Integer sex){
        if (0 == sex){
            return "男";
        }else {
            return "女";
        }
    }
}
