package com.ralph.business.article.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.ralph.common.core.entity.BaseEntity;

/**
 * <p>
 * 业务表-文章主体表
 * </p>
 *
 * @author ralph
 * @since 2022-12-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("business_article")
@ApiModel(value = "业务表-文章主体表")
public class BusinessArticle extends BaseEntity<BusinessArticle> {

    private static final long serialVersionUID = 1L;

    /**
     * 文章url
     */
    @ApiModelProperty(value="文章url")
    private String url;

    /**
     * 文章名称
     */
    @ApiModelProperty(value="文章名称")
    private String name;

    /**
     * 阅读次数
     */
    @ApiModelProperty(value="阅读次数")
    private Integer readCount;


}

