package com.ralph.business.article.convert;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 测试Mapstruct类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SourceEntity{
    private Integer id;

    private Integer parentId;

    private String name;

    private Integer sexInt;

    private LocalDateTime createDate;
}