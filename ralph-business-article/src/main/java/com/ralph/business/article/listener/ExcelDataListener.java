package com.ralph.business.article.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;

public class ExcelDataListener<T> extends AnalysisEventListener<T> {
    // 省略其他未用到重写方法
	/**
	 * 自定义用于暂时存储data
	 * 可以通过实例获取该值
	 */
	private List<T> datas = new ArrayList<>();

	@Override
	public void invoke(T t, AnalysisContext analysisContext) {
        // 每条数据解析都会调用这个方法
		// 数据存储到list，供批量处理，或后续自己业务逻辑处理。
		datas.add(t);
	}
    
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        // 全部数据解析完成会调用这个方法
        // saveData();
    }

	public List<T> getDatas(){
		return datas;
	}
}
