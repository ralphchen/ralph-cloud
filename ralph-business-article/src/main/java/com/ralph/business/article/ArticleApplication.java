package com.ralph.business.article;

import com.ralph.common.docs.annotation.EnableRalphSwagger2;
import com.ralph.common.job.annotation.EnableRalphXxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
@Slf4j
@EnableRalphXxlJob
@EnableScheduling
@EnableRalphSwagger2
@SpringBootApplication
public class ArticleApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ArticleApplication.class, args);
        Environment environment = context.getBean(Environment.class);
        log.info("\n\t---------------------------------------------------"
                + "\n\tApplication " + environment.getProperty("spring.application.name")
                + " is running!\n\tlocal：http://localhost:" + environment.getProperty("server.port")
                +"\n\t---------------------------------------------------");
    }
}
