package com.ralph.business.article.service;

import cn.hutool.http.server.HttpServerRequest;
import org.springframework.web.multipart.MultipartFile;

public interface FreeMarkerSerivce {

    void genFile(HttpServerRequest httpServerRequest, MultipartFile file, String template);
}
