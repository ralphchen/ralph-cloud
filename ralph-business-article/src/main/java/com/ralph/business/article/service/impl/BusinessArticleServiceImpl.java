package com.ralph.business.article.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.business.article.entity.BusinessArticle;
import com.ralph.business.article.mapper.BusinessArticleMapper;
import com.ralph.business.article.service.AsyncService;
import com.ralph.business.article.service.BusinessArticleService;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.exceptions.BusinessException;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 业务表-文章主体表 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2022-12-15
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class BusinessArticleServiceImpl extends ServiceImpl<BusinessArticleMapper, BusinessArticle> implements BusinessArticleService {

    private final AsyncService asyncService;


    /**
     * 添加文章
     * @param urlList 文章url列表
     */
    @Transactional
    public Result<Boolean> addArticle(List<String> urlList) {
        // 验重
        List<BusinessArticle> existList = super.list(Wrappers.<BusinessArticle>lambdaQuery()
                .in(BusinessArticle::getUrl, urlList));
        if (CollUtil.isNotEmpty(existList)){
            List<String> repeatList = existList.stream().map(BusinessArticle::getUrl)
                    .filter(urlList::contains).collect(Collectors.toList());
            throw new BusinessException(ErrorEnum.DATA_REPEAT,"重复url："+repeatList,true);
        }
        try {
            for (String url : urlList) {
                asyncService.saveArticle(url);
            }
        } catch (Exception e) {
            throw new BusinessException(ErrorEnum.FAILED);
        }
        return Result.success("添加成功", Boolean.TRUE);
    }

    /**
     * 执行定时任务
     */
    @XxlJob("article_invoke")
    public ReturnT<String> clickText(String param) {
        log.info(param);
        List<BusinessArticle> articleList = super.list();
        for (BusinessArticle article : articleList) {
            asyncService.invokeInterface(article);
        }
        return ReturnT.SUCCESS;
    }
}

