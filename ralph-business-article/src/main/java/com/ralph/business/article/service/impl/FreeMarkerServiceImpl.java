package com.ralph.business.article.service.impl;

import cn.hutool.http.server.HttpServerRequest;
import com.alibaba.excel.EasyExcel;
import com.ralph.business.article.entity.ExcelEntity;
import com.ralph.business.article.listener.ExcelDataListener;
import com.ralph.business.article.service.FreeMarkerSerivce;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FreeMarkerServiceImpl implements FreeMarkerSerivce {

    /**
     *
     * @param httpServerRequest
     * @param file
     * @param template
     */
    @Override
    public void genFile(HttpServerRequest httpServerRequest, MultipartFile file, String template){
        List<ExcelEntity> entityList = new ArrayList<>();
        try {
            entityList = uploadByReturn(file);
        } catch (Exception e){}

        for (ExcelEntity entity : entityList) {
            String analysis = analysis(template, entity);
            System.out.println(analysis);
        }
    }

    /**
     * 解析文件内容
     * @param file 文件
     * @return 文件解析结果
     * @throws IOException
     */
    public List<ExcelEntity> uploadByReturn(MultipartFile file) throws IOException {
        ExcelDataListener<ExcelEntity> listener = new ExcelDataListener<>();
        EasyExcel.read(file.getInputStream(), ExcelEntity.class, listener).sheet(0).doRead();
        // excel数据解析结果
        return listener.getDatas();
    }

    /**
     * 组装模板内容
     * @param templateStr 模板字符串
     * @param entity 替换内容
     * @return 替换结果
     */
    public String analysis(String templateStr, ExcelEntity entity) {
        try (StringWriter out = new StringWriter()) {
            Configuration configuration = new Configuration(Configuration.VERSION_2_3_0);
            Template template = new Template("模板名称", templateStr, configuration);
            template.process(entity, out);
            return out.toString();
        }catch (Exception e){}
        return null;
    }
}
