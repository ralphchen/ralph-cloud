package com.ralph.business.article.convert;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 测试Mapstruct类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TargetEntity{
    private Integer id;

    private String parentId;

    private String realName;

    private String sex;

    private Date updateDate;
}