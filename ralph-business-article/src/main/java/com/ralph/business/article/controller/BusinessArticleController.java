package com.ralph.business.article.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.business.article.entity.BusinessArticle;
import com.ralph.business.article.service.BusinessArticleService;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.core.annotation.PreventRepeatInvoke;
import com.ralph.common.core.entity.Result;
import com.ralph.common.docs.annotation.ApiNeedField;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @auther ralph
 * @create 2022-12-15
 * @describe 业务表-文章主体表前端控制器
 */
@RequiredArgsConstructor
@RestController
@Api(value = "article", tags = "文章主体表")
@RequestMapping("/article")
public class BusinessArticleController {

    private final BusinessArticleService businessArticleService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param businessArticle 业务表-文章主体表
     * @return Result
     */
    @PreventRepeatInvoke(value = "5")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Result<Page<BusinessArticle>> getBusinessArticlePage(@ApiNeedField({"size","current"}) Page<BusinessArticle> page, BusinessArticle businessArticle) {
        return Result.success(businessArticleService.page(page, Wrappers.query(businessArticle)));
    }

    /**
     * 通过主键查询业务表-文章主体表
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/get/id")
    public Result<BusinessArticle> getById(@RequestParam("id") Integer id) {
        return Result.success(businessArticleService.getById(id));
    }

    /**
     * 新增业务表-文章主体表
     * @param urlList 业务表-文章主体表
     * @return Result
     */
    @ActionLog("新增文章")
    @ApiOperation(value = "新增业务表-文章主体表", notes = "新增业务表-文章主体表")
    @PostMapping("/insert")
    public Result<Boolean> save(@RequestBody List<String> urlList) {
        return businessArticleService.addArticle(urlList);
    }

    /**
     * 修改业务表-文章主体表
     * @param businessArticle 业务表-文章主体表
     * @return Result
     */
    @ActionLog("修改文章")
    @ApiOperation(value = "修改业务表-文章主体表", notes = "修改业务表-文章主体表")
    @PutMapping("/update")
    public Result<Boolean> updateById(@RequestBody BusinessArticle businessArticle) {
        return Result.success(businessArticleService.updateById(businessArticle));
    }

    /**
     * 通过主键删除业务表-文章主体表
     * @param id 主键
     * @return Result
     */
    @ActionLog("删除文章")
    @ApiOperation(value = "通过主键删除业务表-文章主体表", notes = "通过主键删除业务表-文章主体表")
    @DeleteMapping("/delete/id" )
    public Result<Boolean> removeById(@RequestParam("id") Integer id) {
        return Result.success(businessArticleService.removeById(id));
    }
}
