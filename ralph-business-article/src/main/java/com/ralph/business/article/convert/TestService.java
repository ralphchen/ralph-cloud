package com.ralph.business.article.convert;

import java.time.LocalDateTime;

public class TestService {
    public static void main(String[] args) {
        SourceEntity sourceEntity = new SourceEntity(1,0,"realName",0, LocalDateTime.now());

        // TargetEntity targetEntity = EntityConvert.INSTANCES.toTargetEntity(sourceEntity);
        // TargetEntity(id=1, parentId=0, realName=realName, sex=null, updateDate=Sun Feb 12 06:25:10 CST 2023)

        // TargetEntity targetEntity = new TargetEntity(2,"1","ralph","女",new Date());
        // SourceEntity source = EntityConvert.INSTANCES.toTargetEntity(targetEntity);
        // System.out.println(source);

        // BusinessArticle businessArticle = new BusinessArticle();
        // businessArticle.setId(3);
        // businessArticle.setName("article");
        // TargetEntity targetEntity = EntityConvert.INSTANCES.mergeEntity(sourceEntity, businessArticle);

        TargetEntity targetEntity = EntityConvert.INSTANCES.sexMapper(sourceEntity);

        System.out.println(targetEntity);
    }
}





