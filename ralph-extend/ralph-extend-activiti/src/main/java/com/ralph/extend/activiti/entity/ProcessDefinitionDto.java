package com.ralph.extend.activiti.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "流程定义")
public class ProcessDefinitionDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 流程定义id
     */
    @ApiModelProperty(value="流程定义id")
    private String id;

    /**
     * 流程定义名称
     */
    @ApiModelProperty(value="流程定义名称")
    private String name;

    /**
     * 流程定义key
     */
    @ApiModelProperty(value="流程定义key")
    private String key;

    /**
     * 流程定义版本
     */
    @ApiModelProperty(value="流程定义版本")
    private int version;

    /**
     * 流程部署id
     */
    @ApiModelProperty(value="流程定义版本")
    private String deploymentId;

    /**
     * xml文件名称
     */
    @ApiModelProperty(value="xml文件名称")
    private String resourceName;

    /**
     * 图片资源名称
     */
    @ApiModelProperty(value="图片资源名称")
    private String dgrmResourceName;

    /**
     * 流程实例状态: 激活 or 挂起
     */
    @ApiModelProperty(value="流程实例状态: 激活 or 挂起")
    private boolean suspendState;

    /**
     * 流程部署时间
     */
    @ApiModelProperty(value="流程部署时间")
    private Date deployTime;

    /**
     * 流程部署名称
     */
    @ApiModelProperty(value="流程部署名称")
    private String deployName;
}