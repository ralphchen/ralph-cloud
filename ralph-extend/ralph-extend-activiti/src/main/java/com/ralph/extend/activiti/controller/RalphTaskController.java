package com.ralph.extend.activiti.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.common.core.entity.Result;
import com.ralph.extend.activiti.entity.form.ApproveTaskForm;
import com.ralph.extend.activiti.entity.form.StartTaskForm;
import com.ralph.extend.activiti.entity.TaskDto;
import com.ralph.extend.activiti.service.RalphTaskService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.activiti.api.process.model.ProcessInstance;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/task")
@ApiOperation(value = "流程任务", notes = "流程任务")
public class RalphTaskController {

    private final RalphTaskService ralphTaskService;

    /**
     * 发起任务
     * @param taskForm taskForm
     */
    @PostMapping("/start")
    @ApiOperation(value = "发起任务", notes = "发起任务")
    public Result<ProcessInstance> startTask(@RequestBody @Validated StartTaskForm taskForm){
        return Result.success(ralphTaskService.startTask(taskForm));
    }

    /**
     * 获取当前登录人任务列表
     * @param page 分页
     * @return 分页结果
     */
    @GetMapping("/list")
    @ApiOperation(value = "获取当前登录人任务列表", notes = "获取当前登录人任务列表")
    public Result<Page<TaskDto>> getTaskList(Page<TaskDto> page){
        return Result.success(ralphTaskService.getTaskList(page));
    }

    /**
     * 删除任务
     * @param instanceId 任务实例id
     * @param reason 删除原因
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除任务", notes = "删除任务")
    public Result<Boolean> deleteTask(String instanceId, String reason){
        return Result.success(ralphTaskService.deleteTask(instanceId, reason));
    }

    /**
     * 审批任务
     * @param taskForm 审批表单
     * @return 审批结果
     */
    @PostMapping("/approve")
    @ApiOperation(value = "审批任务", notes = "审批任务")
    public Result<Boolean> approveProcess(@RequestBody @Validated ApproveTaskForm taskForm){
        return Result.success(ralphTaskService.approveProcess(taskForm));
    }
}
