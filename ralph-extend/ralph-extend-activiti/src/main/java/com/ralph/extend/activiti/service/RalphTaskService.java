package com.ralph.extend.activiti.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.extend.activiti.entity.form.ApproveTaskForm;
import com.ralph.extend.activiti.entity.form.StartTaskForm;
import com.ralph.extend.activiti.entity.TaskDto;
import org.activiti.api.process.model.ProcessInstance;

public interface RalphTaskService {

    /**
     * 发起任务
     * @param taskForm taskForm
     */
    ProcessInstance startTask(StartTaskForm taskForm);

    /**
     * 获取当前登录人任务列表
     * @param page 分页
     * @return 分页结果
     */
    Page<TaskDto> getTaskList(Page<TaskDto> page);

    /**
     * 删除任务
     * @param instanceId 任务实例id
     * @param reason 删除原因
     * @return 删除结果
     */
    Boolean deleteTask(String instanceId, String reason);

    /**
     * 审批任务
     * @param taskForm 审批表单
     * @return 审批结果
     */
    Boolean approveProcess(ApproveTaskForm taskForm);
}
