package com.ralph.extend.activiti.service.impl;

import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.extend.activiti.entity.form.ApproveTaskForm;
import com.ralph.extend.activiti.service.RalphHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Task;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class RalphHistoryServiceImpl implements RalphHistoryService {

    private final ProcessEngine processEngine;

    private final RepositoryService repositoryService;

    private final RuntimeService runtimeService;

    private final HistoryService historyService;

    /**
     * 查看流程进度图片
     * @param response response
     * @param taskForm 任务表单
     */
    public void getProcessPlan(HttpServletResponse response, ApproveTaskForm taskForm) {
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery()
                .taskDefinitionKey(taskForm.getTaskDefinitionKey())
                .processInstanceId(taskForm.getTaskInstanceId())
                .singleResult();
        BpmnModel bpmnModel;
        List<String> activeActivityIds;
        if (task != null) {
            bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId());
            activeActivityIds = runtimeService.getActiveActivityIds(task.getExecutionId());
        } else {
            HistoricTaskInstance taskInstance = historyService.createHistoricTaskInstanceQuery()
                    .processInstanceId(taskForm.getTaskInstanceId()).list().get(0);
            if (null == taskInstance){
                log.warn("任务不存在");
                throw new BusinessException(ErrorEnum.DATA_NOT_EXISTS);
            }
            bpmnModel = repositoryService.getBpmnModel(taskInstance.getProcessDefinitionId());
            activeActivityIds = new ArrayList<>();
        }
        // Map map = bpmnModel.getItemDefinitions();
        DefaultProcessDiagramGenerator diagramGenerator = new DefaultProcessDiagramGenerator();
        String os = System.getProperty("os.name").toLowerCase();
        String font = "SimSun";
        if (os.startsWith("win")) {
            font = "宋体";
        }
        try (InputStream in = diagramGenerator.generateDiagram(bpmnModel, "jpg", activeActivityIds, activeActivityIds,
                font, font, font,
                processEngine.getProcessEngineConfiguration().getProcessEngineConfiguration().getClassLoader(), 1.0);
             BufferedInputStream bin = new BufferedInputStream(in);
             OutputStream out = response.getOutputStream();
             BufferedOutputStream bout = new BufferedOutputStream(out)
        ) {
            IOUtils.copy(bin, bout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
