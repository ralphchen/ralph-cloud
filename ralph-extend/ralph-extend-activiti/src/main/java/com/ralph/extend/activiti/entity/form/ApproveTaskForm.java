package com.ralph.extend.activiti.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Map;

@Data
@ApiModel(value = "任务审批表单")
public class ApproveTaskForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 任务实例Id
     */
    @NotBlank
    @ApiModelProperty(value="任务实例Id")
    private String taskInstanceId;

    /**
     * 任务定义key
     */
    @NotBlank
    @ApiModelProperty(value="任务定义key")
    private String taskDefinitionKey;

    /**
     * 任务参数变量
     */
    @ApiModelProperty(value="任务参数变量")
    private Map<String,Object> paramMap;
}
