package com.ralph.extend.activiti.service.impl;

import cn.hutool.core.io.IoUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Sets;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.extend.activiti.entity.ProcessDefinitionDto;
import com.ralph.extend.activiti.mapper.RalphActivitiMapper;
import com.ralph.extend.activiti.service.RalphRepositoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.zip.ZipInputStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class RalphRepositoryServiceImpl implements RalphRepositoryService {

    private final RepositoryService repositoryService;

    private final TaskService taskService;

    private final RalphActivitiMapper ralphActivitiMapper;

    private static final Set<String> ZIP_EXTENSION_SET = Sets.newHashSet("zip","rar","7z");

    /**
     * 通过上传文件部署流程
     * @param file 文件
     * @param processName 流程名称
     */
    @Override
    public Deployment deployByFile(MultipartFile file, String processName){
        // 获取上传的文件名
        String fileName = file.getOriginalFilename();
        // 文件的扩展名
        String extension = FilenameUtils.getExtension(fileName);

        ZipInputStream zipInputStream = null;
        InputStream inputStream;
        Deployment deploy = null;
        try {
            inputStream = file.getInputStream();
            DeploymentBuilder builder = repositoryService.createDeployment().name(processName);
            if (ZIP_EXTENSION_SET.contains(extension)){
                zipInputStream = new ZipInputStream(inputStream);
                builder.addZipInputStream(zipInputStream);
            } else {
                builder.addInputStream(fileName, inputStream);
            }
            deploy = builder.deploy();
            log.info("流程"+deploy.getName()+"部署成功，流程id："+deploy.getId());
        } catch (Exception e){
            log.error("文件解析失败");
        } finally {
            try {
                if (null != zipInputStream){
                    zipInputStream.closeEntry();
                    zipInputStream.close();
                }
            }catch (Exception e){
                log.warn("zipInputStream关闭失败");
            }
        }
        return deploy;
    }

    /**
     * 获取流程列表
     * @param page 分页信息
     * @param processName 流程名称
     * @param deploymentId 部署id
     * @return 分页结果
     */
    @Override
    public Page<ProcessDefinitionDto> getProcessList(Page<ProcessDefinitionDto> page, String processName, String deploymentId){
        // 查询流程定义
        return ralphActivitiMapper.selectProcessDefin(page, processName, deploymentId);
    }

    /**
     * 删除流程定义
     *
     * @param deploymentId 流程定义id
     * @return 删除结果
     */
    @Override
    public Boolean deleteDefinition(String deploymentId) {
        long count = taskService.createTaskQuery().deploymentId(deploymentId).count();
        if (count > 1){
            throw new BusinessException(ErrorEnum.DEL_DEFIN_ERROR);
        }
        repositoryService.deleteDeployment(deploymentId);
        return Boolean.TRUE;
    }

    /**
     * 改变流程定义状态（激活或挂起）
     *
     * @param processId 流程id
     * @param state 是否挂起流程，true为挂起，false为激活
     * @return 改变结果
     */
    @Override
    public Boolean suspendOrActiveApply(String processId, boolean state) {
        // 判断流程定义当前状态
        boolean suspended = repositoryService.isProcessDefinitionSuspended(processId);
        if (state == suspended){
            return Boolean.TRUE;
        }
        if (state) {
            // 当流程定义被挂起时，已经发起的该流程定义的流程实例不受影响（如果选择级联挂起则流程实例也会被挂起）。
            // 当流程定义被挂起时，无法发起新的该流程定义的流程实例。
            repositoryService.suspendProcessDefinitionById(processId);
        } else {
            repositoryService.activateProcessDefinitionById(processId);
        }
        return Boolean.TRUE;
    }

    /**
     * 下载Resource文件
     * @param response response
     * @param deploymentId 部署id
     * @param resourceName 文件名称
     */
    @Override
    public void downResource(HttpServletResponse response, String deploymentId, String resourceName){
        InputStream inputStream = repositoryService.getResourceAsStream(deploymentId, resourceName);
        // response.setContentType("text/xml");
        try {
            IoUtil.copy(inputStream,response.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
