package com.ralph.extend.activiti.service;

import com.ralph.extend.activiti.entity.form.ApproveTaskForm;

import javax.servlet.http.HttpServletResponse;

public interface RalphHistoryService {

    /**
     * 查看流程进度图片
     * @param response response
     * @param taskForm 任务表单
     */
    void getProcessPlan(HttpServletResponse response, ApproveTaskForm taskForm);
}
