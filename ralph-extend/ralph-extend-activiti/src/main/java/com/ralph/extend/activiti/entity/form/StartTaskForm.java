package com.ralph.extend.activiti.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Map;

@Data
@ApiModel(value = "开始任务表单")
public class StartTaskForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 流程定义id
     */
    @NotBlank
    @ApiModelProperty(value="流程定义id")
    private String processDefinitionId;

    /**
     * 任务名称
     */
    @NotBlank
    @ApiModelProperty(value="任务名称")
    private String taskName;

    /**
     * 处理人map
     */
    @ApiModelProperty(value="处理人map")
    private Map<String,Object> paramMap;
}
