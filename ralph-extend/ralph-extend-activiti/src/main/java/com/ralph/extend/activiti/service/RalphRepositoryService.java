package com.ralph.extend.activiti.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.extend.activiti.entity.ProcessDefinitionDto;
import org.activiti.engine.repository.Deployment;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface RalphRepositoryService {

    /**
     * 通过上传文件部署流程
     * @param file 文件
     * @param processName 流程名称
     */
    Deployment deployByFile(MultipartFile file, String processName);

    /**
     * 获取流程列表
     * @param page 分页信息
     * @param processName 流程名称
     * @param deploymentId 部署id
     * @return 分页结果
     */
    Page<ProcessDefinitionDto> getProcessList(Page<ProcessDefinitionDto> page, String processName, String deploymentId);

    /**
     * 删除流程定义
     *
     * @param deploymentId 流程定义id
     * @return 删除结果
     */
    Boolean deleteDefinition(String deploymentId);

    /**
     * 改变流程定义状态（激活或挂起）
     * @param processId 流程id
     * @param state 是否挂起流程，true为挂起，false为激活
     * @return 改变结果
     */
    Boolean suspendOrActiveApply(String processId, boolean state);

    /**
     * 下载Resource文件
     * @param response response
     * @param deploymentId 部署id
     * @param resourceName 文件名称
     */
    void downResource(HttpServletResponse response, String deploymentId, String resourceName);
}
