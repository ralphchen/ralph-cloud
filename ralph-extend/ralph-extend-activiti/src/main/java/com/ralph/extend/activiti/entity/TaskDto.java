package com.ralph.extend.activiti.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "任务")
public class TaskDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 流程实例id
     */
    @ApiModelProperty(value="流程实例id")
    private String processInstanceId;

    /**
     * 流程定义key
     */
    @ApiModelProperty(value="流程定义key")
    private String taskDefinitionKey;

    /**
     * 流程定义id
     */
    @ApiModelProperty(value="流程定义id")
    private String processDefinitionId;

    /**
     * 流程负责人
     */
    @ApiModelProperty(value="流程负责人")
    private String assignee;

    /**
     * 流程实例名称
     */
    @ApiModelProperty(value="流程实例名称")
    private String name;

    /**
     * 流程创建时间
     */
    @ApiModelProperty(value="流程创建时间")
    private Date createTime;

    /**
     * 流程挂起状态
     */
    @ApiModelProperty(value="流程挂起状态")
    private boolean suspended;
}