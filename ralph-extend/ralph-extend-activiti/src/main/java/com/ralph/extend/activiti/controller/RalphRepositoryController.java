package com.ralph.extend.activiti.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.common.core.entity.Result;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.extend.activiti.entity.ProcessDefinitionDto;
import com.ralph.extend.activiti.service.RalphRepositoryService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.activiti.engine.repository.Deployment;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * Repository相关接口
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/repository")
@ApiOperation(value = "流程定义", notes = "流程定义")
public class RalphRepositoryController {

    private final RalphRepositoryService ralphRepositoryService;

    /**
     * 通过上传文件部署流程
     * @param file 文件
     * @param processName 流程名称
     */
    @GetMapping("/deploy/file")
    @ApiOperation(value = "通过上传文件部署流程", notes = "通过上传文件部署流程")
    public Result<Deployment> deployByFile(@NotNull MultipartFile file, String processName){
        return Result.success(ralphRepositoryService.deployByFile(file, processName));
    }

    /**
     * 获取流程列表
     * @param page 分页信息
     * @param processName 流程名称
     * @param deploymentId 部署id
     * @return 分页结果
     */
    @GetMapping("/list")
    @ApiOperation(value = "获取流程列表", notes = "获取流程列表")
    public Result<Page<ProcessDefinitionDto>> list(@ApiNeedField({"current,size"}) Page<ProcessDefinitionDto> page, String processName, String deploymentId) {
        return Result.success(ralphRepositoryService.getProcessList(page, processName, deploymentId));
    }

    /**
     * 删除流程定义
     *
     * @param deploymentId 流程定义id
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除流程定义", notes = "删除流程定义")
    public Result<Boolean> deleteDefinition(String deploymentId) {
        return Result.success(ralphRepositoryService.deleteDefinition(deploymentId));
    }

    /**
     * 改变流程定义状态（激活或挂起）
     * @param processId 流程id
     * @param state 是否挂起流程，true为挂起，false为激活
     * @return 改变结果
     */
    @GetMapping("/active")
    @ApiOperation(value = "改变流程定义状态（激活或挂起）", notes = "改变流程定义状态（激活或挂起）")
    public Result<Boolean> suspendOrActiveApply(String processId, boolean state) {
        return Result.success(ralphRepositoryService.suspendOrActiveApply(processId, state));
    }

    /**
     * 下载Resource文件
     * @param response response
     * @param deploymentId 部署id
     * @param resourceName 文件名称
     */
    @GetMapping("/download")
    @ApiOperation(value = "下载Resource文件", notes = "下载Resource文件")
    public void downResource(HttpServletResponse response, String deploymentId, String resourceName){
        ralphRepositoryService.downResource(response, deploymentId, resourceName);
    }
}
