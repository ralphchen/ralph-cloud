package com.ralph.extend.activiti.controller;

import com.ralph.extend.activiti.entity.form.ApproveTaskForm;
import com.ralph.extend.activiti.service.RalphHistoryService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequiredArgsConstructor
@RequestMapping("/history")
@ApiOperation(value = "流程历史", notes = "流程历史")
public class RalphHistoryController {

    private final RalphHistoryService ralphHistoryService;

    /**
     * 查看流程进度图片
     * @param response response
     * @param taskForm 任务表单
     */
    @GetMapping("/plan")
    @ApiOperation(value = "查看流程进度图片", notes = "查看流程进度图片")
    public void getProcessPlan(HttpServletResponse response, ApproveTaskForm taskForm){
        ralphHistoryService.getProcessPlan(response, taskForm);
    }
}
