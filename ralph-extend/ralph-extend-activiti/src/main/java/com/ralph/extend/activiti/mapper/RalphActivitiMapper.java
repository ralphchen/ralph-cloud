package com.ralph.extend.activiti.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.extend.activiti.entity.ProcessDefinitionDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 自定义sql语句mapper
 */
@Mapper
public interface RalphActivitiMapper {

    Page<ProcessDefinitionDto> selectProcessDefin(Page<ProcessDefinitionDto> page,
                                                  @Param("processName") String processName,
                                                  @Param("deploymentId") String deploymentId);
}
