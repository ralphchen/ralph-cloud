package com.ralph.extend.activiti.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.common.core.utils.SecurityUtils;
import com.ralph.extend.activiti.entity.TaskDto;
import com.ralph.extend.activiti.entity.form.ApproveTaskForm;
import com.ralph.extend.activiti.entity.form.StartTaskForm;
import com.ralph.extend.activiti.service.RalphTaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.DeleteProcessPayloadBuilder;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.model.builders.StartProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class RalphTaskServiceImpl implements RalphTaskService {

    private final ProcessRuntime processRuntime;

    private final TaskService taskService;

    /**
     * 发起任务
     * @param taskForm taskForm
     */
    @Override
    public ProcessInstance startTask(StartTaskForm taskForm){
        StartProcessPayloadBuilder startProcessPayloadBuilder = ProcessPayloadBuilder.start()
                .withProcessDefinitionId(taskForm.getProcessDefinitionId())
                .withName(taskForm.getTaskName());
        if (null != taskForm.getParamMap()){
            startProcessPayloadBuilder.withVariables(taskForm.getParamMap());
        }
        return processRuntime.start(startProcessPayloadBuilder.build());
    }


    /**
     * 获取当前登录人任务列表
     * @param page 分页
     * @return 分页结果
     */
    public Page<TaskDto> getTaskList(Page<TaskDto> page){
        String username = SecurityUtils.getUsername();
        List<Task> tasks = taskService.createTaskQuery()
                .taskCandidateOrAssigned(username)
                .listPage((int) ((page.getCurrent() - 1) * page.getSize()), (int) page.getSize());
        List<TaskDto> resList = tasks.stream().map(item ->
            new TaskDto(item.getProcessInstanceId(),
                    item.getTaskDefinitionKey(),
                    item.getProcessDefinitionId(),
                    item.getAssignee(),
                    item.getName(),
                    item.getCreateTime(),
                    item.isSuspended())
        ).collect(Collectors.toList());
        long count = taskService.createTaskQuery().count();
        page.setRecords(resList);
        page.setTotal(count);
        page.setPages(count / page.getSize() + 1);
        return page;
    }

    /**
     * 删除任务
     * @param instanceId 任务实例id
     * @param reason 删除原因
     * @return 删除结果
     */
    public Boolean deleteTask(String instanceId, String reason){
        processRuntime.delete(new DeleteProcessPayloadBuilder()
                .withProcessInstanceId(instanceId)
                .withReason(reason)
                .build());
        return Boolean.TRUE;
    }

    /**
     * 审批任务
     * @param taskForm 审批表单
     * @return 审批结果
     */
    @Override
    public Boolean approveProcess(ApproveTaskForm taskForm){
        Task task = taskService.createTaskQuery()
                .taskDefinitionKey(taskForm.getTaskDefinitionKey())
                .processInstanceId(taskForm.getTaskInstanceId())
                .singleResult();
        if (null == task){
            log.warn("任务不存在，任务id：" + taskForm.getTaskInstanceId());
            return Boolean.FALSE;
        }
        if (null == taskForm.getParamMap()){
            taskService.complete(task.getId());
        }else {
            taskService.complete(task.getId(), taskForm.getParamMap());
        }
        return Boolean.TRUE;
    }
}
