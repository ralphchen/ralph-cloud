package com.ralph.extend.xxljob.core.alarm;


import com.ralph.extend.xxljob.core.model.XxlJobInfo;
import com.ralph.extend.xxljob.core.model.XxlJobLog;

/**
 * @author xuxueli 2020-01-19
 */
public interface JobAlarm {

	/**
	 * job alarm
	 * @param info
	 * @param jobLog
	 * @return
	 */
	public boolean doAlarm(XxlJobInfo info, XxlJobLog jobLog);

}
