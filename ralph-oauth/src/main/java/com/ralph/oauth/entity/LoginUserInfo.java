package com.ralph.oauth.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel(value = "登录用户信息")
public class LoginUserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    @NotBlank
    @ApiModelProperty(value="用户名")
    private String username;

    /**
     * 密码
     */
    @NotBlank
    @ApiModelProperty(value="密码")
    private String password;

    /**
     * 图形验证码
     */
    @ApiModelProperty(value="图形验证码")
    private String captcha;
}
