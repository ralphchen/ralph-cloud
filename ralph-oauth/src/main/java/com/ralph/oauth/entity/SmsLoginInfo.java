package com.ralph.oauth.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel(value = "手机号登录用户信息")
public class SmsLoginInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 手机号
     */
    @NotBlank
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 验证码
     */
    @NotBlank
    @ApiModelProperty(value="验证码")
    private String code;
}
