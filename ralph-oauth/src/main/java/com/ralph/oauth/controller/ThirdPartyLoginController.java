package com.ralph.oauth.controller;

import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.core.annotation.IgnoreUrl;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.entity.UserDTO;
import com.ralph.oauth.service.LoginService;
import com.xkcoding.justauth.AuthRequestFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.utils.AuthStateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 第三方登录
 */
@Slf4j
@RestController
@RequestMapping("third")
@RequiredArgsConstructor
@Api(value = "third", tags = "第三方登录")
public class ThirdPartyLoginController {

    private final AuthRequestFactory factory;

    private  final LoginService loginService;

    /**
     * 第三方登录
     * @param type 第三方类型
     * @param response response
     * @throws IOException
     */
    @IgnoreUrl
    @ActionLog("第三方登录")
    @ApiOperation(value = "第三方登录跳转")
    @GetMapping(value = "/{type}/login")
    public void jump(@PathVariable String type, HttpServletResponse response) throws IOException {
        AuthRequest authRequest = factory.get(type);
        String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
        response.sendRedirect(authorizeUrl);
    }

    /**
     * 第三方登录callback
     * @param type 第三方登录类型
     * @param callback callback
     * @return Result
     */
    @IgnoreUrl
    @ApiIgnore
    @GetMapping(value = "/{type}/callback")
    public Result<UserDTO> callback(@PathVariable String type, AuthCallback callback){
        return Result.success(loginService.callback(type, callback));
    }
}
