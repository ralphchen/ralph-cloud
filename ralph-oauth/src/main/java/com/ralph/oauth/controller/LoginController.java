package com.ralph.oauth.controller;

import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.core.annotation.IgnoreUrl;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.entity.UserDTO;
import com.ralph.oauth.entity.LoginUserInfo;
import com.ralph.oauth.entity.SmsLoginInfo;
import com.ralph.oauth.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 系统登录相关
 */
@RestController
@RequestMapping("auth")
@Api(value = "auth", tags = "系统登录相关")
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     * 登录
     * @param userInfo 用户信息
     * @return token
     */
    @IgnoreUrl
    @ActionLog("登录")
    @PostMapping("/login")
    @ApiOperation(value = "登录", notes = "登录")
    public Result<UserDTO> login(@RequestBody @Validated LoginUserInfo userInfo){
        return Result.success(ErrorEnum.SUCCESS.getMsg(), loginService.login(userInfo));
    }

    /**
     * 用户登出
     * @param username 用户名
     * @return 登出结果
     */
    @ActionLog("登出")
    @PostMapping("/logout")
    @ApiOperation(value = "登出", notes = "登出")
    public Result<Boolean> logout(String username){
        return Result.success(ErrorEnum.SUCCESS.getMsg(),loginService.logout(username));
    }

    /**
     * 生成图形验证码
     * @param request request
     * @param response response
     */
    @IgnoreUrl
    @ApiOperation(value = "验证码")
    @GetMapping(value = "/captcha", produces = "image/jpeg")
    public void captcha(HttpServletRequest request, HttpServletResponse response) {
        loginService.captcha(request, response);
    }

    /**
     * 发送短信验证码
     * @param phone 手机号
     */
    @IgnoreUrl
    @ApiOperation(value = "发送短信验证码")
    @GetMapping(value = "/sms_captcha")
    public void smsCaptcha(String phone) {
        loginService.smsCaptcha(phone);
    }

    /**
     * 手机号登录
     * @param userInfo 用户信息
     * @return token
     */
    @IgnoreUrl
    @ActionLog("手机号登录")
    @PostMapping("/sms/login")
    @ApiOperation(value = "登录", notes = "登录")
    public Result<UserDTO> smsLogin(@RequestBody SmsLoginInfo userInfo){
        return Result.success(ErrorEnum.SUCCESS.getMsg(), loginService.smsLogin(userInfo));
    }
}
