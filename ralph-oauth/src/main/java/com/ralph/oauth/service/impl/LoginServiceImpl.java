package com.ralph.oauth.service.impl;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.entity.UserDTO;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.core.utils.EncryptUtils;
import com.ralph.common.core.utils.JwtTokenUtil;
import com.ralph.common.core.utils.RedisUtils;
import com.ralph.common.feign.feigns.AdminFeign;
import com.ralph.common.security.service.impl.UserDetailsServiceImpl;
import com.ralph.oauth.entity.LoginUserInfo;
import com.ralph.oauth.entity.SmsLoginInfo;
import com.ralph.oauth.service.LoginService;
import com.xkcoding.justauth.AuthRequestFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.model.AuthUser;
import me.zhyd.oauth.request.AuthRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final AuthenticationManager authenticationManager;

    private final UserDetailsServiceImpl userDetailsServiceImpl;

    private final DefaultKaptcha defaultKaptcha;

    private final AuthRequestFactory factory;

    private final AdminFeign adminFeign;

    /**
     * 登录
     *
     * @param userInfo 登录信息
     * @return token
     */
    @Override
    public UserDTO login(LoginUserInfo userInfo) {
        if (StringUtils.isNotBlank(userInfo.getCaptcha())){
            // 校验图形验证码
            verifyCaptcha(userInfo.getCaptcha());
        }
        UsernamePasswordAuthenticationToken upToken =
                new UsernamePasswordAuthenticationToken(userInfo.getUsername(), userInfo.getPassword());
        final Authentication authentication = authenticationManager.authenticate(upToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(userInfo.getUsername());
        UserDTO resUserInfo = (UserDTO)userDetails;
        resUserInfo.setPassword(null);
        String token = JwtTokenUtil.generateToken(userDetails);
        // 将生成的token存储至redis中
        RedisUtils.lAddToken(RedisKeyConstant.TOKEN_KEY_PRE + userInfo.getUsername(), token, JwtTokenUtil.TOKEN_EXPIRATION);
        resUserInfo.setToken(token);
        return resUserInfo;
    }

    /**
     * 用户登出
     *
     * @param username 用户名
     * @return 登出结果
     */
    @Override
    public Boolean logout(String username) {
        // 删除缓存中的token
        RedisUtils.del(RedisKeyConstant.TOKEN_KEY_PRE + username);
        return Boolean.TRUE;
    }

    /**
     * 生成图形验证码
     * @param request request
     * @param response response
     */
    @Override
    public void captcha(HttpServletRequest request, HttpServletResponse response) {
        // 定义response输出类型为image/jpeg类型
        response.setDateHeader("Expires", 0);
        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
        // return a jpeg
        response.setContentType("image/jpeg");
        //-------------------生成验证码 begin --------------------------
        //获取验证码文本内容
        String text = defaultKaptcha.createText();
        log.info("验证码内容：{}", text);
        //将验证码文本内容放入session
        request.getSession().setAttribute("captcha", text);
        //根据文本验证码内容创建图形验证码
        BufferedImage image = defaultKaptcha.createImage(text);
        ServletOutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            //输出流输出图片，格式为jpg
            ImageIO.write(image, "jpg", outputStream);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != outputStream) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 第三方登录callback
     *
     * @param type     第三方登录类型
     * @param callback callback
     * @return Result
     */
    @Override
    public UserDTO callback(String type, AuthCallback callback) {
        AuthRequest authRequest = factory.get(type);
        AuthResponse response = authRequest.login(callback);
        log.info("【response】= {}", JSONUtil.toJsonStr(response));
        JSONObject thirdInfo = getThirdInfo(type, response);
        // 对结果进行加密
        String encryptData = EncryptUtils.getEncryptData(thirdInfo.toString());
        return thirdLogin(encryptData);
    }

    /**
     * 发送短信验证码
     *
     * @param phone 手机号
     */
    @Override
    public void smsCaptcha(String phone) {
        // 校验手机号格式
        if (!Validator.isMobile(phone)){
            throw new BusinessException(ErrorEnum.DATA_FORMAT_ERROR,"手机号",true);
        }
        // 校验手机号发送次数
        String smsCountKey = RedisKeyConstant.SMS_CAPTCHA_COUNT + phone;
        Integer count = (Integer) RedisUtils.get(smsCountKey);
        if (null != count && count >= 10){
            throw new BusinessException(ErrorEnum.CAPTCHA_COUNT_ERROR);
        }
        // 校验是否已发送过验证码
        boolean hasKey = RedisUtils.hasKey(RedisKeyConstant.SMS_CAPTCHA + phone);
        if (hasKey){
            throw new BusinessException(ErrorEnum.CAPTCHA_OFTEN_ERROR);
        }
        // 生成验证码
        String code = RandomUtil.randomNumbers(4);
        // TODO 调用接口发送验证码

        log.info("发送短信验证码成功");
        // 添加缓存,5分钟后失效
        RedisUtils.set(RedisKeyConstant.SMS_CAPTCHA + phone, code, 1000 * 60 * 5);
        RedisUtils.incr(smsCountKey, 1L);
        RedisUtils.expireAtEndOfDay(smsCountKey);
    }

    @Override
    public UserDTO smsLogin(SmsLoginInfo userInfo) {
        // 校验验证码
        String smsKey = RedisKeyConstant.SMS_CAPTCHA + userInfo.getPhone();
        String captcha = RedisUtils.get(smsKey).toString();
        if (StringUtils.isBlank(captcha) || !userInfo.getCode().equals(captcha)){
            throw new BusinessException(ErrorEnum.CAPTCHA_ERROR);
        }
        // 根据手机号获取用户名
        Result<UserDTO> userDTO = adminFeign.getUserInfoByPhone(userInfo.getPhone());
        UserDTO resUserInfo = buildLoginInfo(userDTO);
        // 删除key
        RedisUtils.del(smsKey);
        return resUserInfo;
    }

    private UserDTO buildLoginInfo(Result<UserDTO> userDTO){
        UserDTO resUserInfo = new UserDTO();
        boolean present = Optional.of(userDTO)
                .map(Result::getData)
                .map(UserDTO::getUsername)
                .isPresent();
        if (!present || !ErrorEnum.SUCCESS.getCode().equals(userDTO.getCode())){
            log.info("未获取到用户信息");
            throw new BusinessException(ErrorEnum.USER_ACCOUNT_NOT_EXIST);
        }
        String username = userDTO.getData().getUsername();
        UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(username);
        resUserInfo = (UserDTO)userDetails;
        resUserInfo.setPassword(null);
        String token = JwtTokenUtil.generateToken(userDetails);
        // 将生成的token存储至redis中
        RedisUtils.lAddToken(RedisKeyConstant.TOKEN_KEY_PRE + username, token, JwtTokenUtil.TOKEN_EXPIRATION);
        resUserInfo.setToken(token);
        return resUserInfo;
    }

    /**
     * 第三方登录
     *
     * @param thirdInfo  第三方信息
     * @return Result
     */
    public UserDTO thirdLogin(String thirdInfo) {
        Result<UserDTO> userDTO = adminFeign.getUserInfoByThirdKey(thirdInfo);
        UserDTO resUserInfo = buildLoginInfo(userDTO);
        resUserInfo.setThirdUserInfo(thirdInfo);
        return resUserInfo;
    }

    /**
     * 获取第三方登录信息
     * @param type 第三方类型
     * @param response 第三方接口调用返回结果
     * @return 解析结果
     */
    private JSONObject getThirdInfo(String type, AuthResponse response){
        JSONObject resJson = new JSONObject();
        com.alibaba.fastjson.JSONObject userInfo = Optional.of(response).map(item -> (AuthUser) response.getData())
                .map(AuthUser::getRawUserInfo)
                .orElse(new com.alibaba.fastjson.JSONObject());
        resJson.putOnce("type",type);
        switch (type){
            case "gitee":
                resJson.putOnce("thirdId",userInfo.getString("id"));
                resJson.putOnce("thirdName",userInfo.getString("name"));
                break;
            default: break;
        }
        return resJson;
    }

    /**
     * 校验图形验证码
     * @param captcha 验证码
     */
    private void verifyCaptcha(String captcha){
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String resCaptcha = (String)httpServletRequest.getSession().getAttribute("captcha");
        if (StringUtils.isEmpty(resCaptcha) || StringUtils.isEmpty(captcha)
                || !resCaptcha.equalsIgnoreCase(captcha)){
            throw new BusinessException(ErrorEnum.CAPTCHA_ERROR);
        }
    }
}
