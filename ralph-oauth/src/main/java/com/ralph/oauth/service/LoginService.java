package com.ralph.oauth.service;

import com.ralph.common.core.entity.UserDTO;
import com.ralph.oauth.entity.LoginUserInfo;
import com.ralph.oauth.entity.SmsLoginInfo;
import me.zhyd.oauth.model.AuthCallback;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface LoginService {

    /**
     * 登录
     * @param userInfo 登录信息
     * @return token
     */
    UserDTO login(LoginUserInfo userInfo);

    /**
     * 用户登出
     * @param username 用户名
     * @return 登出结果
     */
    Boolean logout(String username);

    /**
     * 生成图形验证码
     * @param request request
     * @param response response
     */
    void captcha(HttpServletRequest request, HttpServletResponse response);

    /**
     * 第三方登录callback
     * @param type 第三方登录类型
     * @param callback callback
     * @return Result
     */
    UserDTO callback(String type, AuthCallback callback);

    /**
     * 发送短信验证码
     * @param phone 手机号
     */
    void smsCaptcha(String phone);

    UserDTO smsLogin(SmsLoginInfo userInfo);
}
