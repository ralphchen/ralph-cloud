# Ralph-Cloud微服务

## 技术架构

作者画图的技能暂未点亮，暂时先以文本描述

用到的技术栈如下：SpringCloudAlibaba、SpringBoot、Nacos、SpringCloudGateway、Feign、MybatisPlus、Redis

## 整体结构

ralph-cloud

​	|——logs	// 项目运行日志

​	|——ralph-admin	// 系统模块

​	|——ralph-business-article	// 业务模块

​	|——ralph-common	// 通用模块

​		|——ralph-common-codegen	// mybatisplus自动生成代码模块

​		|——ralph-common-core	// 通用模块，如工具类、通用实体类、常量类、通用配置等

​		|——ralph-common-docs	// 文档模块，目前集成了swagger文档

​		|——ralph-common-feign	// feign模块

​		|——ralph-common-security	// 安全模块

​	|——ralph-gateway	// 网关模块

​	|——ralph-nacos	// 服务注册与发现，配置模块

​	|——ralph-oauth	// 授权模块

​	|——script	// 脚本文件、日志文件、jar包

## 模块介绍

### ralph-admin

系统模块，该模块包含了字典功能、参数管理功能、菜单功能、用户功能、角色功能、token管理功能、文件管理功能(MinIO)等

**TODO**：部门功能

### ralph-business-article

自定义业务模块，业务功能：可以定时调用csdn博客接口，增加博客访问量（作用不大）

### ralph-common

#### ralph-common-codegen

基于mybatilplus自动生成代码，使用的是新版的自动生成代码；

可以根据模板生成Controller、Entity等，Controller中还会自动生成增删改查接口代码，可以配置是否需要Swagger注解、是否需要Security的权限注解、是否需要增加日志注解等；还会生成增删改查相关菜单的sql，在数据库执行即可新增相关菜单

集成Screw，实现了数据库文档生成

**TODO**：交互式生成代码

#### ralph-common-core

通用类包含内容如下：

* Redistemplate的配置，Mybatisplus的配置、线程池配置
* 各种常量类
* 通用实体类
* 全局异常类
* 各种工具类
* banner文件统一管理
  在ralph-common-core模块的resources文件夹下有banners文件夹，该文件夹中维护了各模块的banner文件
* 日志文件统一管理
  将日志管理文件logback-spring.xml放在ralph-common-core模块的resources文件夹下，所有依赖于core的模块都会按照该日志文件生成日志
* 自定义注解
  * `@ActionLog`加了该注解的接口会记录操作时间，操作人，操作者ip，请求方式，请求参数等
  * `@IgnoreUrl`加了该注解的接口不会受到Security的权限卡控，可以直接访问接口
  * `@PreventRepeatInvoke`加了该注解的接口可以限制一段时间内不允许重复访问

#### ralph-common-docs

文档模块，目前集成了swagger，使用了knife4j进行了加强

swagger访问路径：http://ip:port/swagger-ui/index.html

knife4j访问路径：http://ip:port/doc.html

鉴权方式：token统一鉴权，在登录后，将获取到的token设置在swagger右上角的Authorized即可，需注意加Bearer 前缀

#### ralph-common-feign

服务间通信，将服务的feign配置在该模块下，支持fallback

#### ralph-common-security

安全模块

自定义了密码校验，目前支持MD5，可扩展其他校验方式

JWT校验，接口权限控制

#### ralph-common-rule

规则引擎模块

根据数据库配置的规则引擎进行校验，如果校验不通过则抛出异常

如果需要新增规则，需要在com.ralph.common.rule.engin.impl下新增实现类，类名规则：Rule+规则编码

使用时注入RuleCheckService即可

### ralph-gateway

网关模块，实现基于数据库的动态路由功能，无需在配置文件中进行配置路由

### ralph-nacos

使用nacos进行服务的注册与发现，配置管理

### ralph-oauth

授权模块，登录、登出等

目前支持用户名密码登录，图形验证码登录

集成JustAuth实现各种第三方登录

### script

#### sql

部署项目所需要数据库sql

#### bin

该文件夹下有直接启动nacos的命令，可以直接通过该文件启动nacos

#### jar

打包生成的jar包与依赖

### 待开发模块

工作流模块

## 部署项目前期准备

### 1、需要在hosts文件中增加如下映射：

* 127.0.0.1 ralph-mysql
* 127.0.0.1 ralph-nacos
* 127.0.0.1 ralph-redis

或者直接将配置文件中的映射修改为ip也可以

### 2、创建数据库

#### 2.1 nacos数据库

在数据库中执行/sql文件夹下ralph-nacos.sql文件即可，如果想修改数据库名称，可以修改ralph-nacos/conf/application.properties文件末尾的db.url.0配置

#### 2.2 系统模块数据库

执行/sql文件夹下的ralph_cloud.sql文件可以生成系统模块所需的数据库表，其中还有业务模块的数据库表，不需要的可以删除

### 3、打包相关

打包时可以将依赖和本体分开打包，将jar包放至ralph-cloud/jar/{projectName}/文件夹下，相关依赖在ralph-cloud/jar/{projectName}/libs文件夹下

在运行时只需要执行`nohup java -Dfile.encoding=utf-8 -Duser.timezone=GMT+8 -Dloader.path=libs/ -jar xxx.jar` 即可

这样在修改完项目时如果没有新增依赖，那么只需要更新jar包即可，如果有新增依赖，只需要将新依赖添加至libs包即可