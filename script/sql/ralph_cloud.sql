-- 创建数据库
CREATE database if NOT EXISTS `ralph_cloud` default character set utf8mb4 collate utf8mb4_general_ci;
USE `ralph_cloud`;

-- 创建数据库表与数据
/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : localhost:3306
 Source Schema         : ralph_cloud

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 05/02/2023 20:15:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for business_article
-- ----------------------------
DROP TABLE IF EXISTS `business_article`;
CREATE TABLE `business_article`  (
                                     `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                     `del_flag` int(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                     `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                     `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                     `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                     `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                     `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                     `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章url',
                                     `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章名称',
                                     `read_count` int(11) NULL DEFAULT NULL COMMENT '阅读次数',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '业务表-文章主体表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of business_article
-- ----------------------------
INSERT INTO `business_article` VALUES (1, 0, '2022-12-16 10:15:33', 'Ralph', '2022-12-16 11:00:01', 'Ralph', 5, 'https://blog.csdn.net/qq_38516524/article/details/128197472', '【SpringCloud系列】Ribbon负载均衡', 81);
INSERT INTO `business_article` VALUES (2, 0, '2022-12-16 10:15:34', 'Ralph', '2022-12-16 11:00:01', 'Ralph', 5, 'https://blog.csdn.net/qq_38516524/article/details/128285836', '自定义SpringBoot Jar包并导入项目', 22);
INSERT INTO `business_article` VALUES (3, 0, '2022-12-16 10:15:34', 'Ralph', '2022-12-16 11:00:01', 'Ralph', 5, 'https://blog.csdn.net/qq_38516524/article/details/128131597', 'IDEA高效配置-实用插件', 1288);
INSERT INTO `business_article` VALUES (4, 0, '2022-12-16 10:15:34', 'Ralph', '2022-12-16 11:00:00', 'Ralph', 5, 'https://blog.csdn.net/qq_38516524/article/details/128233239', '【SpringCloud系统】Zuul', 99);
INSERT INTO `business_article` VALUES (5, 0, '2022-12-16 10:15:34', 'Ralph', '2022-12-16 11:00:01', 'Ralph', 5, 'https://blog.csdn.net/qq_38516524/article/details/128214634', '【SpringCloud系列】Hystrix', 15);
INSERT INTO `business_article` VALUES (6, 0, '2022-12-16 10:15:34', 'Ralph', '2022-12-16 11:00:01', 'Ralph', 5, 'https://blog.csdn.net/qq_38516524/article/details/127388363', '浅学一下Redis', 733);
INSERT INTO `business_article` VALUES (7, 0, '2022-12-16 10:15:35', 'Ralph', '2022-12-16 11:00:01', 'Ralph', 5, 'https://blog.csdn.net/qq_38516524/article/details/128190443', '【SpringCloud系列】微服务', 96);

-- ----------------------------
-- Table structure for system_dict
-- ----------------------------
DROP TABLE IF EXISTS `system_dict`;
CREATE TABLE `system_dict`  (
                                `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                `del_flag` int(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
                                `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典描述',
                                PRIMARY KEY (`id`) USING BTREE,
                                UNIQUE INDEX `type_index`(`type`) USING BTREE COMMENT '字典类型'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_dict
-- ----------------------------

-- ----------------------------
-- Table structure for system_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `system_dict_item`;
CREATE TABLE `system_dict_item`  (
                                     `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                     `del_flag` int(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                     `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                     `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                     `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                     `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                     `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                     `dict_id` int(11) NOT NULL COMMENT '字典id',
                                     `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'key',
                                     `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'value',
                                     `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
                                     `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
                                     PRIMARY KEY (`id`) USING BTREE,
                                     INDEX `key_index`(`key`) USING BTREE,
                                     INDEX `dict_id`(`dict_id`) USING BTREE,
                                     CONSTRAINT `dict_id` FOREIGN KEY (`dict_id`) REFERENCES `system_dict` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典项' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_dict_item
-- ----------------------------

-- ----------------------------
-- Table structure for system_file
-- ----------------------------
DROP TABLE IF EXISTS `system_file`;
CREATE TABLE `system_file`  (
                                `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                `del_flag` int(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '存储文件名',
                                `file_origin_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原始文件名',
                                `file_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '存储文件url',
                                `file_suffix` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件后缀名',
                                `file_size` double NULL DEFAULT NULL COMMENT '文件大小',
                                `bucket_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '存储桶名称',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_file
-- ----------------------------
INSERT INTO `system_file` VALUES (1, 1, '2023-01-06 13:40:53', NULL, '2023-01-06 15:38:30', NULL, 0, '9f4a2e6a-a1bd-45ac-a01e-bf2f14a2866e.png', 'wallhaven-wq7y6r.png', '9f4a2e6a-a1bd-45ac-a01e-bf2f14a2866e.png', 'png', 1821052, 'zzxyd1');
INSERT INTO `system_file` VALUES (2, 1, '2023-01-06 13:45:45', NULL, '2023-01-06 15:37:59', NULL, 0, '08215329-48f3-4896-8d0b-a5ca2d747849.png', 'wallhaven-wq7y6r.png', '/picture/test/08215329-48f3-4896-8d0b-a5ca2d747849.png', 'png', 1821052, 'zzxyd');
INSERT INTO `system_file` VALUES (3, 1, '2023-01-06 15:44:56', NULL, '2023-01-06 15:45:32', NULL, 0, '088a8ed7-5993-4256-a91e-652e319484f1.png', 'wallhaven-wq7y6r.png', '/picture/test/088a8ed7-5993-4256-a91e-652e319484f1.png', 'png', 1821052, 'zzxyd1');
INSERT INTO `system_file` VALUES (4, 0, '2023-01-08 17:07:56', 'admin', '2023-01-08 17:07:56', 'admin', 0, 'bdf630c9-ca2a-4482-8f4a-5da5d5d9fd8e.png', 'wallhaven-wq7y6r.png', 'bdf630c9-ca2a-4482-8f4a-5da5d5d9fd8e.png', 'png', 1821052, 'zzxyd1');

-- ----------------------------
-- Table structure for system_log
-- ----------------------------
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log`  (
                               `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                               `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                               `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                               `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作',
                               `result` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求结果',
                               `ip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
                               `uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求路径',
                               `request_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式',
                               `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
                               `time` int(11) NULL DEFAULT NULL COMMENT '耗费时间',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_log
-- ----------------------------
INSERT INTO `system_log` VALUES (1, '2022-12-19 14:49:27', 'Ralph', '分页查询', 'Result(code=0, msg=成功, data=com.baomidou.mybatisplus.extension.plugins.pagination.Page@4be3ac4c)', '127.0.0.1', '/sys_log/page', 'GET', '', 155);
INSERT INTO `system_log` VALUES (2, '2023-01-11 13:59:59', NULL, '删除动态路由', 'Result(code=0, msg=成功, data=true)', '0:0:0:0:0:0:0:1', '/router/delete/id', 'DELETE', 'id=%5B2%5D', 154);
INSERT INTO `system_log` VALUES (3, '2023-01-11 14:01:15', NULL, '删除动态路由', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/router/delete/id', 'DELETE', 'id=%5B2%5D', 39167);
INSERT INTO `system_log` VALUES (4, '2023-01-11 14:06:16', NULL, '删除动态路由', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/router/delete/id', 'DELETE', 'id=%5B2%5D', 208);
INSERT INTO `system_log` VALUES (5, '2023-01-11 14:10:18', NULL, '删除动态路由', 'Result(code=0, msg=成功, data=true)', '0:0:0:0:0:0:0:1', '/router/delete/id', 'DELETE', 'id=%5B1%5D', 38);
INSERT INTO `system_log` VALUES (6, '2023-01-11 14:19:43', NULL, '删除动态路由', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/router/delete/id', 'DELETE', 'id=%5B7%5D', 44);
INSERT INTO `system_log` VALUES (7, '2023-01-12 09:28:52', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 5);
INSERT INTO `system_log` VALUES (8, '2023-01-12 09:30:46', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=true)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 3);
INSERT INTO `system_log` VALUES (9, '2023-01-12 10:17:00', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 5);
INSERT INTO `system_log` VALUES (10, '2023-01-12 10:17:10', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 1);
INSERT INTO `system_log` VALUES (11, '2023-01-12 10:18:14', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 1);
INSERT INTO `system_log` VALUES (12, '2023-01-12 10:48:50', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B123%5D', 5);
INSERT INTO `system_log` VALUES (13, '2023-01-12 10:49:00', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B123%5D', 2);
INSERT INTO `system_log` VALUES (14, '2023-01-12 11:04:27', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 5);
INSERT INTO `system_log` VALUES (15, '2023-01-12 13:52:22', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 4);
INSERT INTO `system_log` VALUES (16, '2023-01-12 13:53:07', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 1);
INSERT INTO `system_log` VALUES (17, '2023-01-12 13:53:43', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 1);
INSERT INTO `system_log` VALUES (18, '2023-01-12 14:04:57', NULL, '删除菜单表', 'Result(code=0, msg=成功, data=false)', '0:0:0:0:0:0:0:1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 1);
INSERT INTO `system_log` VALUES (19, '2023-01-16 10:35:41', '', '登录', 'Result(code=0, msg=成功, data=UserDTO(token=eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NzM5MjI5NDAsInN1YiI6ImFkbWluIiwiY3JlYXRlZCI6MTY3MzgzNjU0MDE1NX0.EVUmi9WO3bOwOKmjAq7Yp8SE82BMACtiDx2necBv4f5-WHFKl7saf1E6TW_-biz5lTh1ICaI4Hr8-hYyWjh7wg, id=1, username=admin, password=null, phone=, email=null, pwdUpdateTime=null, lockFlag=false, roles=[Admin], permissions=[], menuTree=MenuTree(id=-1, parentId=null, menuName=根菜单, menuCode=ROOT_MENU, path=null, icon=null, sort=null, children=[MenuTree(id=1, parentId=-1, menuName=系统管理, menuCode=SYSTEM_MANAG, path=null, icon=null, sort=1, children=null)]), thirdUserInfo=null, authorities=[Admin]))', '0:0:0:0:0:0:0:1', '/auth/login', 'POST', '', 2833);
INSERT INTO `system_log` VALUES (20, '2023-01-16 10:35:51', '', '登录', 'Result(code=0, msg=成功, data=UserDTO(token=eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NzM5MjI5NTAsInN1YiI6ImFkbWluIiwiY3JlYXRlZCI6MTY3MzgzNjU1MDY0N30.xLXlw6jKl3OPRPt9O-Xgf5D_EQQTqKH6Y0BbmGt4DCbYUTmNFHVBb8jmkzXd-cmg2joW2uWBllRtstxU691dEQ, id=1, username=admin, password=null, phone=, email=null, pwdUpdateTime=null, lockFlag=false, roles=[Admin], permissions=[], menuTree=MenuTree(id=-1, parentId=null, menuName=根菜单, menuCode=ROOT_MENU, path=null, icon=null, sort=null, children=[MenuTree(id=1, parentId=-1, menuName=系统管理, menuCode=SYSTEM_MANAG, path=null, icon=null, sort=1, children=null)]), thirdUserInfo=null, authorities=[Admin]))', '127.0.0.1', '/auth/login', 'POST', '', 117);
INSERT INTO `system_log` VALUES (21, '2023-01-16 10:36:33', '', '删除菜单表', 'Result(code=0, msg=成功, data=true)', '127.0.0.1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 6);
INSERT INTO `system_log` VALUES (22, '2023-01-16 10:43:18', '', '删除菜单表', 'Result(code=0, msg=成功, data=false)', '127.0.0.1', '/menu/delete/id', 'DELETE', 'id=%5B1%5D', 6);
INSERT INTO `system_log` VALUES (23, '2023-01-16 10:46:27', '', '登录', 'org.springframework.security.authentication.InternalAuthenticationServiceException: com.netflix.client.ClientException: Load balancer does not have available server for client: ralph-admin', '127.0.0.1', '/auth/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (24, '2023-01-16 10:46:52', '', '登录', '{\"code\":0,\"msg\":\"成功\",\"data\":{\"token\":\"eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NzM5MjM2MTIsInN1YiI6ImFkbWluIiwiY3JlYXRlZCI6MTY3MzgzNzIxMjE1NX0.Z4qknHgdJDaxZqtCtS2cNymxiH8xQvhcyPYkMr5n7-AOTz27sAZyWSmVmyITDf3sutcGKByaAILSoScPTY8TUg\",\"id\":1,\"username\":\"admin\",\"phone\":\"\",\"lockFlag\":false,\"roles\":[\"Admin\"],\"permissions\":[],\"menuTree\":{\"id\":-1,\"menuName\":\"根菜单\",\"menuCode\":\"ROOT_MENU\",\"children\":[{\"id\":1,\"parentId\":-1,\"menuName\":\"系统管理\",\"menuCode\":\"SYSTEM_MANAG\",\"sort\":1}]},\"authorities\":[{}]}}', '127.0.0.1', '/auth/login', 'POST', '', 379);
INSERT INTO `system_log` VALUES (25, '2023-01-16 11:30:44', '', '删除菜单表', '{\"code\":0,\"msg\":\"成功\",\"data\":false}', '127.0.0.1', '/menu/delete/id', 'DELETE', 'id=[1]', 8);
INSERT INTO `system_log` VALUES (26, '2023-01-17 14:12:26', '', '登录', '{\"code\":0,\"msg\":\"成功\",\"data\":{\"token\":\"eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NzQwMjIzNDUsInN1YiI6ImFkbWluIiwiY3JlYXRlZCI6MTY3MzkzNTk0NTM0M30.lZBBVwECswdpFRRu-IGVtB5hwj4TfPB5syQ9E6TwMJ1H1orzE1XU6I1mO3Z_b0PaklLQm6UkB8gZOOxFjh35ig\",\"id\":1,\"username\":\"admin\",\"phone\":\"\",\"lockFlag\":false,\"roles\":[\"Admin\"],\"permissions\":[],\"menuTree\":{\"id\":-1,\"menuName\":\"根菜单\",\"menuCode\":\"ROOT_MENU\",\"children\":[{\"id\":1,\"parentId\":-1,\"menuName\":\"系统管理\",\"menuCode\":\"SYSTEM_MANAG\",\"sort\":1}]},\"authorities\":[{}]}}', '127.0.0.1', '/auth/login', 'POST', '', 535);
INSERT INTO `system_log` VALUES (27, '2023-02-01 11:42:11', '', '手机号登录', 'feign.codec.DecodeException: Error while extracting response for type [com.ralph.common.core.entity.Result<com.ralph.common.core.entity.UserDTO>] and content type [application/json]; nested exception is org.springframework.http.converter.HttpMessageNotReadableException: JSON parse error: Cannot construct instance of `com.ralph.common.core.entity.UserDTO` (although at least one Creator exists): no String-argument constructor/factory method to deserialize from String value (\'Source must not be null\'); nested exception is com.fasterxml.jackson.databind.exc.MismatchedInputException: Cannot construct instance of `com.ralph.common.core.entity.UserDTO` (although at least one Creator exists): no String-argument constructor/factory method to deserialize from String value (\'Source must not be null\')\n at [Source: (PushbackInputStream); line: 1, column: 45] (through reference chain: com.ralph.common.core.entity.Result[\"data\"])', '0:0:0:0:0:0:0:1', '/auth/sms/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (28, '2023-02-01 11:43:49', '', '手机号登录', 'feign.RetryableException: Read timed out executing GET http://ralph-admin/user/sms/phone?phone=18717238089', '0:0:0:0:0:0:0:1', '/auth/sms/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (29, '2023-02-01 11:44:55', '', '手机号登录', 'java.lang.RuntimeException: com.netflix.client.ClientException: Load balancer does not have available server for client: ralph-admin', '0:0:0:0:0:0:0:1', '/auth/sms/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (30, '2023-02-01 11:45:08', '', '手机号登录', 'java.lang.RuntimeException: com.netflix.client.ClientException: Load balancer does not have available server for client: ralph-admin', '0:0:0:0:0:0:0:1', '/auth/sms/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (31, '2023-02-01 11:45:15', '', '手机号登录', 'BusinessException(code=3019, msg=获取用户信息失败)', '0:0:0:0:0:0:0:1', '/auth/sms/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (32, '2023-02-01 11:45:29', '', '手机号登录', '{\"code\":0,\"msg\":\"成功\",\"data\":{\"token\":\"eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NzUzMDk1MjgsInN1YiI6ImFkbWluIiwiY3JlYXRlZCI6MTY3NTIyMzEyODQ2N30.PEskpDUM23qMSjyUn6ZKUt6_AcJH01MJdAN5Aw7rqURmLO9NaAClfWULIoy1A9ceqfH72QAYOhBdZ2DgA9wAyg\",\"id\":1,\"username\":\"admin\",\"phone\":\"18717238089\",\"lockFlag\":false,\"roles\":[\"Admin\"],\"permissions\":[],\"menuTree\":{\"id\":-1,\"menuName\":\"根菜单\",\"menuCode\":\"ROOT_MENU\",\"children\":[{\"id\":1,\"parentId\":-1,\"menuName\":\"系统管理\",\"menuCode\":\"SYSTEM_MANAG\",\"sort\":1}]},\"authorities\":[{}]}}', '0:0:0:0:0:0:0:1', '/auth/sms/login', 'POST', '', 641);
INSERT INTO `system_log` VALUES (33, '2023-02-01 13:45:37', '', '登录', 'org.springframework.security.authentication.InternalAuthenticationServiceException', '127.0.0.1', '/auth/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (34, '2023-02-01 13:46:31', '', '登录', 'org.springframework.security.authentication.InternalAuthenticationServiceException', '127.0.0.1', '/auth/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (35, '2023-02-01 13:46:41', '', '登录', 'org.springframework.security.authentication.InternalAuthenticationServiceException', '127.0.0.1', '/auth/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (36, '2023-02-01 13:54:24', '', '登录', 'org.springframework.security.authentication.InternalAuthenticationServiceException: com.netflix.client.ClientException: Load balancer does not have available server for client: ralph-admin', '127.0.0.1', '/auth/login', 'POST', '', NULL);
INSERT INTO `system_log` VALUES (37, '2023-02-01 13:54:44', '', '登录', '{\"code\":0,\"msg\":\"成功\",\"data\":{\"token\":\"eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NzUzMTcyODMsInN1YiI6ImFkbWluIiwiY3JlYXRlZCI6MTY3NTIzMDg4MzkxNX0.qyAUDIgQIVIt2h4qPxd7muICiMdUE4A90r-YafwN7UblHlGQDc20bqvcLpYc4XkdMEU0yqVXMD4ZnRqXD8YjLA\",\"id\":1,\"username\":\"admin\",\"phone\":\"18717238089\",\"lockFlag\":false,\"roles\":[\"Admin\"],\"permissions\":[],\"menuTree\":{\"id\":-1,\"menuName\":\"根菜单\",\"menuCode\":\"ROOT_MENU\",\"children\":[{\"id\":1,\"parentId\":-1,\"menuName\":\"系统管理\",\"menuCode\":\"SYSTEM_MANAG\",\"sort\":1}]},\"authorities\":[{}]}}', '127.0.0.1', '/auth/login', 'POST', '', 413);
INSERT INTO `system_log` VALUES (38, '2023-02-03 17:13:08', '', '登录', '{\"code\":0,\"msg\":\"成功\",\"data\":{\"token\":\"eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NzU1MDE5ODcsInN1YiI6ImFkbWluIiwiY3JlYXRlZCI6MTY3NTQxNTU4NzMzNX0.ggy_OghLWsBKAsT18ub53EJaLtFHtTUF933zw8mCCidY2cRRFHTO7SF7dOlGmAqLrUKS0h7wG3KmdoxD6TVSMg\",\"id\":1,\"username\":\"admin\",\"phone\":\"18717238089\",\"lockFlag\":false,\"roles\":[\"Admin\"],\"permissions\":[],\"menuTree\":{\"id\":-1,\"menuName\":\"根菜单\",\"menuCode\":\"ROOT_MENU\",\"children\":[{\"id\":1,\"parentId\":-1,\"menuName\":\"系统管理\",\"menuCode\":\"SYSTEM_MANAG\",\"sort\":1}]},\"authorities\":[{}]}}', '127.0.0.1', '/auth/login', 'POST', '', 1703);
INSERT INTO `system_log` VALUES (39, '2023-02-03 17:13:08', '', '登录', '{\"code\":0,\"msg\":\"成功\",\"data\":{\"token\":\"eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NzU1MDE5ODcsInN1YiI6ImFkbWluIiwiY3JlYXRlZCI6MTY3NTQxNTU4NzMzNX0.ggy_OghLWsBKAsT18ub53EJaLtFHtTUF933zw8mCCidY2cRRFHTO7SF7dOlGmAqLrUKS0h7wG3KmdoxD6TVSMg\",\"id\":1,\"username\":\"admin\",\"phone\":\"18717238089\",\"lockFlag\":false,\"roles\":[\"Admin\"],\"permissions\":[],\"menuTree\":{\"id\":-1,\"menuName\":\"根菜单\",\"menuCode\":\"ROOT_MENU\",\"children\":[{\"id\":1,\"parentId\":-1,\"menuName\":\"系统管理\",\"menuCode\":\"SYSTEM_MANAG\",\"sort\":1}]},\"authorities\":[{}]}}', '127.0.0.1', '/auth/login', 'POST', '', 1703);

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu`  (
                                `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                `parent_id` int(11) NOT NULL COMMENT '父菜单id',
                                `menu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
                                `menu_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单编码',
                                `permission` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
                                `path` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单地址',
                                `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单icon',
                                `sort` int(11) NULL DEFAULT NULL COMMENT '菜单排序',
                                `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单类型；0：菜单，1：按钮',
                                PRIMARY KEY (`id`) USING BTREE,
                                UNIQUE INDEX `menu_code_index`(`menu_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_menu
-- ----------------------------
INSERT INTO `system_menu` VALUES (1, 0, '2023-01-04 13:45:24', NULL, '2023-02-03 17:25:59', 'admin', 201, -1, '系统管理', 'SYSTEM_MANAG', NULL, NULL, NULL, 1, '0');

-- ----------------------------
-- Table structure for system_menu_role_ref
-- ----------------------------
DROP TABLE IF EXISTS `system_menu_role_ref`;
CREATE TABLE `system_menu_role_ref`  (
                                         `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                         `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                         `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                         `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                         `role_id` int(11) NOT NULL COMMENT '角色id',
                                         `menu_id` int(11) NOT NULL COMMENT '菜单id',
                                         PRIMARY KEY (`role_id`, `menu_id`) USING BTREE,
                                         INDEX `menu_id`(`menu_id`) USING BTREE,
                                         CONSTRAINT `menu_id` FOREIGN KEY (`menu_id`) REFERENCES `system_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                         CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `system_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_menu_role_ref
-- ----------------------------
INSERT INTO `system_menu_role_ref` VALUES ('2023-01-04 13:46:03', NULL, '2023-01-04 13:46:03', NULL, 1, 1);

-- ----------------------------
-- Table structure for system_param
-- ----------------------------
DROP TABLE IF EXISTS `system_param`;
CREATE TABLE `system_param`  (
                                 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                 `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                 `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                 `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                 `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                 `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                 `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                 `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'key',
                                 `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'value',
                                 `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
                                 PRIMARY KEY (`id`) USING BTREE,
                                 UNIQUE INDEX `key_index`(`key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_param
-- ----------------------------

-- ----------------------------
-- Table structure for system_role
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role`  (
                                `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                `role_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色编码',
                                `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_role
-- ----------------------------
INSERT INTO `system_role` VALUES (1, 0, '2023-01-03 11:54:18', NULL, '2023-01-03 11:54:18', NULL, 0, 'Admin', '管理员');

-- ----------------------------
-- Table structure for system_router
-- ----------------------------
DROP TABLE IF EXISTS `system_router`;
CREATE TABLE `system_router`  (
                                  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                  `del_flag` int(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                  `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                  `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                  `route_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '路由id',
                                  `route_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由名称',
                                  `uri` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'uri',
                                  `predicates` json NULL COMMENT '断言规则',
                                  `filters` json NULL COMMENT '过滤规则',
                                  `order` int(11) NULL DEFAULT NULL COMMENT '排序',
                                  PRIMARY KEY (`id`) USING BTREE,
                                  UNIQUE INDEX `route_id_index`(`route_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '动态路由' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_router
-- ----------------------------
INSERT INTO `system_router` VALUES (1, 0, '2023-01-10 11:21:10', NULL, '2023-01-11 14:18:27', NULL, 0, 'ralph-admin', 'ralph-admin', 'lb://ralph-admin', '[{\"args\": {\"_genkey_0\": \"/admin/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"_genkey_0\": \"1\"}, \"name\": \"StripPrefix\"}]', 1);
INSERT INTO `system_router` VALUES (2, 0, '2023-01-10 11:21:10', NULL, '2023-01-11 14:07:58', NULL, 0, 'ralph-business-article', 'ralph-business-article', 'lb://ralph-business-article', '[{\"args\": {\"_genkey_0\": \"/article/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"_genkey_0\": \"1\"}, \"name\": \"StripPrefix\"}]', 1);
INSERT INTO `system_router` VALUES (3, 0, '2023-01-10 11:21:10', NULL, '2023-01-10 16:17:31', NULL, 0, 'ralph-oauth', 'ralph-oauth', 'lb://ralph-oauth', '[{\"args\": {\"_genkey_0\": \"/oauth/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"_genkey_0\": \"1\"}, \"name\": \"StripPrefix\"}]', 1);

-- ----------------------------
-- Table structure for system_third_login
-- ----------------------------
DROP TABLE IF EXISTS `system_third_login`;
CREATE TABLE `system_third_login`  (
                                       `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                       `del_flag` int(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                       `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                       `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                       `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                       `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                       `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                       `user_id` int(11) NOT NULL COMMENT '关联用户id',
                                       `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录类型',
                                       `third_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '第三方名称',
                                       `third_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '第三方唯一id',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '第三方登录信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_third_login
-- ----------------------------
INSERT INTO `system_third_login` VALUES (1, 0, '2023-01-13 21:47:44', NULL, '2023-01-13 21:47:44', NULL, 0, 1, 'gitee', NULL, '9403744');

-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user`  (
                                `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
                                `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
                                `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
                                `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
                                `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
                                `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                                `avatar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像地址',
                                `lock_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '锁定标识',
                                `pwd_update_time` datetime NULL DEFAULT NULL COMMENT '密码更新时间',
                                PRIMARY KEY (`id`) USING BTREE,
                                UNIQUE INDEX `username_index`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO `system_user` VALUES (1, 0, '2022-12-28 15:36:09', NULL, '2023-02-01 11:45:25', NULL, 0, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '18717238089', NULL, NULL, 0, NULL);

-- ----------------------------
-- Table structure for system_user_role_ref
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role_ref`;
CREATE TABLE `system_user_role_ref`  (
                                         `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                         `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                         `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                         `update_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新时间',
                                         `user_id` int(11) NOT NULL COMMENT '用户id',
                                         `role_id` int(11) NOT NULL COMMENT '角色id',
                                         PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
                                         INDEX `role_id`(`role_id`) USING BTREE,
                                         CONSTRAINT `system_user_role_ref_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `system_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                         CONSTRAINT `system_user_role_ref_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `system_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_user_role_ref
-- ----------------------------
INSERT INTO `system_user_role_ref` VALUES ('2023-01-03 11:54:30', NULL, '2023-01-03 11:54:30', NULL, 1, 1);

SET FOREIGN_KEY_CHECKS = 1;