#!/usr/bin/env bash

num=`ps -ef |grep ralph-oauth |grep java |awk '{print $2}'`
[ ! -z "$num" ] && kill -9 $num && echo "成功杀死进程"

cd /data/lib
nohup java -Dfile.encoding=utf-8 -Duser.timezone=GMT+8 -jar ralph-oauth.jar  > /dev/null 2>&1 &
