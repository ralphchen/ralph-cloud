
num=`ps -ef |grep minio |grep java |awk '{print $2}'`
[ ! -z "$num" ] && kill -9 $num && echo "成功杀死进程"

nohup /data/minio/minio server /data/minio/data --console-address :34924 > /data/minio/minio.log 2>&1&
