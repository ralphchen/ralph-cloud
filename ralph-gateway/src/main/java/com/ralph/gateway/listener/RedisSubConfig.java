package com.ralph.gateway.listener;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

import java.util.ArrayList;

@Configuration
public class RedisSubConfig {

    /**
     * 路由更新响应topic
     */
    public static final String ROUTE_RES_TOPIC = "systemRouteResponseTopic";
 
    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory factory, RedisMessageListener listener) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(factory);
        ArrayList<ChannelTopic> channelTopics = Lists.newArrayList(
                new ChannelTopic(ROUTE_RES_TOPIC));
        container.addMessageListener(listener, channelTopics);
        return container;
    }
}