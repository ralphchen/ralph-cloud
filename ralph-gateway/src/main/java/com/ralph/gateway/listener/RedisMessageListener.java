package com.ralph.gateway.listener;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.ralph.gateway.config.GatewayServiceHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RedisMessageListener implements MessageListener {

    private final RedisTemplate redisTemplate;

    private final GatewayServiceHandler gatewayServiceHandler;

    @Override
    public void onMessage(Message message, byte[] pattern) {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        String channel = strDeserialize(message.getChannel()).toString();
        if (RedisSubConfig.ROUTE_RES_TOPIC.equals(channel)){
            String routeStr = deserialize(message.getBody()).toString();
            gatewayServiceHandler.loadRouteConfig(JSONUtil.parseArray(routeStr).toList(JSONObject.class));
        }
    }

    private Object strDeserialize(byte[] params){
        return redisTemplate.getStringSerializer().deserialize(params);
    }

    private Object deserialize(byte[] params){
        return redisTemplate.getValueSerializer().deserialize(params);
    }
}