package com.ralph.gateway.config;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 *
 * 核心配置类，项目初始化加载数据库的路由配置
 *
**/

@Slf4j
@Service
public class GatewayServiceHandler implements CommandLineRunner {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 路由缓存key
     */
    private static final String GATEWAY_ROUTES = "gateway:routes";

    // springboot启动后执行
    @Override
    public void run(String... args){
        // 向admin模块发送消息，更新路由列表
        log.info("更新路由信息");
        redisTemplate.convertAndSend("systemRouteRequestTopic","1");
    }

    @SuppressWarnings("all")
    public void loadRouteConfig(List<JSONObject> routeList) {
        redisTemplate.delete(GATEWAY_ROUTES);
        routeList.forEach(gatewayRoute -> {
            RouteDefinition definition = new RouteDefinition();

            List<PredicateDefinition> predicateList = new ArrayList<>();
            List<FilterDefinition> filterList = new ArrayList<>();

            URI uri = URI.create(gatewayRoute.getStr("uri"));

            definition.setId(gatewayRoute.getStr("routeId"));
            // 名称是固定的，spring gateway会根据名称找对应的PredicateFactory
            JSONArray predicates = gatewayRoute.getJSONArray("predicates");
            for (Object temObj : predicates) {
                PredicateDefinition predicate = new PredicateDefinition();
                JSONObject temJson = (JSONObject)temObj;
                predicate.setName(temJson.getStr("name"));
                predicate.setArgs(temJson.get("args",Map.class));
                predicateList.add(predicate);
            }

            JSONArray filters = gatewayRoute.getJSONArray("filters");
            for (Object temObj : filters) {
                FilterDefinition filter = new FilterDefinition();
                JSONObject temJson = (JSONObject)temObj;
                filter.setName(temJson.getStr("name"));
                filter.setArgs(temJson.get("args",Map.class));
                filterList.add(filter);
            }

            definition.setPredicates(predicateList);
            definition.setFilters(filterList);

            definition.setUri(uri);
            redisTemplate.setKeySerializer(new StringRedisSerializer());
            redisTemplate.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(RouteDefinition.class));
            redisTemplate.opsForHash().put(GATEWAY_ROUTES, definition.getId(),definition);
        });
    }
}
