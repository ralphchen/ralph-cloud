package com.ralph.gateway;

import com.ralph.common.docs.annotation.EnableRalphSwagger2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
@Slf4j
@EnableRalphSwagger2
public class GatewayApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(GatewayApplication.class, args);
        Environment environment = context.getBean(Environment.class);
        log.info("\n\t---------------------------------------------------"
                + "\n\tApplication " + environment.getProperty("spring.application.name")
                + " is running!\n\tlocal：http://localhost:" + environment.getProperty("server.port")
                +"\n\t---------------------------------------------------");
    }
}
