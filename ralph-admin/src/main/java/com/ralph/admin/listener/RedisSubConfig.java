package com.ralph.admin.listener;

import com.google.common.collect.Lists;
import com.ralph.common.core.constant.RedisKeyConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

import java.util.ArrayList;

@Configuration
public class RedisSubConfig {
 
    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory factory, RedisMessageListener listener) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(factory);
        ArrayList<ChannelTopic> channelTopics = Lists.newArrayList(
                new ChannelTopic(RedisKeyConstant.SYS_LOG_TYPE_TOPIC),
                new ChannelTopic(RedisKeyConstant.SYS_ROUTE_REQ_TOPIC));
        container.addMessageListener(listener, channelTopics);
        // 使用lambda，不需要创建listener类
        // container.addMessageListener((message,bytes)->{
        //     // 业务逻辑
        // },channelTopics);
        return container;
    }
}