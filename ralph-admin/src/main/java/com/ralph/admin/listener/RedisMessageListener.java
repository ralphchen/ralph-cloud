package com.ralph.admin.listener;

import com.ralph.admin.service.SystemLogService;
import com.ralph.admin.service.SystemRouterService;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.entity.SystemLog;
import com.ralph.common.core.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RedisMessageListener implements MessageListener {

    private final SystemLogService systemLogService;

    private final SystemRouterService systemRouterService;

    @Override
    public void onMessage(Message message, byte[] pattern) {

        String channel = RedisUtils.strDeserialize(message.getChannel()).toString();
        switch (channel){
            case RedisKeyConstant.SYS_LOG_TYPE_TOPIC:
                SystemLog systemLog = (SystemLog)RedisUtils.deserialize(message.getBody());
                systemLogService.save(systemLog);
                break;
            case RedisKeyConstant.SYS_ROUTE_REQ_TOPIC:
                systemRouterService.getRouteList();
                break;
        }
    }
}