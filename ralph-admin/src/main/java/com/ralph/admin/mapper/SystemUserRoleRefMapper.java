package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemUserRoleRef;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2022-12-29
 */
@Mapper
public interface SystemUserRoleRefMapper extends BaseMapper<SystemUserRoleRef> {

}