package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统角色 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2022-12-29
 */
@Mapper
public interface SystemRoleMapper extends BaseMapper<SystemRole> {

    List<SystemRole> selectRoleByUsername(@Param("username") String username);
}