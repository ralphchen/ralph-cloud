package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemMenuRoleRef;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 菜单角色关联表 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2023-01-04
 */
@Mapper
public interface SystemMenuRoleRefMapper extends BaseMapper<SystemMenuRoleRef> {

}