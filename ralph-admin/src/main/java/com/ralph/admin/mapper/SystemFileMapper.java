package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemFile;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 文件管理 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2023-01-06
 */
@Mapper
public interface SystemFileMapper extends BaseMapper<SystemFile> {

}