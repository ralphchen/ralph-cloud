package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemParam;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
@Mapper
public interface SystemParamMapper extends BaseMapper<SystemParam> {

}