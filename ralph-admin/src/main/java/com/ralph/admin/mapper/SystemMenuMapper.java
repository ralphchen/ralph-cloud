package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2023-01-04
 */
@Mapper
public interface SystemMenuMapper extends BaseMapper<SystemMenu> {

    /**
     * 根据用户名获取菜单列表
     *
     * @param username 用户名
     * @return 菜单列表
     */
    List<SystemMenu> selectByUsername(@Param("username") String username);
}