package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemDictItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 字典项 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
@Mapper
public interface SystemDictItemMapper extends BaseMapper<SystemDictItem> {

    /**
     * 根据字典类型获取字典项
     * @param type 字典类型
     * @return 字典项列表
     */
    List<SystemDictItem> getDictItemByType(@Param("type") String type);

    String getTypeByItemId(@Param("itemId")Integer itemId,@Param("dictId")Integer dictId);
}