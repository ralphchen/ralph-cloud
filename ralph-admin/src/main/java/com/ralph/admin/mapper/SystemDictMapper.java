package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemDict;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
@Mapper
public interface SystemDictMapper extends BaseMapper<SystemDict> {

}