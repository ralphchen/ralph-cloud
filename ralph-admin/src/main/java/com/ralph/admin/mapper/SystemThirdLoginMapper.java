package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemThirdLogin;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 第三方登录信息 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2023-01-13
 */
@Mapper
public interface SystemThirdLoginMapper extends BaseMapper<SystemThirdLogin> {

}