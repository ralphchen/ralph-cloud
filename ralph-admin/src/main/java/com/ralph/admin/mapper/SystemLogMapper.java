package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.common.core.entity.SystemLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 操作日志 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2022-12-16
 */
@Mapper
public interface SystemLogMapper extends BaseMapper<SystemLog> {

}