package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemRouter;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 动态路由 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2023-01-10
 */
@Mapper
public interface SystemRouterMapper extends BaseMapper<SystemRouter> {

}