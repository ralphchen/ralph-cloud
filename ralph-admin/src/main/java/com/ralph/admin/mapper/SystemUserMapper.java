package com.ralph.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ralph.admin.entity.SystemUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author ralph
 * @since 2022-12-20
 */
@Mapper
public interface SystemUserMapper extends BaseMapper<SystemUser> {

}