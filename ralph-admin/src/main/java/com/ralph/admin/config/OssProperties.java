package com.ralph.admin.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * oss配置文件信息
 */
@Data
@Configuration
@ConfigurationProperties("ralph.oss")
public class OssProperties {

    private String endpoint;

    private String accessKey;

    private String secretKey;

    private String bucketName;
}
