package com.ralph.admin.utils;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.google.common.collect.Lists;
import com.ralph.common.core.utils.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@SuppressWarnings(value = "all")
public class OssUtils {

    private static AmazonS3 amazonS3;

    @Autowired
    public OssUtils(AmazonS3 amazonS3){
        OssUtils.amazonS3 = amazonS3;
    }

    //============================ bucket相关 ==================================//

    /**
     * 判断bucket是否存在
     * @param bucketName bucketName
     * @return 是否存在
     */
    public static boolean buckedExists(String bucketName){
        if (StringUtils.isBlank(bucketName)){
            log.info("bucketName 为空");
            return false;
        }
        return amazonS3.doesBucketExistV2(bucketName);
    }

    /**
     * 创建bucket
     * @param bucketName bucketName
     * @return 创建结果
     */
    public static boolean createBucket(String bucketName){
        if (buckedExists(bucketName)) {
            Bucket bucket = amazonS3.createBucket((bucketName));
            return bucket != null;
        }
        return false;
    }

    /**
     * 获取所有bucket
     * @return bucket列表
     */
    public static List<Bucket> bucketList(){
        return amazonS3.listBuckets();
    }

    /**
     * 删除bucket
     * @param bucketName bucketName
     */
    public static void deleteBucket(String bucketName){
        amazonS3.deleteBucket(bucketName);
    }

    //============================ 文件相关 ==================================//

    /**
     * 上传文件
     * @param bucketName bucketName
     * @param fileName fileName
     * @param file file
     * @return 文件信息
     */
    public static PutObjectResult uploadFile(String bucketName, String fileName, File file){
        return amazonS3.putObject(bucketName, fileName, file);
    }

    /**
     * 上传文件
     * @param bucketName bucketName
     * @param fileName fileName
     * @param inputStream 文件流
     * @return 文件信息
     */
    public static PutObjectResult uploadFile(String bucketName, String fileName, InputStream inputStream){
        ObjectMetadata objectMetadata = new ObjectMetadata();
        try {
            byte[] bytes = IOUtils.toByteArray(inputStream);
            objectMetadata.setContentLength(inputStream.available());
            objectMetadata.setContentType("application/octet-stream");
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        }catch (Exception e){
            log.error("上传文件失败，" + e);
        }
        return amazonS3.putObject(bucketName, fileName, inputStream, objectMetadata);
    }

    /**
     * 批量删除文件
     * @param bucketName bucketName
     * @param fileNames 文件列表
     */
    public static void deleteFile(String bucketName, String... fileNames){
        List<DeleteObjectsRequest.KeyVersion> collect = Lists.newArrayList(fileNames).stream()
                .map(DeleteObjectsRequest.KeyVersion::new)
                .collect(Collectors.toList());
        DeleteObjectsRequest deleteObjectsRequest = new DeleteObjectsRequest(bucketName);
        deleteObjectsRequest.setKeys(collect);
        amazonS3.deleteObjects(deleteObjectsRequest);
    }

    /**
     * 获取文件
     * @param bucketName bucketName
     * @param fileName fileName
     * @return 文件
     */
    public static S3Object getFile(String bucketName, String fileName){
        return amazonS3.getObject(bucketName, fileName);
    }

    /**
     * 批量获取文件信息
     * @param bucketName bucketName
     * @param prefix prefix
     * @return 文件信息列表
     */
    public static List<S3ObjectSummary> getFileList(String bucketName, String prefix){
        ListObjectsV2Result listObjectsV2Result;
        if (StringUtils.isBlank(prefix)){
            listObjectsV2Result = amazonS3.listObjectsV2(bucketName);
        }else {
            listObjectsV2Result = amazonS3.listObjectsV2(bucketName, prefix);
        }
        return listObjectsV2Result.getObjectSummaries();
    }

    /**
     * 获取文件外链
     * @param bucketName bucketName
     * @param filename filename
     * @param expire 过期时间
     * @return url
     */
    public static String getFileUrl(String bucketName, String filename, Date expire){
        URL url = amazonS3.generatePresignedUrl(bucketName, filename, expire);
        return url.toString();
    }

    /**
     * 大文件分片上传
     *
     * @param bucketName bucketName
     * @param fileName fileName
     * @param file 文件
     */
    public static void mutailpartUpload(String bucketName, String fileName, MultipartFile file){
        long size = file.getSize();
        // 分片最小尺寸
        long minPartSize = 5 * 1024 * 1024;

        // 得到总共的段数，和 分段后，每个段的开始上传的字节位置
        List<Long> positions = new ArrayList<>();
        long filePosition = 0;
        while (filePosition < size) {
            positions.add(filePosition);
            filePosition += Math.min(minPartSize, (size - filePosition));
        }

        // 创建一个列表保存所有分传的 PartETag, 在分段完成后会用到
        List<PartETag> partETags = new ArrayList<>();

        // 第一步，初始化，声明下面将有一个 Multipart Upload
        InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucketName, fileName);
        InitiateMultipartUploadResult initResponse = amazonS3.initiateMultipartUpload(initRequest);
        if (null == initResponse){
            return;
        }

        try {
            // MultipartFile 转 File
            File toFile = FileUtils.multipartFileToFile(file);
            // 开始分片上传
            for (int i = 0; i < positions.size(); i++) {
                final int finalI = i;
                UploadPartRequest uploadRequest = new UploadPartRequest()
                        .withUploadId(initResponse.getUploadId())
                        .withBucketName(bucketName)
                        .withKey(fileName)
                        .withPartSize(Math.min(minPartSize, (size - positions.get(finalI))))
                        .withPartNumber(finalI + 1)
                        .withFile(toFile)
                        .withFileOffset(positions.get(finalI));
                partETags.add(amazonS3.uploadPart(uploadRequest).getPartETag());
            }

            // 第三步，完成上传，合并分段
            CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(bucketName, fileName,
                    initResponse.getUploadId(), partETags);
            amazonS3.completeMultipartUpload(compRequest);
        }catch (Exception e){
            log.error("上传失败");
        }
    }
}
