package com.ralph.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemToken;
import com.ralph.admin.service.SystemTokenService;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.common.core.annotation.IgnoreUrl;
import com.ralph.common.core.entity.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * token管理
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("token")
@Api(value = "token", tags = "token管理")
public class SystemTokenController {

    private final SystemTokenService systemTokenService;

    /**
     * token分页
     * @param page 分页信息
     * @param systemToken token信息
     * @return token分页结果
     */
    @IgnoreUrl
    @ApiOperation(value = "token分页", notes = "token分页")
    @GetMapping("/page")
    public Result<Page> testRedis(@ApiNeedField({"size","current"}) Page page, SystemToken systemToken){
        return Result.success(systemTokenService.tokenPage(page, systemToken));
    }

    /**
     * 删除token
     * @param username 用户名
     * @return token分页结果
     */
    @IgnoreUrl
    @ApiOperation(value = "删除token", notes = "删除token")
    @DeleteMapping("/delete")
    public Result<Boolean> testRedis(String username){
        return Result.success(systemTokenService.delete(username));
    }
}
