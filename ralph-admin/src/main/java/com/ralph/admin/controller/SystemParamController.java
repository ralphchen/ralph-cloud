package com.ralph.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemParam;
import com.ralph.admin.service.SystemParamService;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.core.utils.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

/**
 * @auther ralph
 * @create 2022-12-19
 * @describe 参数配置表前端控制器
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("param")
@Api(value = "param", tags = "参数配置")
public class SystemParamController {

    private final SystemParamService systemParamService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemParam 参数配置表
     * @return Result
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Result<Page<SystemParam>> getSystemParamPage(@ApiNeedField({"size","current"}) Page<SystemParam> page, SystemParam systemParam) {
        return Result.success(systemParamService.page(page, Wrappers.query(systemParam)
                .lambda().orderByDesc(SystemParam::getCreateDate)));
    }

    /**
     * 通过主键查询参数配置表
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/get/id")
    public Result<SystemParam> getById(@RequestParam("id") Integer id) {
        return Result.success(systemParamService.getById(id));
    }

    /**
     * 新增参数配置表
     * @param systemParam 参数配置表
     * @return Result
     */
    @ActionLog("新增参数配置表")
    @ApiOperation(value = "新增参数配置表", notes = "新增参数配置表")
    @PostMapping("/insert")
    public Result<Boolean> save(@RequestBody SystemParam systemParam) {
        return Result.success(systemParamService.saveOrUpdate(systemParam));
    }

    /**
     * 修改参数配置表
     * @param systemParam 参数配置表
     * @return Result
     */
    @ActionLog("修改参数配置表")
    @ApiOperation(value = "修改参数配置表", notes = "修改参数配置表")
    @PutMapping("/update")
    public Result<Boolean> updateById(@RequestBody  SystemParam systemParam) {
        if (null == systemParam){
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        return Result.success(systemParamService.saveOrUpdate(systemParam));
    }

    /**
     * 通过主键删除参数配置表
     * @param id 主键
     * @return Result
     */
    @ActionLog("删除参数配置表")
    @ApiOperation(value = "通过主键删除参数配置表", notes = "通过主键删除参数配置表")
    @DeleteMapping("/delete/id")
    public Result<Boolean> removeById(@RequestParam("id") Integer id) {
        SystemParam param = systemParamService.getById(id);
        if (null == param){
            return Result.failed("参数配置不存在");
        }
        RedisUtils.del(RedisKeyConstant.ADMIN_PARAMS + ":" + param.getParamKey());
        return Result.success(systemParamService.removeById(id));
    }

    /**
     * 通过key查询参数配置表
     * @param key 主键
     * @return Result
     */
    @ApiOperation(value = "通过key查询参数配置表", notes = "通过key查询参数配置表")
    @GetMapping("/get/key")
    @Cacheable(value = RedisKeyConstant.ADMIN_PARAMS, key = "#key", unless = "#result.data == null")
    public Result<SystemParam> getByKey(@RequestParam("key") String key) {
        return Result.success(systemParamService.getByKey(key));
    }
}
