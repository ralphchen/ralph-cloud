package com.ralph.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemUser;
import com.ralph.admin.entity.form.PermissionForm;
import com.ralph.admin.service.SystemUserService;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.core.entity.Groups;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.common.core.annotation.IgnoreUrl;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.entity.UserDTO;
import com.ralph.common.core.exceptions.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotBlank;

/**
 * @auther ralph
 * @create 2022-12-20
 * @describe 用户表前端控制器
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("user")
@Api(value = "user", tags = "用户表")
public class SystemUserController {

    private final SystemUserService systemUserService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemUser 用户表
     * @return Result
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Result<Page<SystemUser>> getSystemUserPage(@ApiNeedField({"size","current"}) Page<SystemUser> page, SystemUser systemUser) {
        return Result.success(systemUserService.page(page, Wrappers.query(systemUser).lambda()
                .like(SystemUser::getUsername,systemUser.getUsername())));
    }

    /**
     * 通过主键查询用户表
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/get/id")
    public Result<SystemUser> getById(@RequestParam("id") Integer id) {
        return Result.success(systemUserService.getById(id));
    }

    /**
     * 新增用户表
     * @param systemUser 用户表
     * @return Result
     */
    @ActionLog("新增用户表")
    @ApiOperation(value = "新增用户表", notes = "新增用户表")
    @PostMapping("/insert")
    public Result<Boolean> save(@RequestBody @Validated(Groups.Create.class) SystemUser systemUser) {
        return Result.success(systemUserService.saveOrUpdateUser(systemUser));
    }

    /**
     * 修改用户表
     * @param systemUser 用户表
     * @return Result
     */
    @ActionLog("修改用户表")
    @ApiOperation(value = "修改用户表", notes = "修改用户表")
    @PutMapping("/update")
    public Result<Boolean> updateById(@RequestBody @Validated(Groups.Update.class) SystemUser systemUser) {
        if (null == systemUser.getId()){
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        return Result.success(systemUserService.saveOrUpdateUser(systemUser));
    }

    /**
     * 通过主键删除用户表
     * @param id 主键
     * @return Result
     */
    @ActionLog("删除用户表")
    @ApiOperation(value = "通过主键删除用户表", notes = "通过主键删除用户表")
    @DeleteMapping("/delete/id" )
    public Result<Boolean> removeById(@RequestParam Integer id) {
        return Result.success(systemUserService.removeById(id));
    }

    /**
     * 根据用户名查询用户信息
     * @param username 用户名
     * @return Result
     */
    @IgnoreUrl
    @ApiIgnore
    @GetMapping("/userinfo/username" )
    public Result<UserDTO> getUserByUsername(String username){
        return Result.success(systemUserService.getUserByUsername(username));
    }

    /**
     * 给用户分配角色
     * @param permissionForm 权限信息
     * @return Result
     */
    @ApiOperation(value = "给用户分配角色", notes = "给用户分配角色")
    @PostMapping("/permission" )
    public Result<Boolean> distributeAuth(@RequestBody @Validated PermissionForm permissionForm){
        return Result.success(systemUserService.distributeAuth(permissionForm));
    }

    /**
     * 根据第三方登录信息获取用户信息
     * @param thirdInfo 第三方登录信息（加密）
     * @return 用户信息
     */
    @IgnoreUrl
    @ApiIgnore
    @GetMapping("/third/key" )
    public Result<UserDTO> getUserInfoByThirdKey(@NotBlank String thirdInfo){
        return Result.success(systemUserService.getUserInfoByThirdKey(thirdInfo));
    }

    /**
     * 根据手机号获取用户信息
     * @param phone 手机号
     * @return 用户信息
     */
    @IgnoreUrl
    @ApiIgnore
    @GetMapping("/sms/phone" )
    public Result<UserDTO> getUserInfoByPhone(@NotBlank String phone){
        return Result.success(systemUserService.getUserInfoByPhone(phone));
    }
}
