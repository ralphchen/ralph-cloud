package com.ralph.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemRole;
import com.ralph.admin.entity.form.PermissionForm;
import com.ralph.admin.service.SystemRoleService;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.exceptions.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @auther ralph
 * @create 2022-12-29
 * @describe 系统角色前端控制器
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("role")
@Api(value = "role", tags = "系统角色")
public class SystemRoleController {

    private final SystemRoleService systemRoleService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemRole 系统角色
     * @return Result
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Result<Page<SystemRole>> getSystemRolePage(@ApiNeedField({"size","current"}) Page<SystemRole> page, SystemRole systemRole) {
        return Result.success(systemRoleService.page(page, Wrappers.query(systemRole)));
    }

    /**
     * 通过主键查询系统角色
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/get/id")
    public Result<SystemRole> getById(@RequestParam("id") Integer id) {
        return Result.success(systemRoleService.getById(id));
    }

    /**
     * 新增系统角色
     * @param systemRole 系统角色
     * @return Result
     */
    @ActionLog("新增系统角色")
    @ApiOperation(value = "新增系统角色", notes = "新增系统角色")
    @PostMapping("/insert")
    public Result<Boolean> save(@RequestBody SystemRole systemRole) {
        return Result.success(systemRoleService.save(systemRole));
    }

    /**
     * 修改系统角色
     * @param systemRole 系统角色
     * @return Result
     */
    @ActionLog("修改系统角色")
    @ApiOperation(value = "修改系统角色", notes = "修改系统角色")
    @PutMapping("/update")
    public Result<Boolean> updateById(@RequestBody SystemRole systemRole) {
        if (null == systemRole.getId()){
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        return Result.success(systemRoleService.updateById(systemRole));
    }

    /**
     * 通过主键删除系统角色
     * @param id 主键
     * @return Result
     */
    @ActionLog("删除系统角色")
    @ApiOperation(value = "通过主键删除系统角色", notes = "通过主键删除系统角色")
    @DeleteMapping("/delete/id" )
    public Result<Boolean> removeById(@RequestParam("id") Integer id) {
        return Result.success(systemRoleService.removeById(id));
    }

    /**
     * 根据用户名查询角色列表
     * @param username 用户名
     * @return Result
     */
    @ApiOperation(value = "根据用户名查询角色列表", notes = "根据用户名查询角色列表")
    @GetMapping("/username/roles" )
    public Result<List<SystemRole>> getRoleListByUser(String username){
        return Result.success(systemRoleService.getRoleListByUser(username));
    }

    /**
     * 给角色分配菜单
     * @param permissionForm 权限信息
     * @return Result
     */
    @ApiOperation(value = "给角色分配菜单", notes = "给角色分配菜单")
    @PostMapping("/role/permission" )
    public Result<Boolean> distributeAuth(@RequestBody @Validated PermissionForm permissionForm){
        return Result.success(systemRoleService.distributeAuth(permissionForm));
    }
}
