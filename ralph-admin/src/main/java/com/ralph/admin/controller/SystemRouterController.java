package com.ralph.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemRouter;
import com.ralph.admin.service.SystemRouterService;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.common.core.annotation.IgnoreUrl;
import com.ralph.common.core.entity.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @auther ralph
 * @create 2023-01-10
 * @describe 动态路由前端控制器
 */
@RequiredArgsConstructor
@RestController
@Api(value = "system_router", tags = "动态路由")
@RequestMapping("router")
public class SystemRouterController {

    private final SystemRouterService systemRouterService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemRouter 动态路由
     * @return Result
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Result<Page<SystemRouter>> getSystemRouterPage(@ApiNeedField({"size","current"}) Page<SystemRouter> page, SystemRouter systemRouter) {
        return Result.success(systemRouterService.page(page, Wrappers.query(systemRouter)));
    }

    /**
     * 通过主键查询动态路由
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/get/id")
    public Result<SystemRouter> getById(@RequestParam("id") Integer id) {
        return Result.success(systemRouterService.getById(id));
    }

    /**
     * 新增动态路由
     * @param systemRouter 动态路由
     * @return Result
     */
    @ActionLog("新增动态路由")
    @ApiOperation(value = "新增动态路由", notes = "新增动态路由")
    @PostMapping("/insert")
    public Result<Boolean> save(@RequestBody SystemRouter systemRouter) {
        boolean res = systemRouterService.save(systemRouter);
        systemRouterService.getRouteList();
        return Result.success(res);
    }
    /**
     * 修改动态路由
     * @param systemRouter 动态路由
     * @return Result
     */
    @ActionLog("修改动态路由")
    @ApiOperation(value = "修改动态路由", notes = "修改动态路由")
    @PutMapping("/update")
    public Result<Boolean> updateById(@RequestBody SystemRouter systemRouter) {
        boolean res = systemRouterService.updateById(systemRouter);
        systemRouterService.getRouteList();
        return Result.success(res);
    }
    /**
     * 通过主键删除动态路由
     * @param id 主键
     * @return Result
     */
    @IgnoreUrl
    @ActionLog("删除动态路由")
    @ApiOperation(value = "通过主键删除动态路由", notes = "通过主键删除动态路由")
    @DeleteMapping("/delete/id")
    public Result<Boolean> removeById(@RequestParam("id") Integer id) {
        boolean res = systemRouterService.removeById(id);
        systemRouterService.getRouteList();
        return Result.success(res);
    }

    /**
     * 更新动态路由
     * @return Result
     */
    @ApiOperation(value = "更新动态路由", notes = "更新动态路由")
    @GetMapping("/replace")
    public Result<Boolean> replaceRoute() {
        systemRouterService.getRouteList();
        return Result.success(Boolean.TRUE);
    }
}
