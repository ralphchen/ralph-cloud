package com.ralph.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.service.SystemLogService;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.entity.SystemLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @auther ralph
 * @create 2022-12-16
 * @describe 操作日志前端控制器
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("sys_log")
@Api(value = "sys_log", tags = "操作日志")
public class SystemLogController {

    private final SystemLogService systemLogService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemLog 操作日志
     * @return Result
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Result<Page<SystemLog>> getSystemLogPage(@ApiNeedField({"size","current"}) Page<SystemLog> page, SystemLog systemLog) {
        return Result.success(systemLogService.page(page, Wrappers.query(systemLog)
                .lambda().orderByDesc(SystemLog::getCreateDate)));
    }

    /**
     * 通过主键查询操作日志
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/get/id")
    public Result<SystemLog> getById(@RequestParam("id") Integer id) {
        return Result.success(systemLogService.getById(id));
    }

    /**
     * 新增操作日志
     * @param systemLog 操作日志
     * @return Result
     */
    @PostMapping("/insert")
    public Result<Boolean> save(@RequestBody SystemLog systemLog) {
        return Result.success(systemLogService.save(systemLog));
    }
}
