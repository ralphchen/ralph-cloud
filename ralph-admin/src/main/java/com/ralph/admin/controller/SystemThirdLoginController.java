package com.ralph.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemThirdLogin;
import com.ralph.admin.service.SystemThirdLoginService;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.common.core.entity.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @auther ralph
 * @create 2023-01-13
 * @describe 第三方登录信息前端控制器
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("third_login")
@Api(value = "system_third_login", tags = "第三方登录信息")
public class SystemThirdLoginController {

    private final SystemThirdLoginService systemThirdLoginService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemThirdLogin 第三方登录信息
     * @return Result
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Result<Page<SystemThirdLogin>> getSystemThirdLoginPage(@ApiNeedField({"size","current"}) Page<SystemThirdLogin> page, SystemThirdLogin systemThirdLogin) {
        return Result.success(systemThirdLoginService.page(page, Wrappers.query(systemThirdLogin)));
    }

    /**
     * 通过主键查询第三方登录信息
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/get/id")
    public Result<SystemThirdLogin> getById(@RequestParam("id") Integer id) {
        return Result.success(systemThirdLoginService.getById(id));
    }

    /**
     * 新增第三方登录信息
     * @param systemThirdLogin 第三方登录信息
     * @return Result
     */
    @ActionLog("新增第三方登录信息")
    @ApiOperation(value = "新增第三方登录信息", notes = "新增第三方登录信息")
    @PostMapping("/insert")
    public Result<Boolean> save(@RequestBody SystemThirdLogin systemThirdLogin) {
        return Result.success(systemThirdLoginService.save(systemThirdLogin));
    }
    /**
     * 修改第三方登录信息
     * @param systemThirdLogin 第三方登录信息
     * @return Result
     */
    @ActionLog("修改第三方登录信息")
    @ApiOperation(value = "修改第三方登录信息", notes = "修改第三方登录信息")
    @PutMapping("/update")
    public Result<Boolean> updateById(@RequestBody  SystemThirdLogin systemThirdLogin) {
        return Result.success(systemThirdLoginService.updateById(systemThirdLogin));
    }

    /**
     * 通过主键删除第三方登录信息
     * @param id 主键
     * @return Result
     */
    @ActionLog("删除第三方登录信息")
    @ApiOperation(value = "通过主键删除第三方登录信息", notes = "通过主键删除第三方登录信息")
    @DeleteMapping("/delete/id")
    public Result<Boolean> removeById(@RequestParam("id") Integer id) {
        return Result.success(systemThirdLoginService.removeById(id));
    }

    /**
     * 绑定第三方信息
     * @param thirdInfo 第三方信息
     */
    @ActionLog("绑定第三方信息")
    @ApiOperation(value = "绑定第三方信息", notes = "绑定第三方信息")
    @GetMapping("/bind")
    public Result<Boolean> bindThirdInfo(String thirdInfo){
        return Result.success(systemThirdLoginService.bindThirdInfo(thirdInfo));
    }

    /**
     * 获取当前登录人第三方信息
     */
    @ApiOperation(value = "获取当前登录人第三方信息", notes = "获取当前登录人第三方信息")
    @GetMapping("/current/info")
    public Result<List<SystemThirdLogin>> currentInfo(){
        return Result.success(systemThirdLoginService.currentInfo());
    }
}
