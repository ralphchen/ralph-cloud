package com.ralph.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemDict;
import com.ralph.admin.entity.SystemDictItem;
import com.ralph.admin.service.SystemDictService;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.core.annotation.IgnoreUrl;
import com.ralph.common.core.annotation.NotResHandler;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.entity.Groups;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.docs.annotation.ApiIgnoreField;
import com.ralph.common.docs.annotation.ApiNeedField;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @auther ralph
 * @create 2022-12-19
 * @describe 字典表前端控制器
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("dict")
@Api(value = "dict", tags = "字典表")
public class SystemDictController {

    private final SystemDictService systemDictService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemDict 字典表
     * @return Result
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Page<SystemDict> getSystemDictPage(@ApiNeedField({"size","current"}) Page<SystemDict> page,
                                              @ApiIgnoreField({"updateDate","updateBy"}) SystemDict systemDict) {
        return systemDictService.page(page, Wrappers.query(systemDict).lambda()
                .like(SystemDict::getType,systemDict.getType())
                .orderByDesc(SystemDict::getCreateDate));
    }

    /**
     * 通过主键查询字典表
     * @param id 主键
     * @return Result
     */
    @IgnoreUrl
    @NotResHandler
    @GetMapping("/get/id")
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    public SystemDict getById(@RequestParam("id") Integer id) {
        return systemDictService.getById(id);
    }

    /**
     * 新增字典表
     * @param systemDict 字典表
     * @return Result
     */
    @ActionLog("新增字典表")
    @ApiOperation(value = "新增字典表", notes = "新增字典表")
    @PostMapping("/insert")
    public Result<Boolean> save(@RequestBody @Validated(Groups.Create.class) SystemDict systemDict) {
        return Result.success(systemDictService.saveOrUpdateDict(systemDict));
    }

    /**
     * 修改字典表
     * @param systemDict 字典表
     * @return Result
     */
    @ActionLog("修改字典表")
    @ApiOperation(value = "修改字典表", notes = "修改字典表")
    @PutMapping("/update")
    public Result<Boolean> updateById(@RequestBody @Validated(Groups.Update.class) SystemDict systemDict) {
        if (null == systemDict.getId()){
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        return Result.success(systemDictService.saveOrUpdateDict(systemDict));
    }

    /**
     * 通过主键删除字典表
     * @param id 主键
     * @param allDelete 是否删除该字典下字典项
     * @return Result
     */
    @ActionLog("删除字典表")
    @ApiOperation(value = "通过主键删除字典表", notes = "通过主键删除字典表")
    @DeleteMapping("/delete/id" )
    public Result<Boolean> removeById(@RequestParam("id") Integer id,
        @RequestParam(name = "allDelete", required = false, defaultValue = "false") boolean allDelete) {
        return Result.success(systemDictService.removeDictById(id, allDelete));
    }

    //====================== 字典项相关接口 ======================//
    /**
     * 获取字典项分页列表
     * @param page 分页对象
     * @param dictId 字典id
     * @return Result
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/item/page")
    public Result<Page<SystemDictItem>> getSystemDictItemPage(@ApiNeedField({"size","current"})  Page<SystemDictItem> page, Integer dictId) {
        return Result.success(systemDictService.getItemPage(page, dictId));
    }

    /**
     * 通过主键查询字典项
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/item/get/id" )
    public Result<SystemDictItem> getItemById(@RequestParam("id" ) Integer id) {
        return Result.success(systemDictService.getItemById(id));
    }

    /**
     * 新增字典项
     * @param systemDictItem 字典项
     * @return Result
     */
    @ActionLog("新增字典项")
    @ApiOperation(value = "新增字典项", notes = "新增字典项")
    @PostMapping("/item/insert")
    public Result<Boolean> saveItem(@RequestBody @Validated(Groups.Create.class) SystemDictItem systemDictItem) {
        return Result.success(systemDictService.saveOrUpdateItem(systemDictItem));
    }

    /**
     * 修改字典项
     * @param systemDictItem 字典项
     * @return Result
     */
    @ActionLog("修改字典项")
    @ApiOperation(value = "修改字典项", notes = "修改字典项")
    @PutMapping("/item/update")
    public Result<Boolean> updateItemById(@RequestBody @Validated(Groups.Update.class) SystemDictItem systemDictItem) {
        if (null == systemDictItem.getId()){
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        return Result.success(systemDictService.saveOrUpdateItem(systemDictItem));
    }

    /**
     * 通过主键删除字典项
     * @param id 主键
     * @return Result
     */
    @ActionLog("删除字典项")
    @ApiOperation(value = "通过主键删除字典项", notes = "通过主键删除字典项")
    @DeleteMapping("/item/delete/id")
    public Result<Boolean> removeItemById(@RequestParam("id") Integer id) {
        return Result.success(systemDictService.removeItemById(id));
    }

    /**
     * 通过type获取所有字典项
     * @param type 字典类型
     * @return Result
     */
    @ApiOperation(value = "通过type获取所有字典项", notes = "通过type获取所有字典项")
    @GetMapping("/item/list/type")
    @Cacheable(value = RedisKeyConstant.ADMIN_DICT_ITEM, key = "#type", unless = "#result.data.isEmpty()")
    public Result<List<SystemDictItem>> getDictItemByType(@RequestParam("type") String type) {
        return Result.success(systemDictService.getDictItemByType(type));
    }
}
