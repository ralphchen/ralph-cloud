package com.ralph.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemMenu;
import com.ralph.admin.service.SystemMenuService;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.MenuTree;
import com.ralph.common.core.entity.Result;
import com.ralph.common.core.exceptions.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @auther ralph
 * @create 2023-01-04
 * @describe 菜单表前端控制器
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("menu")
@Api(value = "menu", tags = "菜单表")
public class SystemMenuController {

    private final SystemMenuService systemMenuService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemMenu 菜单表
     * @return Result
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public Result<Page<SystemMenu>> getSystemMenuPage(@ApiNeedField({"size","current"}) Page<SystemMenu> page, SystemMenu systemMenu) {
        return Result.success(systemMenuService.page(page, Wrappers.query(systemMenu)));
    }

    /**
     * 通过主键查询菜单表
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/get/id")
    public Result<SystemMenu> getById(@RequestParam("id") Integer id) {
        return Result.success(systemMenuService.getById(id));
    }

    /**
     * 新增菜单表
     * @param systemMenu 菜单表
     * @return Result
     */
    @ActionLog("新增菜单表")
    @ApiOperation(value = "新增菜单表", notes = "新增菜单表")
    @PostMapping("/insert")
    public Result<Boolean> save(@RequestBody SystemMenu systemMenu) {
        return Result.success(systemMenuService.save(systemMenu));
    }

    /**
     * 修改菜单表
     * @param systemMenu 菜单表
     * @return Result
     */
    @ActionLog("修改菜单表")
    @ApiOperation(value = "修改菜单表", notes = "修改菜单表")
    @PutMapping("/update")
    public Result<Boolean> updateById(@RequestBody SystemMenu systemMenu) {
        if (null == systemMenu.getId()){
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        return Result.success(systemMenuService.updateById(systemMenu));
    }

    /**
     * 通过主键删除菜单表
     * @param id 主键
     * @return Result
     */
    @ActionLog("删除菜单表")
    @ApiOperation(value = "通过主键删除菜单表", notes = "通过主键删除菜单表")
    @DeleteMapping("/delete/id")
    public Result<Boolean> removeById(@RequestParam("id") Integer id) {
        return Result.success(systemMenuService.removeById(id));
    }

    /**
     * 获取菜单树
     * @return Result
     */
    @ApiOperation(value = "获取菜单树", notes = "获取菜单树")
    @GetMapping("/tree")
    public Result<MenuTree> menuTree() {
        return Result.success(systemMenuService.menuTree());
    }
}
