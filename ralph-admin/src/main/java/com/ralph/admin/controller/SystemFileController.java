package com.ralph.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemFile;
import com.ralph.admin.service.SystemFileService;
import com.ralph.common.core.annotation.ActionLog;
import com.ralph.common.docs.annotation.ApiNeedField;
import com.ralph.common.core.annotation.IgnoreUrl;
import com.ralph.common.core.entity.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @auther ralph
 * @create 2023-01-06
 * @describe 文件管理前端控制器
 */
@RequiredArgsConstructor
@RestController
@Api(value = "system_file", tags = "文件管理")
@RequestMapping("file")
public class SystemFileController {

    private final SystemFileService systemFileService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemFile 文件管理
     * @return Result
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    public Result<Page<SystemFile>> getSystemFilePage(@ApiNeedField({"size","current"}) Page<SystemFile> page, SystemFile systemFile) {
        return Result.success(systemFileService.pageList(page, systemFile));
    }

    /**
     * 通过主键查询文件管理
     * @param id 主键
     * @return Result
     */
    @ApiOperation(value = "通过主键查询", notes = "通过主键查询")
    @GetMapping("/get/id")
    public Result<SystemFile> getById(@RequestParam("id") Integer id) {
        return Result.success(systemFileService.getById(id));
    }

    /**
     * 新增文件管理
     * @param file 文件
     * @param bucketName 桶名称
     * @param filePath 文件路径
     * @return Result
     */
    @ActionLog("新增文件管理")
    @ApiOperation(value = "新增文件管理", notes = "新增文件管理")
    @PostMapping("/upload")
    public Result<SystemFile> upload(@RequestParam("file") MultipartFile file,
                                @RequestParam(name = "bucketName",required = false) String bucketName,
                                @RequestParam(name = "filePath",required = false) String filePath) {
        return Result.success(systemFileService.upload(file, bucketName, filePath));
    }

    /**
     * 根据文件路径下载文件
     * @param bucketName 桶名称
     * @param fileName 文件路径级名称
     * @param response response
     */
    @IgnoreUrl
    @ApiOperation(value = "根据文件路径下载文件", notes = "根据文件路径下载文件")
    @GetMapping("download")
    public void download(@RequestParam(name = "bucketName",required = false) String bucketName,
                                    @RequestParam(name = "fileName") String fileName,
                                    HttpServletResponse response) {
        systemFileService.download(bucketName, fileName, response);
    }

    /**
     * 根据id下载文件
     * @param id 文件id
     * @param response response
     */
    @IgnoreUrl
    @ApiOperation(value = "根据id下载文件", notes = "根据id下载文件")
    @GetMapping("download/fileId")
    public void download(@RequestParam(name = "id") Integer id,
                                    HttpServletResponse response) {
        systemFileService.download(id, response);
    }

    /**
     * 通过主键删除文件管理
     * @param id 主键
     * @return Result
     */
    @ActionLog("删除文件管理")
    @ApiOperation(value = "通过主键删除文件管理", notes = "通过主键删除文件管理")
    @DeleteMapping("/delete/id" )
    public Result<Boolean> removeById(@RequestParam("id") Integer id) {
        return Result.success(systemFileService.deleteFile(id));
    }
}
