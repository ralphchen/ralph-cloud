package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemFile;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 文件管理 服务类
 * </p>
 *
 * @author ralph
 * @since 2023-01-06
 */
public interface SystemFileService extends IService<SystemFile> {

    /**
     * 分页查询
     * @param page 分页对象
     * @param systemFile 文件管理
     * @return Result
     */
    Page<SystemFile> pageList(Page<SystemFile> page, SystemFile systemFile);

    /**
     * 新增文件管理
     * @param file 文件
     * @param bucketName 桶名称
     * @param filePath 文件路径
     * @return Result
     */
    SystemFile upload(MultipartFile file, String bucketName, String filePath);

    /**
     * 下载文件
     * @param bucketName 桶名称
     * @param fileName 文件路径
     * @param response response
     */
    void download(String  bucketName,String fileName, HttpServletResponse response);

    /**
     * 根据id下载文件
     * @param id 文件id
     * @param response response
     */
    void download(Integer id, HttpServletResponse response);

    /**
     * 通过主键删除文件管理
     * @param id 主键
     * @return Result
     */
    Boolean deleteFile(Integer id);
}

