package com.ralph.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.common.core.entity.SystemLog;
import com.ralph.admin.mapper.SystemLogMapper;
import com.ralph.admin.service.SystemLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2022-12-16
 */
@Service
public class SystemLogServiceImpl extends ServiceImpl<SystemLogMapper, SystemLog> implements SystemLogService {

}

