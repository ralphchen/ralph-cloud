package com.ralph.admin.service.impl;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.SystemRouter;
import com.ralph.admin.mapper.SystemRouterMapper;
import com.ralph.admin.service.SystemRouterService;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.utils.RedisUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 动态路由 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2023-01-10
 */
@Service
public class SystemRouterServiceImpl extends ServiceImpl<SystemRouterMapper, SystemRouter> implements SystemRouterService {

    /**
     * 获取动态路由列表
     *
     * @return 动态路由列表
     */
    @Override
    public List<SystemRouter> getRouteList() {
        List<SystemRouter> list = super.list();
        /*
        RedisUtils.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(JSONObject.class));
        for (SystemRouter systemRouter : list) {
            JSONObject entries = JSONUtil.parseObj(systemRouter);
            RedisUtils.hset(RedisKeyConstant.SYSTEM_ROUTE,systemRouter.getRouteId(),entries);
        }
        RedisUtils.setHashValueSerializer(RedisTemplateConfig.hashValueSerializer);
        */
        List<JSONObject> jsonList = list.stream().map(JSONUtil::parseObj).collect(Collectors.toList());
        // TODO 通过消息队列传输
        RedisUtils.convertAndSend(RedisKeyConstant.SYS_ROUTE_RES_TOPIC,jsonList.toString());
        return list;
    }
}

