package com.ralph.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.SystemDict;
import com.ralph.admin.entity.SystemDictItem;
import com.ralph.admin.mapper.SystemDictItemMapper;
import com.ralph.admin.mapper.SystemDictMapper;
import com.ralph.admin.service.SystemDictService;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.core.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
@Service
@RequiredArgsConstructor
public class SystemDictServiceImpl extends ServiceImpl<SystemDictMapper, SystemDict> implements SystemDictService {

    private final SystemDictItemMapper systemDictItemMapper;

    /**
     * 获取字典项分页列表
     * @param page 分页对象
     * @param dictId 字典id
     * @return Result
     */
    @Override
    public Page<SystemDictItem> getItemPage(Page<SystemDictItem> page, Integer dictId) {
        if (null == dictId){
            log.error("字典id为空");
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        return systemDictItemMapper.selectPage(page,Wrappers.<SystemDictItem>lambdaQuery()
                .eq(SystemDictItem::getDictId,dictId)
                .orderByDesc(SystemDictItem::getSort)
                .orderByDesc(SystemDictItem::getCreateDate));
    }

    /**
     * 新增字典表
     *
     * @param systemDict 字典表
     * @return Result
     */
    @Override
    public Boolean saveOrUpdateDict(SystemDict systemDict) {
        // 唯一性校验
        dictJudgeUnique(systemDict);
        if (null != systemDict.getId()){
            deleteRedisKey(null, systemDict.getId());
        }
        return super.saveOrUpdate(systemDict);
    }

    private void dictJudgeUnique(SystemDict systemDict){
        if (StringUtils.isBlank(systemDict.getType())){
            log.error("字典类型为空");
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        List<SystemDict> dictList = super.list(Wrappers.<SystemDict>lambdaQuery()
                .eq(SystemDict::getType, systemDict.getType()));
        for (SystemDict dict : dictList) {
            if (!dict.getId().equals(systemDict.getId())){
                log.error("字典类型重复");
                throw new BusinessException(ErrorEnum.DATA_REPEAT);
            }
        }
    }

    /**
     * 通过主键删除字典表
     *
     * @param id 主键
     * @param allDelete 是否删除该字典下字典项
     * @return Result
     */
    @Override
    public Boolean removeDictById(Integer id, boolean allDelete) {
        if (!allDelete){
            List<SystemDictItem> dictItemList = systemDictItemMapper.selectList(Wrappers.<SystemDictItem>lambdaQuery()
                    .eq(SystemDictItem::getDictId, id));
            if (CollUtil.isNotEmpty(dictItemList)){
                throw new BusinessException(ErrorEnum.DICT_HAS_ITEM);
            }
        }
        // 删除关联字典项
        systemDictItemMapper.delete(Wrappers.<SystemDictItem>lambdaQuery()
                .eq(SystemDictItem::getDictId,id));
        deleteRedisKey(null, id);
        // 删除字典
        return super.removeById(id);
    }

    /**
     * 通过主键查询字典项
     *
     * @param id 主键
     * @return Result
     */
    @Override
    public SystemDictItem getItemById(Integer id) {
        return systemDictItemMapper.selectById(id);
    }

    /**
     * 新增字典项
     *
     * @param systemDictItem 字典项
     * @return Result
     */
    @Override
    public Boolean saveOrUpdateItem(SystemDictItem systemDictItem) {
        // 唯一性校验
        itemJudgeUnique(systemDictItem);
        if (null == systemDictItem.getId()){
            systemDictItemMapper.insert(systemDictItem);
        }else {
            systemDictItemMapper.updateById(systemDictItem);
        }
        deleteRedisKey(null,systemDictItem.getDictId());
        return Boolean.TRUE;
    }

    private void itemJudgeUnique(SystemDictItem systemDictItem) {
        if (null == systemDictItem.getDictId()){
            log.error("字典id为空");
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        List<SystemDictItem> itemList = systemDictItemMapper.selectList(Wrappers.<SystemDictItem>lambdaQuery()
                .eq(SystemDictItem::getDictId, systemDictItem.getDictId()));
        for (SystemDictItem dictItem : itemList) {
            if (!dictItem.getId().equals(systemDictItem.getDictId())){
                log.error("同一字典下字典项不唯一");
                throw new BusinessException(ErrorEnum.DATA_REPEAT);
            }
        }
    }

    /**
     * 通过主键删除字典项
     *
     * @param id 主键
     * @return Result
     */
    @Override
    public Boolean removeItemById(Integer id) {
        systemDictItemMapper.deleteById(id);
        // 删除缓存
        deleteRedisKey(id,null);
        return Boolean.TRUE;
    }

    /**
     * 根据字典项id或字典id删除缓存
     * @param itemId 字典项id
     * @param dictId 字典id
     */
    private void deleteRedisKey(Integer itemId, Integer dictId){
        if (null == itemId && null == dictId){
            log.warn("删除字典缓存时id为空");
            return;
        }
        String dictType = systemDictItemMapper.getTypeByItemId(itemId, dictId);
        RedisUtils.del(RedisKeyConstant.ADMIN_DICT_ITEM + ":" + dictType);
    }

    /**
     * 根据字典类型获取字典项
     *
     * @param type 字典类型
     * @return 字典项列表
     */
    @Override
    public List<SystemDictItem> getDictItemByType(String type) {
        if (StringUtils.isBlank(type)){
            return new ArrayList<>();
        }
        return systemDictItemMapper.getDictItemByType(type);
    }
}

