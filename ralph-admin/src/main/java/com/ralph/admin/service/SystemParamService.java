package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemParam;

/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
public interface SystemParamService extends IService<SystemParam> {

    /**
     * 更新或新增
     * @param entity 信息
     * @return result
     */
    @Override
    boolean saveOrUpdate(SystemParam entity);

    /**
     * 通过key查询参数配置表
     * @param key 主键
     * @return Result
     */
    SystemParam getByKey(String key);
}

