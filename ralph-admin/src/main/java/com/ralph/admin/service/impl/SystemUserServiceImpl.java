package com.ralph.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.*;
import com.ralph.admin.entity.form.PermissionForm;
import com.ralph.admin.mapper.SystemUserMapper;
import com.ralph.admin.service.*;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.MenuTree;
import com.ralph.common.core.entity.UserDTO;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.core.utils.EncryptUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2022-12-20
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SystemUserServiceImpl extends ServiceImpl<SystemUserMapper, SystemUser> implements SystemUserService {

    private final SystemRoleService systemRoleService;

    private final SystemMenuService systemMenuService;

    private final SystemUserRoleRefService systemUserRoleRefService;

    private final SystemThirdLoginService systemThirdLoginService;

    /**
     * 新增或更新用户
     *
     * @param systemUser 用户信息
     * @return 更新结果
     */
    @Override
    public Boolean saveOrUpdateUser(SystemUser systemUser) {
        judgeUnique(systemUser.getId(), systemUser.getUsername(), systemUser.getPhone());
        // 密码进行md5加密
        if (StringUtils.isNotBlank(systemUser.getPassword())){
            systemUser.setPassword(MD5.create().digestHex(systemUser.getPassword()));
            if (null != systemUser.getId()){
                systemUser.setPwdUpdateTime(new Date());
            }
        } else {
            systemUser.setPassword(null);
        }
        return super.saveOrUpdate(systemUser);
    }

    private void judgeUnique(Integer id, String username, String phone){
        if (StringUtils.isBlank(username) && StringUtils.isBlank(phone)){
            return;
        }
        List<SystemUser> userList = super.list(Wrappers.<SystemUser>lambdaQuery()
                .eq(StringUtils.isNotBlank(username), SystemUser::getUsername, username)
                .or()
                .eq(StringUtils.isNotBlank(phone), SystemUser::getPhone, phone));
        for (SystemUser user : userList) {
            if (!user.getId().equals(id)){
                if (user.getUsername().equals(username)){
                    log.warn("用户名重复");
                }
                if (user.getPhone().equals(phone)){
                    log.warn("手机号重复");
                }
                throw new BusinessException(ErrorEnum.DATA_REPEAT);
            }
        }
    }

    /**
     * 根据用户名查询用户信息
     *
     * @param username 用户名
     * @return Result
     */
    @Override
    public UserDTO getUserByUsername(String username) {
        SystemUser user = super.getOne(Wrappers.<SystemUser>lambdaQuery()
                .eq(SystemUser::getUsername, username)
                .last("limit 1"));
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user,userDTO);
        // 封装角色信息
        List<SystemRole> roleList = systemRoleService.getRoleListByUser(username);
        Set<String> roleCodeSet = roleList.stream().map(SystemRole::getRoleCode)
                .filter(StringUtils::isNotBlank).collect(Collectors.toSet());
        userDTO.setRoles(roleCodeSet);
        // 封装菜单信息
        List<SystemMenu> menuList = systemMenuService.getMenuListByUsername(username);
        Set<String> permissionSet = menuList.stream().map(SystemMenu::getPermission)
                .filter(StringUtils::isNotBlank).collect(Collectors.toSet());
        userDTO.setPermissions(permissionSet);
        // 构建菜单树
        MenuTree menuTree = systemMenuService.menuListToTree(menuList);
        userDTO.setMenuTree(menuTree);
        return userDTO;
    }

    /**
     * 给用户分配角色
     *
     * @param permissionForm 权限信息
     * @return Result
     */
    @Override
    public Boolean distributeAuth(PermissionForm permissionForm) {
        SystemUser user = super.getById(permissionForm.getId());
        if (null == user){
            throw new BusinessException(ErrorEnum.DATA_NOT_EXISTS);
        }
        // 删除用户角色关联表中所有该用户相关信息
        systemUserRoleRefService.remove(Wrappers.<SystemUserRoleRef>lambdaQuery()
                .eq(SystemUserRoleRef::getUserId, permissionForm.getId()));
        // 添加新有角色信息
        if (CollUtil.isEmpty(permissionForm.getPermissionIdList())){
            return Boolean.TRUE;
        }
        List<SystemUserRoleRef> userRoleList = permissionForm.getPermissionIdList().stream().map(item -> {
            SystemUserRoleRef systemUserRoleRef = new SystemUserRoleRef();
            systemUserRoleRef.setUserId(permissionForm.getId());
            systemUserRoleRef.setRoleId(item);
            return systemUserRoleRef;
        }).collect(Collectors.toList());
        return systemUserRoleRefService.saveBatch(userRoleList);
    }

    /**
     * 根据第三方登录信息获取用户信息
     * @param thirdInfo 第三方登录信息（加密）
     * @return 用户信息
     */
    public UserDTO getUserInfoByThirdKey(String thirdInfo){
        JSONObject thirdJson = EncryptUtils.decryptDataJson(thirdInfo);
        if (StringUtils.isBlank(thirdJson.getStr("type")) || StringUtils.isBlank(thirdJson.getStr("thirdId"))){
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        UserDTO userDTO = new UserDTO();
        List<SystemThirdLogin> thirdLoginList = systemThirdLoginService.list(Wrappers.<SystemThirdLogin>lambdaQuery()
                .eq(SystemThirdLogin::getType, thirdJson.getStr("type"))
                .eq(SystemThirdLogin::getThirdId, thirdJson.getStr("thirdId")));
        if (CollUtil.isEmpty(thirdLoginList)){
            return userDTO;
        }
        SystemUser user = super.getById(thirdLoginList.get(0).getUserId());
        BeanUtils.copyProperties(user, userDTO);
        userDTO.setPassword(null);
        return userDTO;
    }

    /**
     * 根据手机号获取用户信息
     *
     * @param phone 手机号
     * @return 用户信息
     */
    @Override
    public UserDTO getUserInfoByPhone(String phone) {
        UserDTO resUser = new UserDTO();
        if (StringUtils.isBlank(phone)){
            return resUser;
        }
        SystemUser user = super.getOne(Wrappers.<SystemUser>lambdaQuery().eq(SystemUser::getPhone, phone));
        if (null == user){
            return resUser;
        }
        BeanUtils.copyProperties(user, resUser);
        resUser.setPassword(null);
        return resUser;
    }
}

