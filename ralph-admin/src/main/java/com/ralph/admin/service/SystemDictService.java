package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemDict;
import com.ralph.admin.entity.SystemDictItem;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
public interface SystemDictService extends IService<SystemDict> {

    /**
     * 获取字典项分页列表
     * @param page 分页对象
     * @param dictId 字典id
     * @return Result
     */
    Page<SystemDictItem> getItemPage(Page<SystemDictItem> page, Integer dictId);

    /**
     * 新增字典表
     * @param systemDict 字典表
     * @return Result
     */
    Boolean saveOrUpdateDict(SystemDict systemDict);

    /**
     * 通过主键删除字典表
     * @param id 主键
     * @param allDelete 是否删除该字典下字典项
     * @return Result
     */
    Boolean removeDictById(Integer id, boolean allDelete);

    /**
     * 通过主键查询字典项
     * @param id 主键
     * @return Result
     */
    SystemDictItem getItemById(Integer id);

    /**
     * 新增字典项
     * @param systemDictItem 字典项
     * @return Result
     */
    Boolean saveOrUpdateItem(SystemDictItem systemDictItem);

    /**
     * 通过主键删除字典项
     * @param id 主键
     * @return Result
     */
    Boolean removeItemById(Integer id);

    /**
     * 根据字典类型获取字典项
     * @param type 字典类型
     * @return 字典项列表
     */
    List<SystemDictItem> getDictItemByType(String type);
}

