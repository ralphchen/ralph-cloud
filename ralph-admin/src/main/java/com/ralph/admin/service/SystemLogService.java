package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.common.core.entity.SystemLog;

/**
 * <p>
 * 操作日志 服务类
 * </p>
 *
 * @author ralph
 * @since 2022-12-16
 */
public interface SystemLogService extends IService<SystemLog> {

}

