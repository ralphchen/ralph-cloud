package com.ralph.admin.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.SystemParam;
import com.ralph.admin.mapper.SystemParamMapper;
import com.ralph.admin.service.SystemParamService;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.core.utils.RedisUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
@Service
public class SystemParamServiceImpl extends ServiceImpl<SystemParamMapper, SystemParam> implements SystemParamService {

    @Override
    public boolean saveOrUpdate(SystemParam entity){
        // 唯一性校验
        List<SystemParam> paramList = super.list(Wrappers.<SystemParam>lambdaQuery()
                .eq(SystemParam::getParamKey, entity.getParamKey()));
        for (SystemParam param : paramList) {
            if (!param.getId().equals(entity.getId())){
                log.error("参数配置key重复");
                throw new BusinessException(ErrorEnum.DATA_REPEAT);
            }
        }
        if (null != entity.getId()){
            RedisUtils.del(RedisKeyConstant.ADMIN_PARAMS + ":" + entity.getParamKey());
        }
        return super.saveOrUpdate(entity);
    }

    /**
     * 通过key查询参数配置表
     *
     * @param key 主键
     * @return Result
     */
    @Override
    public SystemParam getByKey(String key) {
        return super.getOne(Wrappers.<SystemParam>lambdaQuery()
                .eq(SystemParam::getParamKey,key)
                .last("limit 1"));
    }
}

