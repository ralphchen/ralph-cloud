package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemToken;

public interface SystemTokenService {

    /**
     * token分页
     * @param page 分页信息
     * @param systemToken token信息
     * @return token分页结果
     */
    Page tokenPage(Page page, SystemToken systemToken);

    /**
     * 删除token
     * @param username 用户名
     * @return token分页结果
     */
    Boolean delete(String username);
}
