package com.ralph.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.SystemMenuRoleRef;
import com.ralph.admin.entity.SystemRole;
import com.ralph.admin.entity.form.PermissionForm;
import com.ralph.admin.mapper.SystemRoleMapper;
import com.ralph.admin.service.SystemMenuRoleRefService;
import com.ralph.admin.service.SystemRoleService;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.exceptions.BusinessException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 系统角色 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2022-12-29
 */
@Service
@RequiredArgsConstructor
public class SystemRoleServiceImpl extends ServiceImpl<SystemRoleMapper, SystemRole> implements SystemRoleService {

    private final SystemMenuRoleRefService systemMenuRoleRefService;

    /**
     * 根据用户名查询角色列表
     *
     * @param username 用户名
     * @return Result
     */
    @Override
    public List<SystemRole> getRoleListByUser(String username) {
        if (StringUtils.isBlank(username)){
            throw new BusinessException(ErrorEnum.REQUIRE_PARAM_EMPTY);
        }
        return baseMapper.selectRoleByUsername(username);
    }

    /**
     * 给角色分配菜单
     *
     * @param permissionForm 权限信息
     * @return Result
     */
    @Override
    public Boolean distributeAuth(PermissionForm permissionForm) {
        SystemRole role = super.getById(permissionForm.getId());
        if (null == role){
            throw new BusinessException(ErrorEnum.DATA_NOT_EXISTS);
        }
        // 删除角色菜单关联表中所有该用户相关信息
        systemMenuRoleRefService.remove(Wrappers.<SystemMenuRoleRef>lambdaQuery()
                .eq(SystemMenuRoleRef::getRoleId, permissionForm.getId()));
        // 添加新的菜单信息
        if (CollUtil.isEmpty(permissionForm.getPermissionIdList())){
            return Boolean.TRUE;
        }
        List<SystemMenuRoleRef> userRoleList = permissionForm.getPermissionIdList().stream().map(item -> {
            SystemMenuRoleRef systemMenuRoleRef = new SystemMenuRoleRef();
            systemMenuRoleRef.setRoleId(permissionForm.getId());
            systemMenuRoleRef.setRoleId(item);
            return systemMenuRoleRef;
        }).collect(Collectors.toList());
        return systemMenuRoleRefService.saveBatch(userRoleList);
    }
}

