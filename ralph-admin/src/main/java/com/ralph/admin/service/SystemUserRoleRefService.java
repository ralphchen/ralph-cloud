package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemUserRoleRef;

/**
 * <p>
 * 角色用户关联表 服务类
 * </p>
 *
 * @author ralph
 * @since 2022-12-20
 */
public interface SystemUserRoleRefService extends IService<SystemUserRoleRef> {
}
