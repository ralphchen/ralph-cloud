package com.ralph.admin.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.UUID;
import com.amazonaws.services.s3.model.S3Object;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.SystemFile;
import com.ralph.admin.mapper.SystemFileMapper;
import com.ralph.admin.service.SystemFileService;
import com.ralph.admin.utils.OssUtils;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.exceptions.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 文件管理 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2023-01-06
 */
@Service
public class SystemFileServiceImpl extends ServiceImpl<SystemFileMapper, SystemFile> implements SystemFileService {

    @Value("${ralph.oss.bucketName}")
    private String defaultBucketName;

    /**
     * 分页查询
     *
     * @param page       分页对象
     * @param systemFile 文件管理
     * @return Result
     */
    @Override
    public Page<SystemFile> pageList(Page<SystemFile> page, SystemFile systemFile) {
        return super.page(page, Wrappers.<SystemFile>lambdaQuery()
                .like(StringUtils.isNotBlank(systemFile.getFileOriginName()),SystemFile::getFileOriginName,systemFile.getFileOriginName())
                .eq(StringUtils.isNotBlank(systemFile.getFileName()), SystemFile::getFileName, systemFile.getFileName())
                .eq(StringUtils.isNotBlank(systemFile.getFileSuffix()), SystemFile::getFileSuffix, systemFile.getFileSuffix()));
    }

    /**
     * 新增文件管理
     * @param file 文件
     * @param bucketName 桶名称
     * @param filePath 文件路径
     * @return Result
     */
    @Override
    public SystemFile upload(MultipartFile file, String bucketName, String filePath) {
        bucketName = verifyBucketName(bucketName);
        String fileName =  UUID.randomUUID() +"."+ FileUtil.extName(file.getOriginalFilename());
        filePath = StringUtils.isBlank(filePath)? fileName: filePath + "/" + fileName;
        try {
            OssUtils.uploadFile(bucketName, filePath, file.getInputStream());
        }catch (Exception e){
            throw new BusinessException(ErrorEnum.FILE_UPLOAD_ERROR);
        }
        SystemFile systemFile = new SystemFile();
        systemFile.setFileName(fileName);
        systemFile.setFileOriginName(file.getOriginalFilename());
        systemFile.setFileUrl(filePath);
        systemFile.setFileSuffix(FileUtil.extName(file.getOriginalFilename()));
        systemFile.setFileSize(file.getSize());
        systemFile.setBucketName(bucketName);
        super.save(systemFile);
        return systemFile;
    }

    /**
     * 下载文件
     *
     * @param bucketName 桶名称
     * @param fileName   文件路径
     * @param response   response
     */
    @Override
    public void download(String bucketName, String fileName, HttpServletResponse response) {
        bucketName = verifyBucketName(bucketName);
        try {
            S3Object fileObject = OssUtils.getFile(bucketName, fileName);
            IoUtil.copy(fileObject.getObjectContent(), response.getOutputStream());
        }catch (Exception e){
            log.error("文件获取失败，" + e);
            throw new BusinessException(ErrorEnum.FILE_DOWNLOAD_ERROR);
        }
    }

    /**
     * 校验bucketName是否为空，为空则赋予默认bucketName
     * @param bucketName bucketName
     * @return bucketName
     */
    private String verifyBucketName(String bucketName){
        // 如果没有传bucketName，则使用默认bucketName;
        if (StringUtils.isBlank(bucketName)){
            bucketName = defaultBucketName;
        }
        if (!OssUtils.buckedExists(bucketName)){
            throw new BusinessException(ErrorEnum.BUCKET_NOT_EXISTS);
        }
        return bucketName;
    }

    /**
     * 根据id下载文件
     *
     * @param id       文件id
     * @param response response
     */
    @Override
    public void download(Integer id, HttpServletResponse response) {
        SystemFile file = super.getById(id);
        if (null == file){
            throw new BusinessException(ErrorEnum.DATA_NOT_EXISTS);
        }
        download(file.getBucketName(), file.getFileUrl(), response);
    }

    /**
     * 通过主键删除文件管理
     *
     * @param id 主键
     * @return Result
     */
    @Override
    public Boolean deleteFile(Integer id) {
        SystemFile file = super.getById(id);
        if (null == file){
            throw new BusinessException(ErrorEnum.DATA_NOT_EXISTS);
        }
        OssUtils.deleteFile(file.getBucketName(), file.getFileUrl());
        return super.removeById(id);
    }
}

