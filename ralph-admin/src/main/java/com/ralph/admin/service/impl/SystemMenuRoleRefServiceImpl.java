package com.ralph.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.SystemMenuRoleRef;
import com.ralph.admin.mapper.SystemMenuRoleRefMapper;
import com.ralph.admin.service.SystemMenuRoleRefService;
import org.springframework.stereotype.Service;

@Service
public class SystemMenuRoleRefServiceImpl extends ServiceImpl<SystemMenuRoleRefMapper, SystemMenuRoleRef> implements SystemMenuRoleRefService {
}
