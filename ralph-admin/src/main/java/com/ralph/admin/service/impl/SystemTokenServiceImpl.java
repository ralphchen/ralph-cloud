package com.ralph.admin.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ralph.admin.entity.SystemToken;
import com.ralph.admin.service.SystemTokenService;
import com.ralph.common.core.constant.RedisKeyConstant;
import com.ralph.common.core.utils.RedisUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class SystemTokenServiceImpl implements SystemTokenService {

    /**
     * token分页
     * @param page 分页信息
     * @param token token信息
     * @return token分页结果
     */
    @Override
    public Page tokenPage(Page page, SystemToken token){
        // 从redis中获取所有token
        Set keys = RedisUtils.keys(RedisKeyConstant.TOKEN_KEY_PRE + "*");
        List<SystemToken> resList = new ArrayList<>();
        for (Object key : keys) {
            SystemToken temToken = new SystemToken();
            temToken.setUsername(key.toString().substring(RedisKeyConstant.TOKEN_KEY_PRE.length()));
            temToken.setToken(RedisUtils.lGetAllStr(key.toString()));
            temToken.setExpiredTime(RedisUtils.getExpire(key.toString()));
            resList.add(temToken);
        }
        long current = page.getCurrent();
        long size = page.getSize();
        if ((current - 1) * size < resList.size()){
            if (current * size < resList.size()){
                resList = resList.subList((int)((current - 1) * size), (int)(current * size));
            }else {
                resList = resList.subList((int)((current - 1) * size), resList.size());
            }
        }
        page.setRecords(resList);
        page.setTotal(resList.size());
        return page;
    }

    /**
     * 删除token
     *
     * @param username 用户名
     * @return token分页结果
     */
    @Override
    public Boolean delete(String username) {
        RedisUtils.del(RedisKeyConstant.TOKEN_KEY_PRE + username);
        return Boolean.TRUE;
    }
}
