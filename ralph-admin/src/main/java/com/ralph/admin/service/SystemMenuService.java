package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemMenu;
import com.ralph.common.core.entity.MenuTree;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author ralph
 * @since 2023-01-04
 */
public interface SystemMenuService extends IService<SystemMenu> {

    /**
     * 根据用户名获取菜单列表
     * @param username 用户名
     * @return 菜单列表
     */
    List<SystemMenu>  getMenuListByUsername(String username);

    /**
     * 获取菜单树
     * @param parentId 父菜单id
     * @return Result
     */
    MenuTree menuTree();

    /**
     * 菜单列表转树
     * @param menuList 菜单列表
     * @return 树形菜单
     */
    MenuTree menuListToTree(List<SystemMenu> menuList);
}

