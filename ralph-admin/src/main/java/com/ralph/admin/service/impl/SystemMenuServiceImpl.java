package com.ralph.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.SystemMenu;
import com.ralph.admin.mapper.SystemMenuMapper;
import com.ralph.admin.service.SystemMenuService;
import com.ralph.common.core.constant.CommonConstant;
import com.ralph.common.core.entity.MenuTree;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2023-01-04
 */
@Service
public class SystemMenuServiceImpl extends ServiceImpl<SystemMenuMapper, SystemMenu> implements SystemMenuService {

    /**
     * 根据用户名获取菜单列表
     *
     * @param username 用户名
     * @return 菜单列表
     */
    @Override
    public List<SystemMenu> getMenuListByUsername(String username) {
        if (StringUtils.isBlank(username)){
            return new ArrayList<>();
        }
        return baseMapper.selectByUsername(username);
    }

    /**
     * 获取菜单树
     *
     * @param parentId 父菜单id
     * @return Result
     */
    @Override
    public MenuTree menuTree() {
        // 获取所有菜单
        List<SystemMenu> menuList = super.list();
        return menuListToTree(menuList);
    }

    /**
     * 菜单列表转树
     * @param menuList 菜单列表
     * @return 树形菜单
     */
    @Override
    public MenuTree menuListToTree(List<SystemMenu> menuList){
        // 类型转换
        List<MenuTree> menuTreeList = menuList.stream()
                .filter(item->CommonConstant.MENU_TYPE_MENU.equals(item.getType()))
                .map(item->{
            MenuTree temMenu = new MenuTree();
            BeanUtils.copyProperties(item,temMenu);
            return temMenu;
        }).sorted(Comparator.comparing(MenuTree::getSort)).collect(Collectors.toList());
        // 根据父菜单id进行分类
        Map<Integer, List<MenuTree>> menuMap = menuTreeList.stream().collect(Collectors.groupingBy(MenuTree::getParentId));
        // 遍历map
        for (Map.Entry<Integer, List<MenuTree>> entry : menuMap.entrySet()) {
            List<MenuTree> childMenus = entry.getValue();
            for (MenuTree childMenu : childMenus) {
                childMenu.setChildren(menuMap.get(childMenu.getId()));
            }
        }
        MenuTree temMenu = new MenuTree();
        temMenu.setId(CommonConstant.ROOT_MENU_ID);
        temMenu.setMenuName("根菜单");
        temMenu.setMenuCode("ROOT_MENU");
        temMenu.setChildren(menuMap.get(CommonConstant.ROOT_MENU_ID));
        return temMenu;
    }
}

