package com.ralph.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.SystemThirdLogin;
import com.ralph.admin.mapper.SystemThirdLoginMapper;
import com.ralph.admin.service.SystemThirdLoginService;
import com.ralph.common.core.constant.ErrorEnum;
import com.ralph.common.core.entity.UserDTO;
import com.ralph.common.core.exceptions.BusinessException;
import com.ralph.common.core.utils.EncryptUtils;
import com.ralph.common.core.utils.SecurityUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 第三方登录信息 服务实现类
 * </p>
 *
 * @author ralph
 * @since 2023-01-13
 */
@Service
public class SystemThirdLoginServiceImpl extends ServiceImpl<SystemThirdLoginMapper, SystemThirdLogin> implements SystemThirdLoginService {

    /**
     * 绑定第三方信息
     * @param thirdStr 第三方信息
     */
    @Override
    public boolean bindThirdInfo(String thirdStr){
        // 解析第三方登录信息
        String decryptData = EncryptUtils.getDecryptData(thirdStr);
        // 转换为JSON
        JSONObject thirdInfo = new JSONObject();
        try {
            thirdInfo = JSONUtil.parseObj(decryptData);
        }catch (Exception e){
            throw new BusinessException(ErrorEnum.DATA_TYPE_TRANS_ERROR);
        }
        UserDTO user = SecurityUtils.getUser();
        SystemThirdLogin systemThirdLogin = new SystemThirdLogin();
        List<SystemThirdLogin> thirdList = super.list(Wrappers.<SystemThirdLogin>lambdaQuery()
                .eq(SystemThirdLogin::getUserId, user.getId())
                .eq(SystemThirdLogin::getType, thirdInfo.getStr("type")));
        if (CollUtil.isNotEmpty(thirdList)){
            throw new BusinessException(ErrorEnum.THIRD_BIND_ERROR,"该账号已绑定，请勿重复绑定",true);
        }
        systemThirdLogin.setUserId(user.getId());
        systemThirdLogin.setType(thirdInfo.getStr("type"));
        systemThirdLogin.setThirdId(thirdInfo.getStr("thirdId"));
        systemThirdLogin.setThirdName(thirdInfo.getStr("thirdName"));
        return super.save(systemThirdLogin);
    }

    /**
     * 获取当前登录人第三方信息
     */
    @Override
    public List<SystemThirdLogin> currentInfo() {
        Integer currentId = SecurityUtils.getCurrentId();
        if (null == currentId){
            return new ArrayList<>();
        }
        return super.list(Wrappers.<SystemThirdLogin>lambdaQuery()
                .eq(SystemThirdLogin::getUserId, currentId));
    }
}

