package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemMenuRoleRef;

/**
 * <p>
 * 角色菜单关联表 服务类
 * </p>
 *
 * @author ralph
 * @since 2022-12-20
 */
public interface SystemMenuRoleRefService extends IService<SystemMenuRoleRef> {
}
