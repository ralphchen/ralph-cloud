package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemUser;
import com.ralph.admin.entity.form.PermissionForm;
import com.ralph.common.core.entity.UserDTO;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ralph
 * @since 2022-12-20
 */
public interface SystemUserService extends IService<SystemUser> {

    /**
     * 新增或更新用户
     * @param systemUser 用户信息
     * @return 更新结果
     */
    Boolean saveOrUpdateUser(SystemUser systemUser);

    /**
     * 根据用户名查询用户信息
     * @param username 用户名
     * @return Result
     */
    UserDTO getUserByUsername(String username);

    /**
     * 给用户分配角色
     * @param permissionForm 权限信息
     * @return Result
     */
    Boolean distributeAuth(PermissionForm permissionForm);

    /**
     * 根据第三方登录信息获取用户信息
     * @param thirdInfo 第三方登录信息（加密）
     * @return 用户信息
     */
    UserDTO getUserInfoByThirdKey(String thirdInfo);

    /**
     * 根据手机号获取用户信息
     * @param phone 手机号
     * @return 用户信息
     */
    UserDTO getUserInfoByPhone(String phone);
}

