package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemRole;
import com.ralph.admin.entity.form.PermissionForm;

import java.util.List;

/**
 * <p>
 * 系统角色 服务类
 * </p>
 *
 * @author ralph
 * @since 2022-12-29
 */
public interface SystemRoleService extends IService<SystemRole> {

    /**
     * 根据用户名查询角色列表
     * @param username 用户名
     * @return Result
     */
    List<SystemRole> getRoleListByUser(String username);

    /**
     * 给角色分配菜单
     * @param permissionForm 权限信息
     * @return Result
     */
    Boolean distributeAuth(PermissionForm permissionForm);
}

