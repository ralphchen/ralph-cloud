package com.ralph.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ralph.admin.entity.SystemUserRoleRef;
import com.ralph.admin.mapper.SystemUserRoleRefMapper;
import com.ralph.admin.service.SystemUserRoleRefService;
import org.springframework.stereotype.Service;

@Service
public class SystemUserRoleRefServiceImpl extends ServiceImpl<SystemUserRoleRefMapper, SystemUserRoleRef> implements SystemUserRoleRefService {
}
