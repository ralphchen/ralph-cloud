package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemRouter;

import java.util.List;

/**
 * <p>
 * 动态路由 服务类
 * </p>
 *
 * @author ralph
 * @since 2023-01-10
 */
public interface SystemRouterService extends IService<SystemRouter> {

    /**
     * 获取动态路由列表
     * @return 动态路由列表
     */
    List<SystemRouter> getRouteList();
}

