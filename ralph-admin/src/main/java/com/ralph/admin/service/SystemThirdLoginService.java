package com.ralph.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ralph.admin.entity.SystemThirdLogin;

import java.util.List;

/**
 * <p>
 * 第三方登录信息 服务类
 * </p>
 *
 * @author ralph
 * @since 2023-01-13
 */
public interface SystemThirdLoginService extends IService<SystemThirdLogin> {

    /**
     * 绑定第三方信息
     * @param thirdInfo 第三方信息
     */
    boolean bindThirdInfo(String thirdInfo);

    /**
     * 获取当前登录人第三方信息
     */
    List<SystemThirdLogin> currentInfo();
}

