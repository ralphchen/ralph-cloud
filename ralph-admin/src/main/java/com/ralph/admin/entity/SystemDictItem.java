package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import com.ralph.common.core.entity.Groups;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 字典项
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_dict_item")
@ApiModel(value = "字典项")
public class SystemDictItem extends BaseEntity<SystemDictItem> {

    private static final long serialVersionUID = 1L;

    /**
     * 字典id
     */
    @NotNull(groups = Groups.Create.class)
    @ApiModelProperty(value="字典id")
    private Integer dictId;

    /**
     * key
     */
    @NotNull(groups = Groups.Create.class)
    @ApiModelProperty(value="key")
    private String itemKey;

    /**
     * value
     */
    @NotNull(groups = Groups.Create.class)
    @ApiModelProperty(value="value")
    private String itemValue;

    /**
     * 描述
     */
    @ApiModelProperty(value="描述")
    private String description;

    /**
     * 排序
     */
    @ApiModelProperty(value="排序")
    private Integer sort;
}

