package com.ralph.admin.entity.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("权限模块")
public class PermissionForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主体id
     */
    @NotNull
    @ApiModelProperty(value="主体id")
    private Integer id;

    /**
     * 权限id集合
     */
    @NotNull
    @ApiModelProperty(value="权限id集合")
    private List<Integer> permissionIdList;
}
