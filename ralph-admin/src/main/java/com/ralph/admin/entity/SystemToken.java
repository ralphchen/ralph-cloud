package com.ralph.admin.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "token")
public class SystemToken implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * token
     */
    @ApiModelProperty(value="token")
    private List<String> token;

    /**
     * 用户名
     */
    @ApiModelProperty(value="用户名")
    private String username;

    /**
     * 过期时间
     */
    @ApiModelProperty(value="过期时间")
    private Long expiredTime;
}
