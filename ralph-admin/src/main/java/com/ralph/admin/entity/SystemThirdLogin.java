package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 第三方登录信息
 * </p>
 *
 * @author ralph
 * @since 2023-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_third_login")
@ApiModel(value = "第三方登录信息")
public class SystemThirdLogin extends BaseEntity<SystemThirdLogin> {

    private static final long serialVersionUID = 1L;

    /**
     * 关联用户id
     */
    @ApiModelProperty(value="关联用户id")
    private Integer userId;

    /**
     * 登录类型
     */
    @ApiModelProperty(value="登录类型")
    private String type;

    /**
     * 第三方名称
     */
    @ApiModelProperty(value="第三方名称")
    private String thirdName;

    /**
     * 第三方唯一id
     */
    @ApiModelProperty(value="第三方唯一id")
    private String thirdId;
}

