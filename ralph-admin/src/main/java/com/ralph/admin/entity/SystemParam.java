package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 参数配置表
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_param")
@ApiModel(value = "参数配置表")
public class SystemParam extends BaseEntity<SystemParam> {

    private static final long serialVersionUID = 1L;

    /**
     * key
     */
    @ApiModelProperty(value="key")
    private String paramKey;

    /**
     * value
     */
    @ApiModelProperty(value="value")
    private String paramValue;

    /**
     * 描述
     */
    @ApiModelProperty(value="描述")
    private String description;


}

