package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 文件管理
 * </p>
 *
 * @author ralph
 * @since 2023-01-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_file")
@ApiModel(value = "文件管理")
public class SystemFile extends BaseEntity<SystemFile> {

    private static final long serialVersionUID = 1L;

    /**
     * 存储文件名
     */
    @ApiModelProperty(value="存储文件名")
    private String fileName;

    /**
     * 原始文件名
     */
    @ApiModelProperty(value="原始文件名")
    private String fileOriginName;

    /**
     * 存储文件url
     */
    @ApiModelProperty(value="存储文件url")
    private String fileUrl;

    /**
     * 文件后缀名
     */
    @ApiModelProperty(value="文件后缀名")
    private String fileSuffix;

    /**
     * 文件大小
     */
    @ApiModelProperty(value="文件大小")
    private Object fileSize;

    /**
     * 存储桶名称
     */
    @ApiModelProperty(value="存储桶名称")
    private String bucketName;


}

