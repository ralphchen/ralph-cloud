package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 动态路由
 * </p>
 *
 * @author ralph
 * @since 2023-01-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_router")
@ApiModel(value = "动态路由")
public class SystemRouter extends BaseEntity<SystemRouter> {

    private static final long serialVersionUID = 1L;

    /**
     * 路由id
     */
    @ApiModelProperty(value="路由id")
    private String routeId;

    /**
     * 路由名称
     */
    @ApiModelProperty(value="路由名称")
    private String routeName;

    /**
     * uri
     */
    @ApiModelProperty(value="uri")
    private String uri;

    /**
     * 断言规则
     */
    @ApiModelProperty(value="断言规则")
    private String predicates;

    /**
     * 过滤规则
     */
    @ApiModelProperty(value="过滤规则")
    private String filters;

    /**
     * 排序
     */
    @TableField("`order`")
    @ApiModelProperty(value="排序")
    private Integer order;


}

