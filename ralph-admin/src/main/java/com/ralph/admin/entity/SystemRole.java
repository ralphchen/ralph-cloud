package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author ralph
 * @since 2022-12-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_role")
@ApiModel(value = "系统角色")
public class SystemRole extends BaseEntity<SystemRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 角色编码
     */
    @ApiModelProperty(value="角色编码")
    private String roleCode;

    /**
     * 角色名称
     */
    @ApiModelProperty(value="角色名称")
    private String roleName;


}

