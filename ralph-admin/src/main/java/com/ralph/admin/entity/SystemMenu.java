package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author ralph
 * @since 2023-01-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_menu")
@ApiModel(value = "菜单表")
public class SystemMenu extends BaseEntity<SystemMenu> {

    private static final long serialVersionUID = 1L;

    /**
     * 父菜单id
     */
    @ApiModelProperty(value="父菜单id")
    private Integer parentId;

    /**
     * 菜单名称
     */
    @ApiModelProperty(value="菜单名称")
    private String menuName;

    /**
     * 菜单编码
     */
    @ApiModelProperty(value="菜单编码")
    private String menuCode;

    /**
     * 权限标识
     */
    @ApiModelProperty(value="权限标识")
    private String permission;

    /**
     * 菜单地址
     */
    @ApiModelProperty(value="菜单地址")
    private String path;

    /**
     * 菜单icon
     */
    @ApiModelProperty(value="菜单icon")
    private String icon;

    /**
     * 菜单排序
     */
    @ApiModelProperty(value="菜单排序")
    private Integer sort;

    /**
     * 菜单类型；0：菜单，1：按钮
     */
    @ApiModelProperty(value="菜单类型；0：菜单，1：按钮")
    private String type;


}

