package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import com.ralph.common.core.entity.Groups;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author ralph
 * @since 2022-12-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_dict")
@ApiModel(value = "字典表")
public class SystemDict extends BaseEntity<SystemDict> {

    private static final long serialVersionUID = 1L;

    /**
     * 字典类型
     */
    @NotBlank(groups = Groups.Create.class)
    @ApiModelProperty(value="字典类型")
    private String type;

    /**
     * 字典描述
     */
    @ApiModelProperty(value="字典描述")
    private String description;


}

