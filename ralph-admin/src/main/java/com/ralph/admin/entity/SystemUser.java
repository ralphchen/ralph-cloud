package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import com.ralph.common.core.entity.Groups;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author ralph
 * @since 2022-12-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_user")
@ApiModel(value = "用户表")
public class SystemUser extends BaseEntity<SystemUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    @NotBlank(groups = Groups.Create.class)
    @ApiModelProperty(value="用户名")
    private String username;

    /**
     * 密码
     */
    @NotBlank(groups = Groups.Create.class)
    @ApiModelProperty(value="密码")
    private String password;

    /**
     * 手机号
     */
    @NotBlank(groups = Groups.Create.class)
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="邮箱")
    private String email;

    /**
     * 头像地址
     */
    @ApiModelProperty(value="头像地址")
    private String avatar;

    /**
     * 锁定标识
     */
    @ApiModelProperty(value="锁定标识")
    private Boolean lockFlag;

    /**
     * 密码更新时间
     */
    @ApiModelProperty(value="密码更新时间")
    private Date pwdUpdateTime;
}

