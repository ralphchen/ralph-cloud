package com.ralph.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ralph.common.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Timestamp;

/**
 * <p>
 * 菜单角色关联表
 * </p>
 *
 * @author ralph
 * @since 2023-01-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("system_menu_role_ref")
@ApiModel(value = "菜单角色关联表")
public class SystemMenuRoleRef extends BaseEntity<SystemMenuRoleRef> {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Timestamp createDate;

    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private String createBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private Timestamp updateDate;

    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private String updateBy;

    /**
     * 角色id
     */
        @ApiModelProperty(value="角色id")
    private Integer roleId;

    /**
     * 菜单id
     */
        @ApiModelProperty(value="菜单id")
    private Integer menuId;


}

